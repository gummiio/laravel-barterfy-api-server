<?php

namespace Barterfy\Inbox\Requests;

use Barterfy\Api\Requests\ApiRequest;

class InboxMessageListRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->route('inboxes')->users->where('id', Auth()->id())->count();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'limit' => 'integer',
            'before' => 'date_format:Y-m-d H:i:s'
        ];
    }
}
