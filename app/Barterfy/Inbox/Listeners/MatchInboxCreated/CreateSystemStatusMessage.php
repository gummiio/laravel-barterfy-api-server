<?php

namespace Barterfy\Inbox\Listeners\MatchInboxCreated;

use Barterfy\Inbox\Events\MatchInboxCreated;
use Barterfy\Inbox\Events\NewMessageCreated;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSystemStatusMessage implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MatchInboxCreated  $event
     * @return void
     */
    public function handle(MatchInboxCreated $event)
    {
        $inbox = $event->inbox;
        $inbox->load('items.user');
        $body = [];

        $inbox->items->each(function ($item) use (&$body) {
            $body[] = "{$item->user->display_name}'s \"$item->description\"";
        });

        $message = $inbox->systemMessage(
            'Congratulation! You and have a match! ' .
            implode(' with ', $body) .
            '. Please press the "i" icon and click Trade Information to see more detail.'
        );

        event(new NewMessageCreated($message));
    }
}
