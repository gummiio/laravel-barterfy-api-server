<?php

namespace Acceptance\Users;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;

class MeTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->get('v2/users/me', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_return_whats_nesssary()
    {
        $user = factory(User::class)->create();

        $this->get('v2/users/me', $this->headers($user));

        $this->seeJsonStructure([
            'data' => [
                'id', 'avatar', 'display_name', 'is_you', 'auth_token',
                'country', 'city', 'longitude', 'latitude',
                'last_login', 'created_at'
            ]
        ]);
    }
}
