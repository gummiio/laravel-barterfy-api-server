<?php

namespace Acceptance\Items;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Item\Model\Item;

class SearchTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->get('v2/items/search', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_shoud_fail_if_current_auth_user_does_not_have_coordinates()
    {
        $user = factory(User::class)->create([
            'longitude' => null, 'latitude' => null
        ]);

        $this->get('v2/items/search', $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(400);
    }

    /** @test */
    public function it_should_success_if_user_has_coordinates()
    {
        $user = factory(User::class)->create([
            'longitude' => 123, 'latitude' => 456
        ]);

        $this->get('v2/items/search', $this->headers($user));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function it_should_not_return_your_items()
    {
        $user = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $user->items()->saveMany(factory(Item::class, 2)->make([
            'is_readied' => true
        ]));

        $other = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other->items()->saveMany(factory(Item::class, 5)->make([
            'is_readied' => true
        ]));

        $this->get('v2/items/search', $this->headers($user));

        $this->dontSeeJson(['is_yorus' => true]);

        $this->assertEquals(5, count($this->jsonData()));
    }

    /** @test */
    public function it_should_not_return_unreadied_items()
    {
        $user = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other->items()->saveMany(factory(Item::class, 5)->make([
            'is_readied' => true
        ]));

        $item = $other->items()->save(factory(Item::class)->make([
            'is_readied' => false,
            'description' => 'do-not-see-me'
        ]));

        $this->get('v2/items/search', $this->headers($user));

        $this->dontSeeJson(['description' => 'do-not-see-me']);

        $this->assertEquals(5, count($this->jsonData()));
    }

    /** @test */
    public function it_should_not_return_traded_items()
    {
        $user = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other->items()->saveMany(factory(Item::class, 5)->make([
            'is_traded' => false, 'is_readied' => true
        ]));

        $item = $other->items()->save(factory(Item::class)->make([
            'is_traded' => true, 'is_readied' => true,
            'description' => 'do-not-see-me'
        ]));

        $this->get('v2/items/search', $this->headers($user));

        $this->dontSeeJson(['description' => 'do-not-see-me']);

        $this->assertEquals(5, count($this->jsonData()));
    }

    /** @test */
    public function it_should_get_items_from_within_radius()
    {
        $user = factory(User::class)->create([
            'longitude' => -123.144035, 'latitude' => 49.289275
        ]);

        factory(User::class)->create([ // ~ 29km
            'longitude' => -122.739258, 'latitude' => 49.265438
        ])->items()->saveMany(factory(Item::class, 5)->make([
            'is_readied' => true
        ]));

        factory(User::class)->create([ // ~46km
            'longitude' => -122.508545, 'latitude' => 49.223750
        ])->items()->saveMany(factory(Item::class, 3)->make([
            'is_readied' => true
        ]));

        $this->get('v2/items/search', $this->headers($user));

        $this->assertEquals(5, count($this->jsonData()));
    }

    /** @test */
    public function it_should_limits_to_20_by_default()
    {
        $user = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        factory(User::class)->create([ // ~ 29km
            'longitude' => 100, 'latitude' => 100
        ])->items()->saveMany(factory(Item::class, 50)->make([
            'is_readied' => true
        ]));

        $this->get('v2/items/search', $this->headers($user));

        $this->assertEquals(20, count($this->jsonData()));
    }

    /** @test */
    public function it_should_only_get_items_from_user_that_you_dont_currently_matching_with()
    {
        $user = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $item = $other->items()->save(factory(Item::class)->make([
            'is_readied' => true
        ]));

        factory(Inbox::class)->create([
            'match_result' => null
        ])->users()->saveMany([$user, $other]);

        $returnItem = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ])->items()->save(factory(Item::class)->make([
            'is_readied' => true,
            'description' => 'you-should-see-me'
        ]));

        $this->get('v2/items/search', $this->headers($user));

        $this->seeJson(['description' => 'you-should-see-me']);

        $this->assertEquals(1, count($this->jsonData()));
    }

    /** @test */
    public function you_should_not_be_getting_items_that_you_already_made_decision_with()
    {
        $user = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $item = $other->items()->save(factory(Item::class)->make([
            'is_readied' => true
        ]));

        $item->likedBy($user);

        $this->get('v2/items/search', $this->headers($user));

        $this->assertEquals(0, count($this->jsonData()));
    }

    /** @test */
    public function the_limit_can_be_overwrites()
    {
        $user = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        factory(User::class)->create([ // ~ 29km
            'longitude' => 100, 'latitude' => 100
        ])->items()->saveMany(factory(Item::class, 50)->make([
            'is_readied' => true
        ]));

        $this->get('v2/items/search?limit=5', $this->headers($user));

        $this->assertEquals(5, count($this->jsonData()));

        $this->get('v2/items/search?limit=40', $this->headers($user));

        $this->assertEquals(40, count($this->jsonData()));
    }

    /** @test */
    public function the_radius_can_be_overwrites()
    {
        $user = factory(User::class)->create([
            'longitude' => -123.144035, 'latitude' => 49.289275
        ]);

        factory(User::class)->create([ // ~ 29km
            'longitude' => -122.739258, 'latitude' => 49.265438
        ])->items()->saveMany(factory(Item::class, 5)->make([
            'is_readied' => true
        ]));

        factory(User::class)->create([ // ~46km
            'longitude' => -122.508545, 'latitude' => 49.223750
        ])->items()->saveMany(factory(Item::class, 3)->make([
            'is_readied' => true
        ]));

        $this->get('v2/items/search?radius=20', $this->headers($user));

        $this->assertEquals(0, count($this->jsonData()));

        $this->get('v2/items/search?radius=35', $this->headers($user));

        $this->assertEquals(5, count($this->jsonData()));

        $this->get('v2/items/search?radius=50', $this->headers($user));

        $this->assertEquals(8, count($this->jsonData()));
    }

    /** @test */
    public function the_unit_can_be_changed()
    {
        $user = factory(User::class)->create([
            'longitude' => -123.144035, 'latitude' => 49.289275
        ]);

        factory(User::class)->create([ // ~ 29km / 18 miles
            'longitude' => -122.739258, 'latitude' => 49.265438
        ])->items()->saveMany(factory(Item::class, 5)->make([
            'is_readied' => true
        ]));

        factory(User::class)->create([ // ~46km / 29 miles
            'longitude' => -122.508545, 'latitude' => 49.223750
        ])->items()->saveMany(factory(Item::class, 3)->make([
            'is_readied' => true
        ]));

        $this->get('v2/items/search?radius=15&unit=miles', $this->headers($user));

        $this->assertEquals(0, count($this->jsonData()));

        $this->get('v2/items/search?radius=20&unit=miles', $this->headers($user));

        $this->assertEquals(5, count($this->jsonData()));

        $this->get('v2/items/search?radius=35&unit=miles', $this->headers($user));

        $this->assertEquals(8, count($this->jsonData()));
    }

    /** @test */
    public function it_can_disregard_the_radius()
    {
        $user = factory(User::class)->create([
            'longitude' => -123.144035, 'latitude' => 49.289275
        ]);

        factory(User::class)->create([ // ~ 29km / 18 miles
            'longitude' => -122.739258, 'latitude' => 49.265438
        ])->items()->saveMany(factory(Item::class, 5)->make([
            'is_readied' => true
        ]));

        factory(User::class)->create([ // ~46km / 29 miles
            'longitude' => -122.508545, 'latitude' => 49.223750
        ])->items()->saveMany(factory(Item::class, 3)->make([
            'is_readied' => true
        ]));

        $this->get('v2/items/search?radius=-1', $this->headers($user));

        $this->assertEquals(8, count($this->jsonData()));
    }

    /** @test */
    public function it_can_grab_item_even_having_a_matching_inbox()
    {
        $user = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other->items()->saveMany(factory(Item::class, 10)->make([
            'is_readied' => true
        ]));

        factory(Inbox::class)->create([
            'match_result' => null
        ])->users()->saveMany([$user, $other]);

        $this->get('v2/items/search?unmatch_only=0', $this->headers($user));

        $this->assertEquals(10, count($this->jsonData()));
    }

    /** @test */
    public function it_can_exlucde_items_by_id()
    {
        $user = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $other = factory(User::class)->create([
            'longitude' => 100, 'latitude' => 100
        ]);

        $excludes = $other->items()->saveMany(factory(Item::class, 2)->make([
            'is_readied' => true,
            'description' => 'should-not-see-me'
        ]));

        $other->items()->saveMany(factory(Item::class, 10)->make([
            'is_readied' => true
        ]));

        $queryString = [];

        $excludes->each(function ($item) use (&$queryString) {
            $queryString[] = 'excludes[]=' . $item->id;
        });

        $this->get('v2/items/search?' . implode('&', $queryString), $this->headers($user));

        $this->dontSeeJson(['description' => 'should-not-see-me']);

        $this->assertEquals(10, count($this->jsonData()));
    }
}
