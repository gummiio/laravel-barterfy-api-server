<?php

namespace Barterfy\Inbox\Listeners\InboxTradeCompleted;

use Barterfy\Inbox\Events\InboxTradeCompleted;
use Barterfy\Inbox\Model\Inbox;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyOnSlack implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InboxTradeCompleted  $event
     * @return void
     */
    public function handle(InboxTradeCompleted $event)
    {
        $inbox = $event->inbox;
        $users = $inbox->users;
        $items = $inbox->items;

        $slack = app('SlackHelper')
            ->text(sprintf('*A Trade has been completed!*'))
            ->color('good')
            ->addField([
                "*Inbox ID*: {$inbox->id}",
                "*At:* ".$inbox->updated_at->toDateTimeString()
            ]);

        $users->each(function ($user) use ($slack) {
            $slack->addField([
                "_User ID_: {$user->id}",
                "_User Name_: {$user->full_name}"
            ], '', true);
        });

        $items->each(function ($item) use ($slack) {
            $slack->addField([
                "_Item ID_: {$item->id}",
                "_Item Description_: {$item->description}"
            ], '', true);
        });

        $slack->send();
    }
}
