<?php

namespace Barterfy\Inbox\Listeners\NewSupportMessageCreated;

use Barterfy\Device\Repository\DeviceRepository;
use Barterfy\Inbox\Events\NewSupportMessageCreated;
use Barterfy\Inbox\Repository\InboxMessageStatusRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendPushNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewSupportMessageCreated  $event
     * @return void
     */
    public function handle(NewSupportMessageCreated $event)
    {
        if (! $event->shouldNotify) return;

        $message = $event->message;

        $inbox = $message->inbox;

        $devices = DeviceRepository::GetInboxDevices($inbox, 'notify_on_new_message', $message->user);

        $devices->each(function ($device) use ($message) {
            app('PushNotificationHelper')
                ->to($device)
                // ->badge(InboxMessageStatusRepository::GetUserUnreadMessages($device->user)->count())
                ->with($this->prepareNotificationData($message))
                ->send(sprintf('Barterfy Support: %s', $message->message));
        });
    }

    protected function prepareNotificationData($message)
    {
        return [
            'type' => 'messages',
            'inbox_id' => $message->inbox_id,
            'message_id' => $message->id,
        ];
    }
}
