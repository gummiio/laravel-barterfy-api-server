<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Barterfy\Inbox\Events\AcceptanceHasBeenRegreted;
use Barterfy\Inbox\Events\InboxHasBeenAccepted;
use Barterfy\Inbox\Events\InboxHasBeenRejected;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Inbox\Repository\InboxRepository;
use Barterfy\Inbox\Requests\InboxAcceptanceRequest;
use Barterfy\Inbox\Requests\InboxListRequest;
use Barterfy\Inbox\Requests\InboxShowRequest;

class InboxController extends ApiController
{
    public function index(InboxListRequest $request)
    {
        $inboxes = InboxRepository::GetInboxesByUser(Auth()->user(), $request);

        return response()->apiSuccess($inboxes->count() . ' inbox(es) found.', $inboxes);
    }

    public function show(InboxShowRequest $request, Inbox $inbox)
    {
        return response()->apiSuccess('', $inbox);
    }

    public function support()
    {
        $inbox = InboxRepository::GetUserSupportInbox(Auth()->user());

        return response()->apiSuccess('', $inbox);
    }

    public function accept(InboxAcceptanceRequest $request, Inbox $inbox)
    {
        if ($decision = InboxRepository::UserHasMadeDecision(Auth()->user(), $inbox)) {
            return response()->apiError('You already ' . ($decision->accepted? 'accepted' : 'rejected') . ' the trade', '', 400);
        }

        $acceptance = $inbox->acceptedBy(Auth()->user());

        event(new InboxHasBeenAccepted($acceptance));

        return response()->apiSuccess('You have accepted the trade.', '', 201);
    }


    public function reject(InboxAcceptanceRequest $request, Inbox $inbox)
    {
        if ($decision = InboxRepository::UserHasMadeDecision(Auth()->user(), $inbox)) {
            return response()->apiError('You already ' . ($decision->accepted? 'accepted' : 'rejected') . ' the trade', '', 400);
        }

        $acceptance = $inbox->rejectedBy(Auth()->user());

        event(new InboxHasBeenRejected($acceptance));

        return response()->apiSuccess('You have rejected the trade.', '', 201);
    }


    public function regret(InboxAcceptanceRequest $request, Inbox $inbox)
    {
        if (! InboxRepository::UserHasMadeDecision(Auth()->user(), $inbox)) {
            return response()->apiError('You have not express your decision yet.', '', 400);
        }

        $acceptance = $inbox->regretedBy(Auth()->user());

        event(new AcceptanceHasBeenRegreted($acceptance));

        return response()->apiSuccess('You have regrated your decision.', '');
    }
}
