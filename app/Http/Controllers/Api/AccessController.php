<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Barterfy\Access\Events\NewUserRegistered;
use Barterfy\Access\Events\UserLoggedIn;
use Barterfy\Access\Repository\UserRepository;
use Barterfy\Access\Requests\UserLoginRequest;
use Barterfy\Access\Requests\UserRegisterRequest;

class AccessController extends ApiController
{
    public function register(UserRegisterRequest $request)
    {
        if (! $user = UserRepository::GetUserByFacebookToken($request->facebook_token)) {
            $graphUser = UserRepository::GetGraphUserFromFacebookToken($request->facebook_token);

            if (! $user = UserRepository::GetUserFromGraphUser($graphUser)) {
                $user = UserRepository::CreateUserFromGraphUser($graphUser, $request->facebook_token);

                event(new NewUserRegistered($user));

                $user = $user->fresh();
                $user->jwt = $user->jwt;
                return response()->apiSuccess('User Created.', $user, 201);
            }

            $user->facebook_token = $request->facebook_token;
            $user->save();

            $user->jwt = $user->jwt;
            return response()->apiSuccess('User Updated.', $user);
        }

        $user->jwt = $user->jwt;
        return response()->apiSuccess('User Already Exists.', $user);
    }

    public function login(UserLoginRequest $request)
    {
        if (! $user = UserRepository::GetUserByFacebookToken($request->facebook_token)) {
            return response()->apiError('User Not Found', null, 404);
        }

        event(new UserLoggedIn($user));

        $user->jwt = $user->jwt;
        return response()->apiSuccess('Login Successful.', $user);
    }
}
