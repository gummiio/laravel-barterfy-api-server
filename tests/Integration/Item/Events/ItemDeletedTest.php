<?php

namespace Integration\Item\Events;

use Barterfy\Access\Model\User;
use Barterfy\Helpers\CloudinaryHelper;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Item\Events\ItemDeleted;
use Barterfy\Item\Model\Item;
use Barterfy\Item\Model\ItemImage;
use Integration\IntegrationTestCase;
use Mockery;

class ItemDeletedTest extends IntegrationTestCase
{
    /** @test */
    public function should_unmatch_all_matching_inbox_upon_deletion()
    {
        $user = factory(User::class)->create();
        $item = $user->items()->save(
            factory(Item::class)->make()
        );

        factory(Inbox::class, 2)->create(['match_result' => false])
            ->each(function ($inbox) use ($item) {
                $inbox->items()->save($item);
            });

        factory(Inbox::class, 3)->create(['match_result' => true])
            ->each(function ($inbox) use ($item) {
                $inbox->items()->save($item);
            });

        factory(Inbox::class, 5)->create(['match_result' => null])
            ->each(function ($inbox) use ($item) {
                $inbox->items()->save($item);
            });

        event(new ItemDeleted($item));

        $inboxes = Inbox::all();

        $this->assertCount(7, $inboxes->whereStrict('match_result', false));
        $this->assertCount(0, $inboxes->whereStrict('match_result', null));
        $this->assertCount(3, $inboxes->whereStrict('match_result', true));
    }

    /** @test */
    public function should_clean_up_all_item_likes_statues()
    {
        $you = factory(User::class)->create();
        $item = $you->items()->save(
            factory(Item::class)->make()
        );

        factory(User::class, 5)->create()->each(function ($user) use ($item) {
            $item->likedBy($user);
        });

        factory(User::class, 7)->create()->each(function ($user) use ($item) {
            $item->dislikedBy($user);
        });

        $this->assertCount(12, $item->itemLikes);

        event(new ItemDeleted($item));

        $this->assertCount(0, $item->fresh()->itemLikes);
    }

    /** @test */
    public function it_should_delete_item_images()
    {
        $you = factory(User::class)->create();
        $item = $you->items()->save(
            factory(Item::class)->make()
        );

        $image = $item->images()->save(
            factory(ItemImage::class)->make()
        );

        event(new ItemDeleted($item));

        $this->seeInDatabase('item_images', [
            'id' => $image->id,
            ['deleted_at', '!=', null]
        ]);
    }
}
