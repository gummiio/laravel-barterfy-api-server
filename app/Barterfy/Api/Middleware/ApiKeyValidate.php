<?php

namespace Barterfy\Api\Middleware;

use Barterfy\Api\Exceptions\ApiTokenInvalidException;
use Barterfy\Api\Exceptions\ApiTokenNotFoundException;
use Closure;

class ApiKeyValidate
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $admin = false)
    {
        if (! app('ApiGuard')->fetchApiToken()) {
            throw new ApiTokenNotFoundException;
        }

        if (! app('ApiGuard')->verifiedApiToken($admin)) {
            throw new ApiTokenInvalidException;
        }

        return $next($request);
    }
}
