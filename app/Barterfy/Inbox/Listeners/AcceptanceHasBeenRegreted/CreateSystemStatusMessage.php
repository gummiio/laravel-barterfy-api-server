<?php

namespace Barterfy\Inbox\Listeners\AcceptanceHasBeenRegreted;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Events\AcceptanceHasBeenRegreted;
use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Model\Inbox;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSystemStatusMessage implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AcceptanceHasBeenRegreted  $event
     * @return void
     */
    public function handle(AcceptanceHasBeenRegreted $event)
    {
        $acceptance = json_decode($event->acceptanceJSON);
        $inbox = Inbox::find($acceptance->inbox_id);
        $accepter = User::find($acceptance->user_id);

        $message = $inbox->systemMessage(
            $accepter->display_name . ' changed the status to matching.'
        );

        event(new NewMessageCreated($message));
    }
}
