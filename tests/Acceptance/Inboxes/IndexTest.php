<?php

namespace Acceptance\Inboxes;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;
use Carbon\Carbon;

class IndexTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->get('v2/inboxes', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_will_retreive_your_matching_inboxes()
    {
        $other = factory(User::class)->create();
        $other->inboxes()->saveMany(
            factory(Inbox::class, 5)->make(['match_result' => null])
        );

        $you = factory(User::class)->create();
        $you->inboxes()->save(
            factory(Inbox::class)->make(['match_result' => null])
        );

        $you->inboxes()->save(
            factory(Inbox::class)->make(['match_result' => false])
        );

        $this->get('v2/inboxes', $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->assertEquals(1, count($this->jsonData()));
    }

    /** @test */
    public function is_should_return_in_updated_at_descending_order()
    {
        $you = factory(User::class)->create();
        $first = $you->inboxes()->save(
            factory(Inbox::class)->make([
                'match_result' => null, 'updated_at' => Carbon::now()
            ])
        );

        $second = $you->inboxes()->save(
            factory(Inbox::class)->make([
                'match_result' => null, 'updated_at' => Carbon::now()->addMinutes(1)
            ])
        );

        $this->get('v2/inboxes', $this->headers($you));

        $shouldMatch = collect([$second, $first])->pluck('id')->all();
        $this->assertEquals($shouldMatch, collect($this->jsonData())->pluck('id')->all());
    }

    /** @test */
    public function it_should_not_include_support_inbox()
    {
        $you = factory(User::class)->create();
        $you->inboxes()->save(
            factory(Inbox::class)->make([
                'match_result' => null, 'is_support' => true, 'name' => 'do-not-see-me'
            ])
        );

        $you->inboxes()->saveMany(
            factory(Inbox::class, 5)->make(['match_result' => null])
        );

        $this->get('v2/inboxes', $this->headers($you));

        $this->dontSeeJson(['name' => 'do-not-see-me']);

        $this->assertCount(5, $this->jsonData());
    }

    /** @test */
    public function it_should_return_10_inboxes_by_default()
    {
        $you = factory(User::class)->create();
        $you->inboxes()->saveMany(
            factory(Inbox::class, 50)->make(['match_result' => null])
        );

        $this->get('v2/inboxes', $this->headers($you));

        $this->assertCount(10, $this->jsonData());
    }

    /** @test */
    public function it_can_include_support_inbox()
    {
        $you = factory(User::class)->create();
        $you->inboxes()->save(
            factory(Inbox::class)->make([
                'match_result' => null, 'is_support' => true, 'name' => 'do-see-me'
            ])
        );

        $you->inboxes()->saveMany(
            factory(Inbox::class, 5)->make(['match_result' => null])
        );

        $this->get('v2/inboxes?with_support=1', $this->headers($you));

        $this->seeJson(['name' => 'do-see-me']);

        $this->assertCount(6, $this->jsonData());
    }

    /** @test */
    public function the_limit_can_be_changed()
    {
        $you = factory(User::class)->create();
        $you->inboxes()->saveMany(
            factory(Inbox::class, 50)->make(['match_result' => null])
        );

        $this->get('v2/inboxes?limit=20', $this->headers($you));

        $this->assertCount(20, $this->jsonData());
    }

    /** @test */
    public function can_get_inbox_before_certain_datetime()
    {
        $user = factory(User::class)->create();
        $now = Carbon::now();
        $inc = clone($now);

        for ($i = 0; $i < 5; $i ++) {
            $user->inboxes()->save(
                factory(Inbox::class)->make([
                    'match_result' => null, 'updated_at' => $inc->addMinutes(1),
                    'name' => 'should-see-me'
                ])
            );
        }

        for ($i = 0; $i < 25; $i ++) {
            $user->inboxes()->save(
                factory(Inbox::class)->make([
                    'match_result' => null, 'updated_at' => $inc->addMinutes(1),
                    'name' => 'should-not-see-me'
                ])
            );
        }

        $before = $now->addMinutes(6)->toDateTimeString();
        $this->get('v2/inboxes?before=' . $before, $this->headers($user));

        $this->dontSeeJson(['name' => 'should-not-see-me']);
        $this->seeJson(['name' => 'should-see-me']);
        $this->assertEquals(5, count($this->jsonData()));
    }

    /** @test */
    public function it_can_get_by_inbox_match_status()
    {
        $user = factory(User::class)->create();

        $user->inboxes()->saveMany(
            factory(Inbox::class, 2)->make([
                'match_result' => true, 'name' => 'successed-inbox'
            ])
        );

        $user->inboxes()->saveMany(
            factory(Inbox::class, 3)->make([
                'match_result' => false, 'name' => 'failed-inbox'
            ])
        );

        $user->inboxes()->saveMany(
            factory(Inbox::class, 4)->make([
                'match_result' => null, 'name' => 'matching-inbox'
            ])
        );

        $this->get('v2/inboxes?match_result=successed', $this->headers($user));

        $this->dontSeeJson(['name' => 'matching-inbox']);
        $this->dontSeeJson(['name' => 'failed-inbox']);
        $this->seeJson(['name' => 'successed-inbox']);
        $this->assertEquals(2, count($this->jsonData()));

        $this->get('v2/inboxes?match_result=failed', $this->headers($user));

        $this->dontSeeJson(['name' => 'matching-inbox']);
        $this->dontSeeJson(['name' => 'successed-inbox']);
        $this->seeJson(['name' => 'failed-inbox']);
        $this->assertEquals(3, count($this->jsonData()));

        $this->get('v2/inboxes?match_result=matching', $this->headers($user));

        $this->dontSeeJson(['name' => 'failed-inbox']);
        $this->dontSeeJson(['name' => 'successed-inbox']);
        $this->seeJson(['name' => 'matching-inbox']);
        $this->assertEquals(4, count($this->jsonData()));
    }
}
