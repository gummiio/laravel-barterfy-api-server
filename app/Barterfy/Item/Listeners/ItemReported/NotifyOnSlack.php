<?php

namespace Barterfy\Item\Listeners\ItemReported;

use Barterfy\Item\Events\ItemReported;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyOnSlack implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemReported  $event
     * @return void
     */
    public function handle(ItemReported $event)
    {
        $message = $event->message;
        $item = $event->item;
        $user = $item->user;

        app('SlackHelper')
            ->text('*An item has been reported.*')
            ->color('danger')
            ->addField([
                "*Item ID:* <" . $item->getItemUrl() . "|{$item->id}>",
                "*Item Description:* {$item->description}",
                "*User:* {$user->full_name}",
                "*Reason:* {$message}",
                "*At:* " . Carbon::now()->toDateTimeString()
            ])
            ->thumbnail($item->images->first()->url)
            ->send();
    }
}
