<?php

namespace Barterfy\Api\Exceptions;

use Barterfy\Api\Exceptions\ApiResponseException;

class ApiTokenNotFoundException extends ApiResponseException
{
    public function __construct()
    {
        parent::__construct('Whoops, looks like something went wrong.', [
            'type'    => 'ApiTokenNotFound',
            'message' => 'Unable to find api token.'
        ], 403);
    }
}
