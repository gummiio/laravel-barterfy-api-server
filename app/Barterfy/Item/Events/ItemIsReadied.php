<?php

namespace Barterfy\Item\Events;

use App\Events\Event;
use Barterfy\Item\Model\Item;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class ItemIsReadied extends Event implements ShouldQueue, ShouldBroadcast
{
    use SerializesModels;

    /**
     * Item Object
     *
     * @var object
     */
    public $item;
    public $shouldNotify;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Item $item, $shouldNotify = true)
    {
        $this->item = $item;
        $this->shouldNotify = $shouldNotify;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [env('REDIS_CHANNEL', '')];
    }

    public function broadcastWith()
    {
        return [
            'sendTo' => [$this->item->user->id],
            'item' => $this->item->toArray()
        ];
    }
}
