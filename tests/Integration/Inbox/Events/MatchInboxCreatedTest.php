<?php

namespace Integration\Inbox\Events;

use Barterfy\Inbox\Events\MatchInboxCreated;
use Barterfy\Inbox\Model\Inbox;
use Integration\IntegrationTestCase;

class MatchInboxCreatedTest extends IntegrationTestCase
{
    /** @test */
    public function it_should_create_matching_welcome_message()
    {
        $inbox = factory(Inbox::class)->create();

        event(new MatchInboxCreated($inbox));

        $this->seeInDatabase('inbox_messages', [
            'inbox_id' => $inbox->id,
            'type' => 'status'
        ]);


        $this->assertCount(1, $inbox->load('messages')->messages);
    }
}
