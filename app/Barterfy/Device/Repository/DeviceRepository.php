<?php

namespace Barterfy\Device\Repository;

use Barterfy\Access\Model\User;
use Barterfy\Device\Model\Device;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Repository;
use Illuminate\Http\Request;

class DeviceRepository extends Repository
{
    public static function GetDevicesByUser(User $user, Request $request)
    {
        $query = $user->devices();

        if ($request->has('platform')) {
            $query->where('device_platform', $request->platform);
        }

        if ($request->input('deleted', false)) {
            $query->withTrashed();
        }

        return $query->get();
    }

    public static function GetDeviceByNotificationToken($notification_token)
    {
        return Device::where('notification_token', $notification_token)->first();
    }

    public static function CreateDeviceFromInput(Request $request)
    {
        $device = new Device($request->all());
        $device->notification_token = $request->notification_token;

        return $device;
    }

    public static function GetInboxDevices(Inbox $inbox, $settingKey, $excludeUser = null)
    {
        $devices = Device::select('devices.*')
            ->join('users', 'devices.user_id', '=', 'users.id')
            ->join('inbox_users', 'inbox_users.user_id', '=', 'users.id')
            ->join('inboxes', 'inbox_users.inbox_id', '=', 'inboxes.id')
            ->where('inboxes.id', $inbox->id)
            ->where('users.online', false)
            ->where('devices.' . $settingKey, true);

        if ($excludeUser) {
            $devices->where('users.id', '!=', $excludeUser->id);
        }

        return $devices->get();
    }
}
