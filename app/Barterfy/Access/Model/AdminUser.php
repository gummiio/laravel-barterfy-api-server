<?php

namespace Barterfy\Access\Model;

use Barterfy\Model;
use Barterfy\Traits\HasMetaData;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;

class AdminUser extends Model implements AuthenticatableContract
{
    use Authenticatable, SoftDeletes, HasMetaData;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admin_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'first_name', 'middle_name', 'last_name', 'metadata'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password'];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['full_name', 'display_name', 'auth_token'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $casts = [
        'metadata' => 'array'
    ];

    /**
     * Get full_name attribute
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        $full_name = implode(' ', [
            $this->first_name, $this->middle_name, $this->last_name
        ]);

        return preg_replace('#\s{2,}#', ' ', $fullname);
    }

    /**
     * Get display_name attribute
     *
     * @return string
     */
    public function getDisplayNameAttribute()
    {
        return $this->first_name;
    }

    /**
     * Get auth_token attribute
     *
     * @return string
     */
    public function getAuthTokenAttribute()
    {
        return base64_encode(':{$this->api_token}');
    }
}
