<?php

namespace App\Providers;

use Barterfy\Access\Model\User;
use Barterfy\Device\Model\Device;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Item\Model\Item;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers\Web';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot()
    {
        parent::boot();

        Route::bind('items', function ($itemId) {
            return Item::findOrFail($itemId);
        });

        Route::bind('devices', function ($deviceId) {
            return Device::findOrFail($deviceId);
        });

        Route::bind('inboxes', function ($inboxId) {
            return Inbox::findOrFail($inboxId);
        });

        Route::bind('users', function ($userId) {
            return User::findOrFail($userId);
        });
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapWebRoutes();

        $this->mapApiRoutes();

        $this->mapAdminRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::group([
            'middleware' => 'web',
            'namespace' => $this->namespace,
        ], function ($router) {
            require base_path('routes/web.php');
        });
    }

    protected function mapApiRoutes()
    {
        Route::group([
            'middleware' => ['web', 'api'],
            'namespace'  => 'App\Http\Controllers\Api',
            'prefix'     => 'v2',
            'as'         => 'api::'
        ], function ($router) {
            require base_path('routes/api.php');
        });
    }

    protected function mapAdminRoutes()
    {
        Route::group([
            'middleware' => ['web'],
            'namespace'  => 'App\Http\Controllers\Admin',
            'prefix'     => 'admin',
            'as'         => 'admin::'
        ], function ($router) {
            require base_path('routes/admin.php');
        });
    }
}
