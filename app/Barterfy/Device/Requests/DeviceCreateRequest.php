<?php

namespace Barterfy\Device\Requests;

use Barterfy\Api\Requests\ApiRequest;

class DeviceCreateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'notification_token' => 'required',
            'device_platform' => 'required|in:ios,android,windows',
            'notify_on_matches' => 'boolean',
            'notify_on_new_message' => 'boolean'
        ];
    }
}
