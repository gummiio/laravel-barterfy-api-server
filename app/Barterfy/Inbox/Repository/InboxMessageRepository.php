<?php

namespace Barterfy\Inbox\Repository;

use Barterfy\Inbox\Model\Inbox;
use Barterfy\Inbox\Model\InboxMessage;
use Barterfy\Repository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InboxMessageRepository extends Repository
{
    public static function CreateSystemMessage($messageBody, $messageType)
    {
        return new InboxMessage([
            'type' => $messageType,
            'message' => $messageBody
        ]);
    }

    public static function CreateMessageFromInput(Request $request)
    {
        $message = new InboxMessage([
            'message' => $request->message
        ]);

        return $message;
    }

    public static function GetInboxMessages(Inbox $inbox, Request $request)
    {
        $query = $inbox->messages()
            ->limit($request->input('limit', 10))
            ->orderBy('created_at', 'desc');

        if ($request->has('before')) {
            $before = Carbon::createFromFormat('Y-m-d H:i:s', $request->before);
            $query->where('created_at', '<' , $before);
        }

        return $query->get();
    }

    public static function GetLatestUserMessages($take = 10)
    {
        return InboxMessage::whereNotNull('user_id')
            ->select('inbox_messages.*')
            ->with('inbox', 'user')
            ->join('inboxes', 'inbox_messages.inbox_id', '=', 'inboxes.id')
            ->where('inboxes.is_support', false)
            ->whereNull('inboxes.deleted_at')
            ->latest()
            ->take($take)
            ->get();
    }

    public static function GetLatestSupportMessage($take = 10)
    {
        return InboxMessage::whereNotNull('user_id')
            ->select('inbox_messages.*')
            ->with('inbox', 'user')
            ->join('inboxes', 'inbox_messages.inbox_id', '=', 'inboxes.id')
            ->where('inboxes.is_support', true)
            ->whereNull('inboxes.deleted_at')
            ->latest()
            ->take($take)
            ->get();
    }
}
