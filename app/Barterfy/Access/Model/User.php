<?php

namespace Barterfy\Access\Model;

use Barterfy\Access\Model\Relationships\UserRelationships;
use Barterfy\Access\Model\Scopes\UserScopes;
use Barterfy\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\SoftDeletes;
use Tymon\JWTAuth\Facades\JWTAuth;

class User extends Model implements AuthenticatableContract
{
    use Authenticatable, SoftDeletes, UserRelationships, UserScopes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'first_name', 'middle_name', 'last_name', 'age', 'gender',
        'avatar', 'country', 'city', 'longitude', 'latitude'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'username', 'password', 'email', 'api_token',
        'first_name', 'middle_name', 'last_name', 'facebook_token',
        'age', 'gender', 'deleted_at', 'updated_at', 'pivot'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['last_login', 'deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'is_you' => 'boolean',
        'longitude' => 'float',
        'latitude' => 'float',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['is_you', 'display_name', 'auth_token'];

    /**
     * Get is_you attribute
     *
     * @return string
     */
    public function getIsYouAttribute()
    {
        return $this->id == Auth()->id();
    }

    /**
     * Get full_name attribute
     *
     * @return string
     */
    public function getFullNameAttribute()
    {
        $full_name = implode(' ', [
            $this->first_name, $this->middle_name, $this->last_name
        ]);

        return preg_replace('#\s{2,}#', ' ', $full_name);
    }

    /**
     * Get display_name attribute
     *
     * @return string
     */
    public function getDisplayNameAttribute()
    {
        return $this->first_name;
    }

    /**
     * Get auth_token attribute
     *
     * @return string
     */
    public function getAuthTokenAttribute()
    {
        return base64_encode(":{$this->api_token}");
    }

    /**
     * Get jwt attribute
     *
     * @return  string
     */
    public function getJwtAttribute()
    {
        return JWTAuth::fromUser($this, [
            'id' => $this->id,
            'username' => $this->username,
            'api_token' => $this->api_token,
            'display_name' => $this->display_name,
            'full_name' => $this->full_name
        ]);
    }

    public function getIsFakeAttribute()
    {
        return str_contains($this->username, 'fake_');
    }

    public function getFbProfileLinkAttribute()
    {
        return sprintf(
            'https://www.facebook.com/app_scoped_user_id/%s/',
            str_replace('fb_', '', $this->username)
        );
    }

    public function getProfileUrl()
    {
        return url('admin/users/' . $this->id);
    }
}
