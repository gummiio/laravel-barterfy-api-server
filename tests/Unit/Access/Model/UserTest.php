<?php

namespace Unit\Access\Model;

use Barterfy\Access\Model\User;
use Unit\UnitTestCase;

class UserTest extends UnitTestCase
{
    /** @test */
    public function it_formats_user_full_name()
    {
        $user = factory(User::class)->make([
            'first_name' => 'John',
            'last_name' => 'Doe'
        ]);

        $this->assertEquals('John Doe', $user->full_name);
    }

    /** @test */
    public function it_formats_user_full_name_with_middle_name()
    {
        $user = factory(User::class)->make([
            'first_name' => 'John',
            'middle_name' => 'Jane',
            'last_name' => 'Doe'
        ]);

        $this->assertEquals('John Jane Doe', $user->full_name);
    }

    /** @test */
    public function display_name_should_only_show_first_name()
    {
        $user = factory(User::class)->make([
            'first_name' => 'John',
            'last_name' => 'Doe'
        ]);

        $this->assertEquals('John', $user->display_name);
    }

    /** @test */
    public function it_returns_auth_token()
    {
        $user = factory(User::class)->make([
            'api_token' => 'some-random-string'
        ]);

        $this->assertEquals(base64_encode(':some-random-string'), $user->auth_token);
    }
}
