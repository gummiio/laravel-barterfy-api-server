<?php

namespace Barterfy\Item\Repository;

use Barterfy\Access\Model\User;
use Barterfy\Item\Model\Item;
use Barterfy\Repository;
use Illuminate\Http\Request;

class ItemRepository extends Repository
{
    public static function GetItemsByUser(User $user, Request $request)
    {
        $query = $user->items()
            ->where('is_traded', $request->input('traded', false))
            ->orderBy('id', 'desc')
            ->limit($request->input('limit', 10));

        if ($request->has('readied')) {
            $query->where('is_readied', $request->input('readied'));
        }

        if ($request->has('from')) {
            $query->where('id', '<', $request->input('from'));
        }

        return $query->get();
    }

    public static function CreateItemFromInput(Request $request)
    {
        $item = new Item([
            'description' => $request->description
        ]);

        return $item;
    }

    public static function SearchItemsForUser(User $user, Request $request)
    {
        $limit = $request->input('limit', 20);
        $radius = $request->input('radius', 30); // ~30km = west van to east couqitlam = ~19 miles
        $unitNumber = $request->input('unit', 'kms') === 'kms'? 6371 : 3959;

        $query = Item::query();

        if ($radius >= 0) {
            # https://laracasts.com/discuss/channels/eloquent/haversine-query-using-eloquent?page=1
            # miles    http://www.onlineconversion.com/map_greatcircle_distance.htm
            $query = Item::select('items.*')
                ->join('users', 'items.user_id', '=', 'users.id')
                ->selectRaw('( ? * acos( cos( radians(?) ) *
                    cos( radians( latitude ) )
                    * cos( radians( longitude ) - radians(?)
                    ) + sin( radians(?) ) *
                    sin( radians( latitude ) ) )
                    ) AS distance',
                    [$unitNumber, $user->latitude, $user->longitude, $user->latitude]
                )
                ->havingRaw("distance <= ?", [$radius])
                ->whereNull('users.deleted_at');
        }

        if ($request->input('unmatch_only', true)) {
            $query->whereNotIn('items.user_id', function($query) use ($user) {
                $query->select('user_id')
                    ->from('inbox_users')
                    ->whereIn('inbox_id', function($query) use ($user) {
                        $query->select('inboxes.id')
                            ->from('inboxes')
                            ->join('inbox_users', 'inbox_users.inbox_id', '=', 'inboxes.id')
                            ->whereNull('inboxes.match_result')
                            ->where('inbox_users.user_id', $user->id);
                    })
                    ->where('user_id', '!=', $user->id);
            });
        }

        if ($request->input('excludes', [])) {
            $query->whereNotIn('items.id', $request->excludes);
        }

        $query->whereNotIn('items.id', function ($query) use ($user) {
            $query->select('item_id')
                ->from('item_likes')
                ->where('user_id', $user->id);
        });

        return $query->where('items.user_id', '!=', $user->id)
            ->notTraded()
            ->readied()
            ->inRandomOrder()
            ->limit($limit)
            ->get();
    }
}
