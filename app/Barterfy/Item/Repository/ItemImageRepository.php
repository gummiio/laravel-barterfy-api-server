<?php

namespace Barterfy\Item\Repository;

use Barterfy\Access\Model\User;
use Barterfy\Item\Model\ItemImage;
use Barterfy\Repository;
use Illuminate\Http\Request;

class ItemImageRepository extends Repository
{
    public static function CreateImagesFromInput(Request $request)
    {
        $images = [];

        foreach ($request->file('images') as $image) {
            $filename = sha1(microtime()) . '.' . $image->guessClientExtension();
            $path = storage_path() . '/temp/';

            $images[] = new ItemImage([
                'cloud_index' => App('CloudinaryHelper')::randomCloud(),
                'path' => $path . '/' .$filename
            ]);

            // laravel bug @5.2.39, middleware termiate will freak out
            copy($image->getPathname(), $path . $filename);
            // $image->move($path, $filename);
        }

        return $images;
    }
}
