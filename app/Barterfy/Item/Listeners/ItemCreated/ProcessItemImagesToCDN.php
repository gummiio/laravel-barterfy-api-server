<?php

namespace Barterfy\Item\Listeners\ItemCreated;

use Barterfy\Item\Events\ItemCreated;
use Barterfy\Item\Events\ItemIsReadied;
use Barterfy\Item\Model\Item;
use Barterfy\Item\Model\Scopes\ItemIsReadiedScope;
use Illuminate\Contracts\Queue\ShouldQueue;

class ProcessItemImagesToCDN implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemCreated  $event
     * @return void
     */
    public function handle(ItemCreated $event)
    {
        $item = Item::find($event->itemId);

        $item->images->each(function ($image) {
            try {
                $upload = App('CloudinaryHelper')::init($image->path, $image->cloud_index)
                    ->width('1000')
                    ->height('1000')
                    ->tags(env('CLOUDINARY_UPLOAD_TAG', ''))
                    ->format('jpg')
                    ->upload(md5(microtime()), ['crop' => 'thumb']);

                $image->deleteLocalCopy();

                $image->public_id = $upload['public_id'];
                $image->resource_type = $upload['resource_type'];
                $image->url = $upload['secure_url'];
                $image->path = null;
                $image->save();

            } catch (\Exception $e) {
                // todo
                dd($e->getMessage());
            }
        });

        $item->readied();

        event(new ItemIsReadied($item, $event->shouldNotify));
    }
}
