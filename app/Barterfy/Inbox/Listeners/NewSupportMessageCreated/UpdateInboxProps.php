<?php

namespace Barterfy\Inbox\Listeners\NewSupportMessageCreated;

use Barterfy\Inbox\Events\NewSupportMessageCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateInboxProps implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewSupportMessageCreated  $event
     * @return void
     */
    public function handle(NewSupportMessageCreated $event)
    {
        $message = $event->message;
        $inbox = $message->inbox;

        $message = $message->fresh()->load('user');

        $inbox->total_messages = $inbox->total_messages + 1;
        $inbox->last_message = $message->toArray();
        $inbox->save();
    }
}
