<?php

namespace Barterfy\Device\Requests;

use Barterfy\Api\Requests\ApiRequest;

class DeviceUpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->route('devices')->user_id === Auth()->id();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'notify_on_matches' => 'boolean',
            'notify_on_new_message' => 'boolean'
        ];
    }
}
