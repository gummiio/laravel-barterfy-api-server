<?php

namespace Integration\Item\Events;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Item\Events\ItemLiked;
use Barterfy\Item\Model\Item;
use Integration\IntegrationTestCase;

class ItemLikedTest extends IntegrationTestCase
{
    /** @test */
    public function it_should_not_match_if_users_are_already_matching()
    {
        $other = factory(User::class)->create();
        $you = factory(User::class)->create();

        $inbox = factory(Inbox::class)->create(['match_result' => 'null']);
        $inbox->users()->saveMany([$other, $you]);

        $item = $you->items()->save(
            factory(Item::class)->make(['is_readied' => true])
        );

        $itemLike = $item->likedBy($other);

        $this->doesntExpectEvents(MatchInboxCreated::class);

        $this->assertCount(1, Inbox::all());

        event(new ItemLiked($itemLike));

        $this->assertCount(1, Inbox::all());
    }

    /** @test */
    public function it_should_not_match_if_other_user_has_nothing_they_liked()
    {
        $other = factory(User::class)->create();
        $you = factory(User::class)->create();

        $item = $you->items()->save(
            factory(Item::class)->make(['is_readied' => true])
        );

        $itemLike = $item->likedBy($other);

        $this->doesntExpectEvents(MatchInboxCreated::class);

        $this->assertCount(0, Inbox::all());

        event(new ItemLiked($itemLike));

        $this->assertCount(0, Inbox::all());
    }

    /** @test */
    public function inbox_should_be_created_if_both_like_each_others_stuff()
    {
        $other = factory(User::class)->create();
        $you = factory(User::class)->create();

        $yourItem = $you->items()->save(
            factory(Item::class)->make(['is_readied' => true])
        );

        $otherItem = $other->items()->save(
            factory(Item::class)->make(['is_readied' => true])
        );

        $otherItem->likedBy($you);
        $itemLike = $yourItem->likedBy($other);

        $this->assertCount(0, Inbox::all());

        event(new ItemLiked($itemLike));

        $this->assertCount(1, Inbox::all());
    }
}
