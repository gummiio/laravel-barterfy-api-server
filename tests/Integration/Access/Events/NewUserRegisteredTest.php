<?php

namespace Integration\Access\Events;

use Barterfy\Access\Events\NewUserRegistered;
use Barterfy\Access\Model\User;
use Integration\IntegrationTestCase;

class NewUserRegisteredTest extends IntegrationTestCase
{
    /** @test */
    public function a_support_inbox_will_be_created_upon_registration()
    {
        $user = factory(User::class)->create();

        event(new NewUserRegistered($user));

        $this->assertCount(1, $user->inboxes()->support()->get());
    }
}
