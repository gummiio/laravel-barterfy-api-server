<?php

namespace Barterfy\Inbox\Model\Relationships;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Inbox\Model\InboxMessageStatus;

trait InboxMessageRelationships
{
    /**
     * A message belongs to an inbox
     *
     * @return relationship object
     */
    public function inbox()
    {
        return $this->belongsTo(Inbox::class);
    }

    /**
     * A message belongs to an user
     *
     * @return relationship object
     */
    public function user()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * A message will contains read status
     *
     * @return relationship object
     */
    public function statuses()
    {
        return $this->hasMany(InboxMessageStatus::class);
    }
}
