<?php

namespace Barterfy\Item\Model\Relationships;

use Barterfy\Access\Model\User;
use Barterfy\Item\Model\Item;

trait ItemLikeRelationships
{
    /**
     * A like status has a item
     *
     * @return relation object
     */
    public function item()
    {
        return $this->belongsTo(Item::class);
    }

    /**
     * A image belones to a user
     *
     * @return relation object
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
