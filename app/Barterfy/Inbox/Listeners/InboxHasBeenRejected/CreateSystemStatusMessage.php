<?php

namespace Barterfy\Inbox\Listeners\InboxHasBeenRejected;

use Barterfy\Inbox\Events\InboxHasBeenRejected;
use Barterfy\Inbox\Events\NewMessageCreated;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSystemStatusMessage implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InboxHasBeenRejected  $event
     * @return void
     */
    public function handle(InboxHasBeenRejected $event)
    {
        $acceptance = $event->acceptance;
        $inbox = $acceptance->inbox;
        $accepter = $acceptance->user;

        $message = $inbox->systemMessage(
            $accepter->display_name . ' has rejected this trade.'
        );

        event(new NewMessageCreated($message));
    }
}
