<?php

namespace Barterfy\Access\Events;

use App\Events\Event;
use Barterfy\Access\Model\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class NewUserRegistered extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * User Object
     *
     * @var object
     */
    public $user;
    public $shouldNotify;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user, $shouldNotify = true)
    {
        $this->user = $user;
        $this->shouldNotify = $shouldNotify;
    }
}
