<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminAccessTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 191)->unique();
            $table->string('password', 191);
            $table->string('email', 191);
            $table->string('api_token', 191);

            $table->string('first_name', 191)->nullable()->default(null);
            $table->string('middle_name', 191)->nullable()->default(null);
            $table->string('last_name', 191)->nullable()->default(null);
            $table->string('avatar', 191)->nullable()->default(null);

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_users');
    }
}
