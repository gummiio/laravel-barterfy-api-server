<?php

namespace Barterfy\Api\Transformer;

use Barterfy\Api\Transformer\Transformer;

class HiddenColumnTransformer extends Transformer
{
    public function transform($columns = [])
    {
        foreach ($columns as $column) {
            if (array_key_exists($column, $this->input)) {
                unset($this->input[$column]);
            }
        }

        return $this;
    }
}
