<?php

namespace Barterfy\Inbox\Requests;

use Barterfy\Api\Requests\ApiRequest;

class InboxMessageCreateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->route('inboxes')->users->where('id', Auth()->id())->count();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'message' => 'required'
        ];
    }
}
