<?php

namespace Barterfy\Inbox\Model;

use Barterfy\Model;

class InboxMessageStatus extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inbox_message_statuses';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['is_read'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $casts = [
        'is_read' => 'boolean'
    ];
}
