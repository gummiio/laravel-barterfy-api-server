<?php

namespace Acceptance\Devices;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Device\Model\Device;

class IndexTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->get('v2/devices', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_will_retreive_your_devices()
    {
        $other = factory(User::class)->create();
        $other->devices()->saveMany(
            factory(Device::class, 5)->make()
        );

        $you = factory(User::class)->create();
        $you->devices()->save(
            factory(Device::class)->make()
        );

        $this->get('v2/devices', $this->headers($you));

        $this->seeJson(['success' => true]);
        $this->seeJsonStructure(['*' => []]);

        $this->assertResponseStatus(200);

        $this->assertCount(1, $this->jsonData());
    }

    /** @test */
    public function it_should_not_retreive_deleted_devices()
    {
        $you = factory(User::class)->create();
        $you->devices()->saveMany(
            factory(Device::class, 3)->make()
        );

        $you->devices()->first()->delete();

        $this->get('v2/devices', $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->assertCount(2, $this->jsonData());
    }

    /** @test */
    public function it_should_include_deleted_if_params_is_passed()
    {
        $you = factory(User::class)->create();
        $you->devices()->saveMany(
            factory(Device::class, 2)->make()
        );

        $you->devices()->first()->delete();

        $this->get('v2/devices?deleted=1', $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->assertCount(2, $this->jsonData());
    }

    /** @test */
    public function it_could_also_filter_by_device_platform()
    {
        $you = factory(User::class)->create();
        $you->devices()->saveMany(
            factory(Device::class, 4)->make(['device_platform' => 'ios'])
        );

        $you->devices()->saveMany(
            factory(Device::class, 5)->make(['device_platform' => 'android'])
        );

        $this->get('v2/devices?platform=ios', $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->assertCount(4, $this->jsonData());

        $this->get('v2/devices?platform=android', $this->headers($you));

        $this->assertCount(5, $this->jsonData());
    }
}
