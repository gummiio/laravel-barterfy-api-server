<?php

namespace Barterfy\Inbox\Providers;

use Barterfy\Traits\Dispatchable;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Support\ServiceProvider;

class InboxServiceProvider extends ServiceProvider
{
    use Dispatchable;

    public $serviceEvents = [
        \Barterfy\Inbox\Events\SupportInboxCreated::class => [
            \Barterfy\Inbox\Listeners\SupportInboxCreated\CreateSystemStatusMessage::class,
        ],
        \Barterfy\Inbox\Events\MatchInboxCreated::class => [
            \Barterfy\Inbox\Listeners\MatchInboxCreated\NotifyOnSlack::class,
            \Barterfy\Inbox\Listeners\MatchInboxCreated\CreateSystemStatusMessage::class,
            // \Barterfy\Inbox\Listeners\MatchInboxCreated\SendPushNotification::class,
        ],
        \Barterfy\Inbox\Events\InboxHasBeenAccepted::class => [
            \Barterfy\Inbox\Listeners\InboxHasBeenAccepted\CreateSystemStatusMessage::class,
            \Barterfy\Inbox\Listeners\InboxHasBeenAccepted\CheckIfAllAccepted::class
        ],
        \Barterfy\Inbox\Events\InboxHasBeenRejected::class => [
            \Barterfy\Inbox\Listeners\InboxHasBeenRejected\CreateSystemStatusMessage::class,
            \Barterfy\Inbox\Listeners\InboxHasBeenRejected\TerminateInbox::class
        ],
        \Barterfy\Inbox\Events\AcceptanceHasBeenRegreted::class => [
            \Barterfy\Inbox\Listeners\AcceptanceHasBeenRegreted\CreateSystemStatusMessage::class
        ],
        \Barterfy\Inbox\Events\InboxTradeCompleted::class => [
            \Barterfy\Inbox\Listeners\InboxTradeCompleted\NotifyOnSlack::class,
            \Barterfy\Inbox\Listeners\InboxTradeCompleted\UpdateItemsTradedStatus::class,
            \Barterfy\Inbox\Listeners\InboxTradeCompleted\CreateSystemStatusMessage::class,
        ],

        // messages
        \Barterfy\Inbox\Events\NewSupportMessageCreated::class => [
            \Barterfy\Inbox\Listeners\NewSupportMessageCreated\SendPushNotification::class,
            \Barterfy\Inbox\Listeners\NewSupportMessageCreated\UpdateInboxProps::class,
            \Barterfy\Inbox\Listeners\NewSupportMessageCreated\NotifyOnSlack::class,
        ],
        \Barterfy\Inbox\Events\NewMessageCreated::class => [
            \Barterfy\Inbox\Listeners\NewMessageCreated\LogOnSlack::class,
            \Barterfy\Inbox\Listeners\NewMessageCreated\CreateUnreadStatus::class,
            \Barterfy\Inbox\Listeners\NewMessageCreated\UpdateInboxProps::class,
            \Barterfy\Inbox\Listeners\NewMessageCreated\SendPushNotification::class,
        ],
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(DispatcherContract $dispatcher)
    {
        $this->dispatchServiceEvents($dispatcher);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
