<?php

namespace Barterfy\Inbox\Model;

use Barterfy\Inbox\Model\Relationships\InboxMessageRelationships;
use Barterfy\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InboxMessage extends Model
{
    use SoftDeletes, InboxMessageRelationships;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inbox_messages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['type', 'message'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'deleted_at', 'statuses'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['is_system', 'diff_for_humans', 'read_by'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['user', 'statuses'];

    /**
     * Get is_system attribute
     *
     * @return boolean
     */
    public function getIsSystemAttribute()
    {
        return is_null($this->user_id);
    }

    /**
     * Get read_by attribute
     *
     * @return boolean
     */
    public function getReadByAttribute()
    {
        return $this->statuses->filter(function($status) {
            return $status->is_read === true;
        })->pluck('id')->all();
    }

    public function getDiffForHumansAttribute()
    {
        return $this->created_at->diffForHumans();
    }
}
