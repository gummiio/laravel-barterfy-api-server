<?php

namespace Acceptance\Users;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;

class StatsTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->get('v2/users/stats', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_not_include_support_inbox_for_matches_count()
    {
        $you = factory(User::class)->create();

        $you->inboxes()->saveMany(
            factory(Inbox::class, 5)->make()
        );

        $you->inboxes()->save(
            factory(Inbox::class)->make(['is_support' => true])
        );

        $this->assertCount(6, $you->inboxes);

        $this->get('v2/users/stats', $this->headers($you));

        $this->seeJson(['success' => true]);
        $this->seeJson(['matches' => 5]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function it_should_not_include_deleted_inboxes()
    {
        $you = factory(User::class)->create();

        $you->inboxes()->saveMany(
            factory(Inbox::class, 5)->make()
        );

        $you->inboxes()->first()->delete();

        $this->get('v2/users/stats', $this->headers($you));

        $this->seeJson(['success' => true]);
        $this->seeJson(['matches' => 4]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function it_return_proper_inboxes_counts()
    {
        $you = factory(User::class)->create();
        $other = factory(User::class)->create();

        $you->inboxes()->saveMany(
            factory(Inbox::class, 3)->make(['match_result' => null])
        );

        $you->inboxes()->saveMany(
            factory(Inbox::class, 4)->make(['match_result' => true])
        );

        $you->inboxes()->saveMany(
            factory(Inbox::class, 5)->make(['match_result' => false])
        );

        $other->inboxes()->saveMany(
            factory(Inbox::class, rand(5, 10))->make(['match_result' => null])
        );

        $other->inboxes()->saveMany(
            factory(Inbox::class, rand(5, 10))->make(['match_result' => true])
        );

        $other->inboxes()->saveMany(
            factory(Inbox::class, rand(5, 10))->make(['match_result' => false])
        );

        $this->get('v2/users/stats', $this->headers($you));

        $this->seeJson(['success' => true]);
        $this->seeJson(['matches' => 12]);
        $this->seeJson(['matching' => 3]);
        $this->seeJson(['successed' => 4]);
        $this->seeJson(['failed' => 5]);

        $this->assertResponseStatus(200);
    }
}
