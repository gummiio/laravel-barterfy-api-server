<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Barterfy\Device\Events\DeviceCreated;
use Barterfy\Device\Events\DeviceDeleted;
use Barterfy\Device\Events\DeviceUpdated;
use Barterfy\Device\Model\Device;
use Barterfy\Device\Repository\DeviceRepository;
use Barterfy\Device\Requests\DeviceCreateRequest;
use Barterfy\Device\Requests\DeviceDeleteRequest;
use Barterfy\Device\Requests\DeviceListRequest;
use Barterfy\Device\Requests\DeviceShowRequest;
use Barterfy\Device\Requests\DeviceUpdateRequest;
use Illuminate\Http\Request;
use PushNotification;

class DeviceController extends ApiController
{
    public function index(DeviceListRequest $request)
    {
        $devices = DeviceRepository::GetDevicesByUser(Auth()->user(), $request);

        return response()->apiSuccess('', $devices);
    }

    public function store(DeviceCreateRequest $request)
    {
        if ($device = DeviceRepository::GetDeviceByNotificationToken($request->notification_token)) {
            if ($device->user_id == Auth()->id()) {
                return response()->apiSuccess('Device Exists', $device, 200);
            }

            $device->delete();
        }

        $device = DeviceRepository::CreateDeviceFromInput($request);
        Auth()->user()->devices()->save($device);

        event(new DeviceCreated($device));

        return response()->apiSuccess('Device registered.', $device->fresh(), 201);
    }

    public function show(DeviceShowRequest $request, Device $device)
    {
        return response()->apiSuccess('', $device);
    }

    public function update(DeviceUpdateRequest $request, Device $device)
    {
        $device->fill(array_filter($request->only(Device::$SwitchableSettings)));

        $device->save();

        event(new DeviceUpdated($device));

        return response()->apiSuccess('Device updated.', $device, 200);
    }

    public function destroy(DeviceDeleteRequest $request, Device $device)
    {
        $device->delete();

        event(new DeviceDeleted($device));

        return response()->apiSuccess('Device deleted.', [], 200);
    }

    public function send_test(Request $request)
    {
        $this->validate($request, [
            'push_device' => 'required|in:ios,android',
            'device_token' => 'required'
        ]);

        try {
            $pushMessage = PushNotification::Message(
                $request->input('push_text', 'This is a test notification')
            );

            $result = PushNotification::app($request->push_device)
                    ->to($request->device_token)
                    ->send($pushMessage);

            return response()->apiSuccess('Sent successfully.', $result->pushManager->getIterator()->offsetGet(0)->getStatus());
        } catch (\Exception $e) {
            return response()->apiError('Sent error.', $e->getMessage(), $e->getCode()? : 500);
        }

    }
}
