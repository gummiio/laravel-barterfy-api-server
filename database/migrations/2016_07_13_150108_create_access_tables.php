<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccessTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username', 191);
            $table->string('password', 191);
            $table->string('email', 191)->nullable()->default(null);
            $table->string('api_token', 191);

            $table->string('first_name', 191)->nullable()->default(null);
            $table->string('middle_name', 191)->nullable()->default(null);
            $table->string('last_name', 191)->nullable()->default(null);
            $table->text('facebook_token')->nullable()->default(null);
            $table->string('avatar', 191)->nullable()->default(null);
            $table->string('age', 191)->nullable()->default(null);
            $table->string('gender', 191)->nullable()->default(null);

            $table->string('country', 191)->nullable()->default(null);
            $table->string('city', 191)->nullable()->default(null);
            $table->float('longitude', 20, 8)->nullable()->default(null);
            $table->float('latitude', 20, 8)->nullable()->default(null);

            $table->boolean('online')->default(false);
            $table->timestamp('last_login')->nullable()->default(null);
            $table->softDeletes();
            $table->timestamps();

            $table->unique(['username', 'deleted_at']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
