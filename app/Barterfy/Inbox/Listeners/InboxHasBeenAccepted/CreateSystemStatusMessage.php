<?php

namespace Barterfy\Inbox\Listeners\InboxHasBeenAccepted;

use Barterfy\Inbox\Events\InboxHasBeenAccepted;
use Barterfy\Inbox\Events\NewMessageCreated;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSystemStatusMessage implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InboxHasBeenAccepted  $event
     * @return void
     */
    public function handle(InboxHasBeenAccepted $event)
    {
        $acceptance = $event->acceptance;
        $inbox = $acceptance->inbox;
        $accepter = $acceptance->user;

        $message = $inbox->systemMessage(
            $accepter->display_name . ' has indicated that this trade has been completed. Please press the "i" icon and click Trade Completed to close the trade.'
        );

        event(new NewMessageCreated($message));
    }
}
