<?php

namespace Barterfy\Api\Middleware;

use Barterfy\Api\Events\LongApiRespondTimeExecuted;
use Barterfy\Api\Jobs\LogApiRequest;
use Barterfy\Api\Repository\ApiLogRepository;
use Carbon\Carbon;
use Closure;
use Config;

class ApiLogging
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    /**
     * Log the request to the database
     *
     * @param  $request
     * @param  $response
     * @return void
     */
    public function terminate($request, $response)
    {
        $apiGuard = app('ApiGuard');

        if ($apiGuard->verifiedApiToken()) {
            $logData = ApiLogRepository::generateDataFromCurrentSession($request, $response);
            dispatch(new LogApiRequest($logData, Carbon::now()));
        }
    }
}
