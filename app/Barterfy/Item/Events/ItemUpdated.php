<?php

namespace Barterfy\Item\Events;

use App\Events\Event;
use Barterfy\Item\Model\Item;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class ItemUpdated extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * Item Object
     *
     * @var object
     */
    public $item;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Item $item)
    {
        $this->item = $item;
    }
}
