<?php

namespace Acceptance\Items;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Item\Events\ItemUpdated;
use Barterfy\Item\Model\Item;

class UpdateTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_return_resource_not_found_even_unauthenticate()
    {
        $this->put('v2/items/1', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate_when_item_exists()
    {
        $user = factory(User::class)->create();
        $user->items()->save(
            factory(Item::class)->make()
        );

        $this->put('v2/items/1', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_id_does_not_exists()
    {
        $user = factory(User::class)->create();

        $this->put('v2/items/1', [], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_failed_if_item_does_not_belong_to_you()
    {
        $other = factory(User::class)->create();
        $other->items()->save(
            factory(Item::class)->make()
        );

        $you = factory(User::class)->create();

        $this->put('v2/items/1', [], $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function should_successfully_update_when_item_exists_and_belong_to_you()
    {
        $you = factory(User::class)->create();
        $you->items()->save(
            factory(Item::class)->make([
                'description' => 'abcde'
            ])
        );

        $this->expectsEvents(ItemUpdated::class);

        $this->put('v2/items/1', [
            'description' => '12345'
        ], $this->headers($you));

        $this->seeJson(['success' => true]);
        $this->seeJson(['description' => '12345']);

        $this->assertResponseStatus(200);
    }
}
