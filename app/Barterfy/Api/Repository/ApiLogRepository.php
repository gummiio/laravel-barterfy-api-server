<?php

namespace Barterfy\Api\Repository;

use Barterfy\Api\Model\ApiLog;
use Barterfy\Repository;

class ApiLogRepository extends Repository
{

    public static function generateDataFromCurrentSession($request, $response)
    {
        $apiGuard = app('ApiGuard');
        $authType = $apiGuard->getApiKey()->is_admin? 'admin' : 'app';

        $header = collect($request->header())
            ->except(['cookie', 'php-auth-user', 'php-auth-pw']);

        $logData = [
            'api_key_id'   => $apiGuard->getApiKey()->id,
            'endpoint'     => $request->path(),
            'method'       => $request->method(),
            'params'       => json_encode($request->except('api_token')),
            'ip'           => $request->ip(),
            'user_agent'   => $request->server('HTTP_USER_AGENT'),
            'respond_code' => $response->getStatusCode(),
            'respond_time' => microtime(true) - LARAVEL_START
        ];

        if (! Auth($authType)->guest()) {
            $logData['auth_user_id'] = Auth($authType)->id();
        }

        // $logData->save();

        return $logData;
    }
}
