<?php

namespace Barterfy\Inbox\Model;

use Barterfy\Inbox\Model\Relationships\InboxAcceptanceRelationships;
use Barterfy\Model;

class InboxAcceptance extends Model
{
    use InboxAcceptanceRelationships;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inbox_acceptances';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['accepted'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $casts = [
        'accepted' => 'boolean'
    ];
}
