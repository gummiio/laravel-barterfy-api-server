<?php

namespace Barterfy\Item\Listeners\ItemLiked;

use Barterfy\Inbox\Events\MatchInboxCreated;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Inbox\Repository\InboxRepository;
use Barterfy\Item\Events\ItemLiked;
use Barterfy\Item\Repository\ItemLikeRepository;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckIfMatchOccurred implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemLiked  $event
     * @return void
     */
    public function handle(ItemLiked $event)
    {
        $itemLike = $event->itemLike;
        $item = $itemLike->item;
        $owner = $item->user;
        $liker = $itemLike->user;

        if (InboxRepository::UsersHaveActiveInbox([$owner, $liker])) {
            return false; // bail earily
        }

        if (! $yourItem = ItemLikeRepository::UserItemsLikedBy($owner, $liker)) {
            return false;
        }

        $inbox = Inbox::create([
            'name' => $liker->display_name . ' & ' . $owner->display_name
        ]);

        $inbox->items()->saveMany([$item, $yourItem]);
        $inbox->users()->saveMany([$owner, $liker]);

        event(new MatchInboxCreated($inbox));
    }
}
