<?php

namespace Barterfy\Access\Model\Relationships;

use Barterfy\Api\Model\ApiLog;
use Barterfy\Device\Model\Device;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Item\Model\Item;
use Barterfy\Item\Model\ItemLike;

trait UserRelationships
{
    /**
     * A User have multiple api logs
     *
     * @return relation object
     */
    public function apiLogs()
    {
        return $this->hasMany(ApiLog::class, 'auth_user_id');
    }

    /**
     * A User can have mutiple device registered
     *
     * @return relation object
     */
    public function devices()
    {
        return $this->hasMany(Device::class);
    }

    /**
     * A User can post multiple items
     *
     * @return relation object
     */
    public function items()
    {
        return $this->hasMany(Item::class);
    }

    /**
     * A User like multiple items
     *
     * @return relation object
     */
    public function itemLikes()
    {
        return $this->hasMany(ItemLike::class);
    }

    /**
     * A User have multiple inboxes
     *
     * @return relation object
     */
    public function inboxes()
    {
        return $this->belongsToMany(Inbox::class, 'inbox_users');
    }


}
