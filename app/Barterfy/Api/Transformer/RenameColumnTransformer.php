<?php

namespace Barterfy\Api\Transformer;

use Barterfy\Api\Transformer\Transformer;

class RenameColumnTransformer extends Transformer
{
    public function transform($columns = [])
    {
        foreach ($columns as $column => $newKey) {
            if (array_key_exists($column, $this->input)) {
                $this->input[$newKey] = $this->input[$column];
                unset($this->input[$column]);
            }
        }

        return $this;
    }
}
