<?php

namespace Acceptance\Items;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Item\Model\Item;

class IndexTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->get('v2/items', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_will_retreive_your_itemss()
    {
        $other = factory(User::class)->create();
        $other->items()->saveMany(
            factory(Item::class, 5)->make()
        );

        $you = factory(User::class)->create();
        $you->items()->save(
            factory(Item::class)->make()
        );

        $this->get('v2/items', $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->assertEquals(1, count($this->jsonData()));
    }

    /** @test */
    public function it_should_not_include_traded_items()
    {
        $user = factory(User::class)->create();
        $user->items()->saveMany(
            factory(Item::class,5)->make(['is_traded' => false])
        );

        $user->items()->first()->traded();

        $this->get('v2/items', $this->headers($user));

        $this->assertEquals(4, count($this->jsonData()));
    }

    /** @test */
    public function it_should_include_both_readied_and_not_readied_items()
    {
        $user = factory(User::class)->create();
        $user->items()->saveMany(
            factory(Item::class,2)->make(['is_readied' => false])
        );

        $user->items()->saveMany(
            factory(Item::class,3)->make(['is_readied' => true])
        );

        $this->get('v2/items', $this->headers($user));

        $this->assertEquals(5, count($this->jsonData()));
    }

    /** @test */
    public function it_should_limit_to_10()
    {
        $user = factory(User::class)->create();
        $user->items()->saveMany(
            factory(Item::class,50)->make()
        );

        $this->get('v2/items', $this->headers($user));

        $this->assertEquals(10, count($this->jsonData()));
    }

    /** @test */
    public function it_could_only_pull_the_traded_items()
    {
        $user = factory(User::class)->create();
        $user->items()->saveMany(
            factory(Item::class,3)->make(['is_traded' => false])
        );

        $user->items()->saveMany(
            factory(Item::class,2)->make(['is_traded' => true])
        );

        $this->get('v2/items?traded=1', $this->headers($user));

        $this->assertEquals(2, count($this->jsonData()));
    }

    /** @test */
    public function it_could_only_pull_the_readied_items()
    {
        $user = factory(User::class)->create();
        $user->items()->saveMany(
            factory(Item::class,3)->make(['is_readied' => false])
        );

        $user->items()->saveMany(
            factory(Item::class,2)->make(['is_readied' => true])
        );

        $this->get('v2/items?readied=1', $this->headers($user));

        $this->assertEquals(2, count($this->jsonData()));
    }

    /** @test */
    public function limit_can_be_overwrite()
    {
        $user = factory(User::class)->create();
        $user->items()->saveMany(
            factory(Item::class,50)->make()
        );

        $this->get('v2/items?limit=25', $this->headers($user));

        $this->assertEquals(25, count($this->jsonData()));
    }

    /** @test */
    public function can_retreive_from_item_id()
    {
        $user = factory(User::class)->create();

        $user->items()->saveMany(
            factory(Item::class,4)->make(['is_readied' => true, 'description' => 'should-see-me'])
        );

        $user->items()->saveMany(
            factory(Item::class,25)->make(['is_readied' => true, 'description' => 'should-not-see-me'])
        );

        $this->get('v2/items?from=5', $this->headers($user));

        $this->dontSeeJson(['description' => 'should-not-see-me']);
        $this->seeJson(['description' => 'should-see-me']);
        $this->assertEquals(4, count($this->jsonData()));
    }

    /** @test */
    public function stats_should_be_attached_as_extra_parameters()
    {
        $user = factory(User::class)->create();
        $user->items()->saveMany(
            factory(Item::class,25)->make()
        );

        $this->get('v2/items', $this->headers($user));

        $this->seeJsonStructure([
            'stats' => [
                'matches', 'matching', 'successed', 'failed'
            ]
        ]);
    }
}
