<?php

namespace Barterfy\Device\Model;

use Barterfy\Device\Model\Relationships\DeviceRelationships;
use Barterfy\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model
{
    use SoftDeletes, DeviceRelationships;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'devices';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'device_platform', 'system_name', 'system_version',
        'device_manufacturer', 'device_model', 'device_id', 'device_locale', 'device_name',
        'notify_on_matches', 'notify_on_new_message'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'deleted_at', 'updated_at',
        'system_name', 'system_version',
        'device_manufacturer', 'device_model', 'device_id', 'device_locale', 'device_name'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'notify_on_matches' => 'boolean',
        'notify_on_new_message' => 'boolean'
    ];

    /**
     * Settings that's allowed to be turned on and off on update request
     *
     * @var array
     */
    public static $SwitchableSettings = [
        'notify_on_matches',
        'notify_on_new_message'
    ];
}
