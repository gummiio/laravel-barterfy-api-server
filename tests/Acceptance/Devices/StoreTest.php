<?php

namespace Acceptance\Devices;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Device\Model\Device;

class StoreTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->post('v2/devices/', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_no_data_is_posted()
    {
        $user = factory(User::class)->create();

        $this->post('v2/devices/', [], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(422);
    }

    /** @test */
    public function is_shoud_failed_if_required_data_is_invalid()
    {
        $user = factory(User::class)->create();

        $this->post('v2/devices/', [
            'notification_token' => 'asdf',
            'device_platform' => 'beeper'
        ], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(422);
    }

    /** @test */
    public function it_should_create_device_with_required_field_filled()
    {
        $user = factory(User::class)->create();

        $this->post('v2/devices/', [
            'notification_token' => 'asdf',
            'device_platform' => 'ios'
        ], $this->headers($user));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(201);

        $this->assertCount(1, Device::all());

        $this->seeInDatabase('devices', ['notification_token' => 'asdf']);
    }

    /** @test */
    public function it_should_not_create_new_device_if_notification_token_exists_with_same_user()
    {
        $user = factory(User::class)->create();
        $user->devices()->save(
            factory(Device::class)->make(['notification_token' => 'asdf'])
        );

        $this->post('v2/devices/', [
            'notification_token' => 'asdf',
            'device_platform' => 'ios'
        ], $this->headers($user));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->assertCount(1, Device::all());
    }

    /** @test */
    public function it_should_delete_old_device_if_new_device_notification_token_matched_but_not_same_user()
    {
        $oldUser = factory(User::class)->create();
        $oldUser->devices()->save(
            factory(Device::class)->make(['notification_token' => 'asdf'])
        );

        $you = factory(User::class)->create();

        $this->post('v2/devices/', [
            'notification_token' => 'asdf',
            'device_platform' => 'ios'
        ], $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(201);

        $this->assertCount(1, Device::all());


        \DB::enableQueryLog();

        $this->seeInDatabase('devices', [
            'notification_token' => 'asdf',
            'user_id' => $you->id
        ]);

        $this->notSeeInDatabase('devices', [
            'notification_token' => 'asdf',
            'user_id' => $oldUser->id,
            'deleted_at' => null
        ]);
    }

    /** @test */
    public function device_will_be_created_if_matching_notification_token_is_deleted()
    {
        $other = factory(User::class)->create();
        $other->devices()->save(
            factory(Device::class)->make(['notification_token' => 'asdf'])
        );

        $other->devices()->first()->delete();

        $you = factory(User::class)->create();

        $this->post('v2/devices/', [
            'notification_token' => 'asdf',
            'device_platform' => 'ios'
        ], $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(201);

        $this->seeInDatabase('devices', [
            'notification_token' => 'asdf',
            'user_id' => $you->id,
            'deleted_at' => null
        ]);

        $this->seeInDatabase('devices', [
            'notification_token' => 'asdf',
            'user_id' => $other->id,
            ['deleted_at', '!=', null]
        ]);
    }
}
