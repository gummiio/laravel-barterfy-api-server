<?php

namespace Barterfy\Item\Model\Relationships;

use Barterfy\Access\Model\User;
use Barterfy\Item\Model\ItemImage;
use Barterfy\Item\Model\ItemLike;

trait ItemRelationships
{
    /**
     * A image has many images
     *
     * @return relation object
     */
    public function images()
    {
        return $this->hasMany(ItemImage::class);
    }

    /**
     * A image has many like status
     *
     * @return relation object
     */
    public function itemLikes()
    {
        return $this->hasMany(ItemLike::class);
    }

    public function itemLikesLike()
    {
        return $this->itemLikes()->where('liked', true);
    }

    public function itemLikesDislike()
    {
        return $this->itemLikes()->where('liked', false);
    }

    /**
     * A image belones to a user
     *
     * @return relation object
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
