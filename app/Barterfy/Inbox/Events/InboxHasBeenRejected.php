<?php

namespace Barterfy\Inbox\Events;

use App\Events\Event;
use Barterfy\Inbox\Model\InboxAcceptance;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class InboxHasBeenRejected extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * InboxAcceptance Object
     *
     * @var object
     */
    public $acceptance;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(InboxAcceptance $acceptance)
    {
        $this->acceptance = $acceptance;
    }
}
