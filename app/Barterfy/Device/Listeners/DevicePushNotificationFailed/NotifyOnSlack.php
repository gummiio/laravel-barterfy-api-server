<?php

namespace Barterfy\Device\Listeners\DevicePushNotificationFailed;

use Barterfy\Device\Events\DevicePushNotificationFailed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class NotifyOnSlack implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DevicePushNotificationFailed  $event
     * @return void
     */
    public function handle(DevicePushNotificationFailed $event)
    {
        $device = $event->device;
        $error = $event->error;
        $message = $event->message;

        app('SlackHelper')
            ->text(sprintf('*There is a error on push nottification!*'))
            ->color('danger')
            ->addField([
                "*Device ID*: {$device->id} `{$device->device_platform}`",
                "*User*: <" . $device->user->getProfileUrl() ."#devices|{$device->user->full_name} _({$device->user->id})_>",
                "*Error*: {$error}",
                "*Message*: {$message}",
            ])
            ->send();
    }
}
