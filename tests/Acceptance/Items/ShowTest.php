<?php

namespace Acceptance\Items;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Item\Model\Item;
use Barterfy\Item\Model\ItemImage;

class ShowTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_return_resource_not_found_even_unauthenticate()
    {
        $this->get('v2/items/1', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate_when_item_exists()
    {
        $user = factory(User::class)->create();
        $user->items()->save(
            factory(Item::class)->make()
        );

        $this->get('v2/items/1', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_id_does_not_exists()
    {
        $user = factory(User::class)->create();

        $this->get('v2/items/1', $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_failed_if_item_does_not_belong_to_you()
    {
        $other = factory(User::class)->create();
        $other->items()->save(
            factory(Item::class)->make()
        );

        $you = factory(User::class)->create();

        $this->get('v2/items/1', $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function should_success_when_item_exists_and_belong_to_you()
    {
        $you = factory(User::class)->create();
        $you->items()->save(
            factory(Item::class)->make()
        );

        $this->get('v2/items/1', $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function it_should_return_all_images()
    {
        $you = factory(User::class)->create();
        $item = $you->items()->save(
            factory(Item::class)->make()
        );

        $item->images()->saveMany(
            factory(ItemImage::class, 3)->make()
        );

        $this->get('v2/items/1', $this->headers($you));

        $this->assertEquals(3, count($this->jsonData()->images));
    }

    /** @test */
    public function it_should_return_item_owner()
    {
        $you = factory(User::class)->create();
        $item = $you->items()->save(
            factory(Item::class)->make()
        );

        $this->get('v2/items/1', $this->headers($you));

        $this->assertEquals($you->id, $this->jsonData()->user->id);
    }

    /** @test */
    public function it_should_return_whats_nesssary()
    {
        $you = factory(User::class)->create();
        $item = $you->items()->save(
            factory(Item::class)->make()
        );

        $item->images()->save(
            factory(ItemImage::class)->make()
        );

        $this->get('v2/items/1', $this->headers($you));

        $this->seeJsonStructure([
            'data' => [
                'id', 'description', 'is_traded', 'is_readied',
                'created_at', 'updated_at', 'is_yours', 'dislikes', 'likes',
                'images' => [
                    '*' => ['id', 'resource_type', 'url', 'created_at']
                ],
                'user'
            ]
        ]);
    }
}
