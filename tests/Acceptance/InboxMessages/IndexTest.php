<?php

namespace Acceptance\InboxMessages;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Inbox\Model\InboxMessage;
use Carbon\Carbon;

class IndexTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_return_resource_not_found_even_unauthenticate()
    {
        $this->get('v2/inboxes/1/messages', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate_when_inbox_exists()
    {
        $user = factory(User::class)->create();
        $user->inboxes()->save(
            factory(Inbox::class)->make()
        );

        $this->get('v2/inboxes/1/messages', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_id_does_not_exists()
    {
        $user = factory(User::class)->create();

        $this->get('v2/inboxes/1/messages', $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_failed_if_inbox_does_not_belong_to_you()
    {
        $other = factory(User::class)->create();
        $other->inboxes()->save(
            factory(Inbox::class)->make()
        );

        $you = factory(User::class)->create();

        $this->get('v2/inboxes/1/messages', $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function it_will_retreive_your_inbox_messages()
    {
        $you = factory(User::class)->create();
        $inbox = $you->inboxes()->save(
            factory(Inbox::class)->make(['match_result' => null])
        );

        $inbox->messages()->saveMany(
            factory(InboxMessage::class, 10)->make([
                'user_id' => $you->id,
                'message' => 'should-see-me'
            ])
        );

        $this->get('v2/inboxes/1/messages', $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->assertEquals(10, count($this->jsonData()));

        $this->seeJson(['message' => 'should-see-me']);
    }

    /** @test */
    public function is_should_return_in_created_at_descending_order()
    {
        $you = factory(User::class)->create();
        $inbox = $you->inboxes()->save(
            factory(Inbox::class)->make(['match_result' => null])
        );

        $first = $inbox->messages()->save(
            factory(InboxMessage::class)->make([
                'user_id' => $you->id,
                'created_at' => Carbon::now()
            ])
        );

        $second = $inbox->messages()->save(
            factory(InboxMessage::class)->make([
                'user_id' => $you->id,
                'created_at' => Carbon::now()->addMinutes(1)
            ])
        );

        $this->get('v2/inboxes/1/messages', $this->headers($you));

        $shouldMatch = collect([$second, $first])->pluck('id')->all();
        $this->assertEquals($shouldMatch, collect($this->jsonData())->pluck('id')->all());
    }

    /** @test */
    public function it_should_return_10_messages_by_default()
    {
        $you = factory(User::class)->create();
        $inbox = $you->inboxes()->save(
            factory(Inbox::class)->make(['match_result' => null])
        );

        $inbox->messages()->saveMany(
            factory(InboxMessage::class, 20)->make([
                'user_id' => $you->id
            ])
        );

        $this->get('v2/inboxes/1/messages', $this->headers($you));

        $this->assertCount(10, $this->jsonData());
    }

    /** @test */
    public function the_limit_can_be_changed()
    {
        $you = factory(User::class)->create();
        $inbox = $you->inboxes()->save(
            factory(Inbox::class)->make(['match_result' => null])
        );

        $inbox->messages()->saveMany(
            factory(InboxMessage::class, 50)->make([
                'user_id' => $you->id
            ])
        );

        $this->get('v2/inboxes/1/messages?limit=20', $this->headers($you));

        $this->assertCount(20, $this->jsonData());
    }

    /** @test */
    public function can_get_messages_before_certain_datetime()
    {
        $you = factory(User::class)->create();
        $inbox = $you->inboxes()->save(
            factory(Inbox::class)->make(['match_result' => null])
        );

        $now = Carbon::now();
        $inc = clone($now);

        for ($i = 0; $i < 5; $i ++) {
            $inbox->messages()->save(
                factory(InboxMessage::class)->make([
                    'user_id' => $you->id,
                    'created_at' => $inc->addMinutes(1),
                    'message' => 'should-see-me'
                ])
            );
        }

        for ($i = 0; $i < 25; $i ++) {
            $inbox->messages()->save(
                factory(InboxMessage::class)->make([
                    'user_id' => $you->id,
                    'created_at' => $inc->addMinutes(1),
                    'message' => 'should-not-see-me'
                ])
            );
        }

        $before = $now->addMinutes(6)->toDateTimeString();
        $this->get('v2/inboxes/1/messages?before=' . $before, $this->headers($you));

        $this->dontSeeJson(['message' => 'should-not-see-me']);
        $this->seeJson(['message' => 'should-see-me']);
        $this->assertEquals(5, count($this->jsonData()));
    }

    /** @test */
    public function the_returned_message_should_have_additional_attributes()
    {
        $you = factory(User::class)->create();
        $inbox = $you->inboxes()->save(
            factory(Inbox::class)->make(['match_result' => null])
        );

        $inbox->messages()->saveMany(
            factory(InboxMessage::class, 50)->make([
                'user_id' => $you->id
            ])
        );

        $this->get('v2/inboxes/1/messages', $this->headers($you));

        $this->seeJsonStructure([
            'data' => [
                '*' => [
                    'type', 'message',
                    'is_system', 'diff_for_humans', 'read_by'
                ]
            ]
        ]);
    }
}
