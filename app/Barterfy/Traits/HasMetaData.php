<?php

namespace Barterfy\Traits;

trait HasMetaData
{
    public function getMetadataAttribute($value)
    {
        if (! $value) {
            return array();
        }

        return $value;
    }

    /**
     * set object's metadata
     *
     * @param string $key
     * @param mixed $value
     */
    public function setMeta($key, $value = null)
    {
        $this->metadata[$key] = $value;

        return $this;
    }

    /**
     * set multiple metadatas
     *
     * @param array $metas
     */
    public function setMetas($metas = array())
    {
        foreach ($metas as $key => $value) {
            $this->setMeta($key, $value);
        }

        return $this;
    }

    /**
     * get object's metadata
     *
     * @param  string $key
     * @param  mixed $default
     */
    public function getMeta($key, $default = null)
    {
        return isset($this->metadata[$key])? $this->metadata[$key] : $default;
    }

    /**
     * shortcut to set/get metadata
     *
     * @param  string $key
     * @param  mixed $value
     */
    public function meta($key, $value = 'ಥ_ಥ')
    {
        return $value === 'ಥ_ಥ'?
            $this->getMeta($key) : $this->setMeta($key, $value);
    }
}
