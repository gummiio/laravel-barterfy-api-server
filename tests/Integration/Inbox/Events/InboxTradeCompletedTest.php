<?php

namespace Integration\Inbox\Events;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Events\InboxTradeCompleted;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Item\Model\Item;
use Integration\IntegrationTestCase;

class InboxTradeCompletedTest extends IntegrationTestCase
{
    /** @test */
    public function it_will_update_inbox_items_trade_status()
    {
        $user = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();

        $item = factory(Item::class)->make([
            'is_traded' => false, 'is_readied' => true
        ]);

        $user->items()->save($item);
        $inbox->items()->save($item);

        event(new InboxTradeCompleted($inbox));

        $this->assertTrue($item->fresh()->is_traded);
    }

    /** @test */
    public function it_will_create_system_message()
    {
        $user = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();

        $item = factory(Item::class)->make([
            'is_traded' => false, 'is_readied' => true
        ]);

        $user->items()->save($item);
        $inbox->items()->save($item);

        event(new InboxTradeCompleted($inbox));

        $this->assertCount(1, $inbox->load('messages')->messages);
    }
}
