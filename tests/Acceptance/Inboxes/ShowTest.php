<?php

namespace Acceptance\Inboxes;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Item\Model\Item;

class ShowTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_return_resource_not_found_even_unauthenticate()
    {
        $this->get('v2/inboxes/1', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate_when_inbox_exists()
    {
        $user = factory(User::class)->create();
        $user->inboxes()->save(
            factory(Inbox::class)->make()
        );

        $this->get('v2/inboxes/1', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_id_does_not_exists()
    {
        $user = factory(User::class)->create();

        $this->get('v2/inboxes/1', $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_failed_if_inbox_does_not_belong_to_you()
    {
        $other = factory(User::class)->create();
        $other->inboxes()->save(
            factory(Inbox::class)->make()
        );

        $you = factory(User::class)->create();

        $this->get('v2/inboxes/1', $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function should_success_when_item_exists_and_belong_to_you()
    {
        $you = factory(User::class)->create();
        $you->inboxes()->save(
            factory(Inbox::class)->make()
        );

        $this->get('v2/inboxes/1', $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function it_should_return_items_and_users()
    {
        $you = factory(User::class)->create();
        $item = $you->items()->save(
            factory(Item::class)->make(['description' => 'should-see-me'])
        );

        $other = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();
        $inbox->users()->saveMany([$you, $other]);
        $inbox->items()->saveMany([$item]);

        $this->get('v2/inboxes/1', $this->headers($you));

        $this->assertEquals(2, count($this->jsonData()->users));
        $this->assertEquals(1, count($this->jsonData()->items));

        $this->seeJson(['description' => 'should-see-me']);
    }

    /** @test */
    public function it_should_return_whats_nesssary()
    {
        $you = factory(User::class)->create();
        $yourItem = $you->items()->save(
            factory(Item::class)->make()
        );

        $other = factory(User::class)->create();
        $otherItem = $other->items()->save(
            factory(Item::class)->make()
        );

        $inbox = factory(Inbox::class)->create();
        $inbox->users()->saveMany([$you, $other]);

        $this->get('v2/inboxes/1', $this->headers($you));

        $this->seeJsonStructure([
            'data' => [
                'id', 'name',
                'is_support', 'match_result', 'active',
                'total_messages',
                'last_message', 'items', 'users'
            ]
        ]);
    }
}
