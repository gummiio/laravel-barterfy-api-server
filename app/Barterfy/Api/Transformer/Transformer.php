<?php

namespace Barterfy\Api\Transformer;

abstract class Transformer
{
    /**
     * Input that needs to be transformed
     *
     * @var array
     */
    protected $input = [];

    /**
     * Constructor
     *
     * @param array $input
     */
    public function __construct($input)
    {
        $this->input = $input;
    }

    /**
     * The transform method
     *
     * @param  array $columns
     */
    abstract public function transform($columns);

    /**
     * Get the input value
     *
     * @return array
     */
    public function getResults()
    {
        return $this->input;
    }
}
