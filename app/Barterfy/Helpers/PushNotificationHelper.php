<?php

namespace Barterfy\Helpers;

use Barterfy\Device\Events\DevicePushNotificationFailed;
use Barterfy\Device\Model\Device;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotification;
use LaravelFCM\Message\PayloadNotificationBuilder;
use PushNotification;

class PushNotificationHelper
{
    protected $device;

    protected $data;

    protected $badge;

    public function __construct()
    {
        $this->devices = collect();
    }

    public function to(Device $device)
    {
        $this->device = $device;

        return $this;
    }

    public function with($data)
    {
        $this->data = $data;

        return $this;
    }

    public function badge($count)
    {
        $this->badge = intval($count);

        return $this;
    }

    public function send($message)
    {
        try {
            // if ($this->device->device_platform == 'ios') {
            //     return $this->sendToIOS($message);
            // }

            return $this->sendToFCM($message);
        } catch (\Exception $e) {
            event(new DevicePushNotificationFailed($this->device, $e->getMessage(), $message));
        }
    }

    // public function sendToIOS($message)
    // {
    //     $pushMessage = PushNotification::Message($message, [
    //         'badge' => $this->badge,
    //         'custom' => $this->data
    //     ]);

    //     return PushNotification::app($this->device->device_platform)
    //             ->to($this->device->notification_token)
    //             ->send($pushMessage);

    //     return $this->sendToFCM($message);
    // }

    public function sendToFCM($message)
    {
        $notificationBuilder = new PayloadNotificationBuilder();
        $notificationBuilder->setBody($message)
                    ->setIcon('ic_stat_b')
                    ->setBadge((string) $this->badge);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData(array_merge($this->data, [
            'category' => 'CATEGORY_MESSAGE',
            'priority' => 'HIGH'
        ]));

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setPriority('high');

        $options = $optionBuilder->build();
        $data = $dataBuilder->build();
        $notification = new class($notificationBuilder) extends PayloadNotification {
            public function toArray()
            {
                $array = parent::toArray();
                $array['category'] = 'CATEGORY_MESSAGE';
                $array['priority'] = 'HIGH';
                return $array;
            }
        };

        $downstreamResponse = FCM::sendTo($this->device->notification_token, $options, $notification, $data);


        if ($downstreamResponse->numberFailure()) {
            $error = array_values($downstreamResponse->tokensWithError());

            throw new \Exception("FCM notification failed. - " . json_encode($error[0]));
        }

        return $downstreamResponse;
    }
}
