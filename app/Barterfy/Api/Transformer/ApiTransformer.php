<?php

namespace Barterfy\Api\Transformer;

use Barterfy\Api\Transformer\HiddenColumnTransformer;
use Barterfy\Api\Transformer\RenameColumnTransformer;

class ApiTransformer
{
    /**
     * The transformed input
     *
     * @var array
     */
    protected $transformed;

    /**
     * The applied transformers
     *
     * @var array
     */
    protected $transformers = [];

    /**
     * Columns that should be filtered out
     *
     * @var array
     */
    protected $hiddenColumns = [];

    /**
     * Columns that should be renamed to clearer name
     *
     * @var array
     */
    protected $renameColumns = [];

    /**
     * Constructor
     *
     * @param array $value input that needs to be transformed
     */
    public function __construct($value)
    {
        $this->transformed = $value;

        $this->addTransformer(HiddenColumnTransformer::class, $this->hiddenColumns);
        $this->addTransformer(RenameColumnTransformer::class, $this->renameColumns);
    }

    /**
     * Run thought the applied transformers
     *
     * @return array input after transformed
     */
    public function transform()
    {
        foreach ($this->transformers as $transformer => $arguments) {
            $this->transformed = (new $transformer($this->transformed))
                ->transform($arguments)
                ->getResults();
        }

        ksort($this->transformed);

        return $this->transformed;
    }

    /**
     * Add transformer into the lists
     *
     * @param string $transformer transformer class name
     * @param array $arguments   arguments that get passed to the transformer
     */
    public function addTransformer($transformer, $arguments)
    {
        $this->transformers[$transformer] = $arguments;

        return $this;
    }

    /**
     * Remove transformer from the lists
     *
     * @param  string $transformer transformer class name
     */
    public function removeTransformer($transformer)
    {
        if (isset($this->transformers[$transformer])) {
            unset($this->transformers[$transformer]);
        }

        return $this;
    }
}
