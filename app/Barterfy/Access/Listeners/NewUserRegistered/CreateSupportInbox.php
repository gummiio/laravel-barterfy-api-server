<?php

namespace Barterfy\Access\Listeners\NewUserRegistered;

use Barterfy\Access\Events\NewUserRegistered;
use Barterfy\Inbox\Events\SupportInboxCreated;
use Barterfy\Inbox\Repository\InboxRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateSupportInbox implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserRegistered  $event
     * @return void
     */
    public function handle(NewUserRegistered $event)
    {
        $user = $event->user;

        $inbox = InboxRepository::CreateSupportInboxForUser($user);

        event(new SupportInboxCreated($inbox, $user));
    }
}
