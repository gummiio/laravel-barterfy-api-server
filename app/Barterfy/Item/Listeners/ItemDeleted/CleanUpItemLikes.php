<?php

namespace Barterfy\Item\Listeners\ItemDeleted;

use Barterfy\Item\Events\ItemDeleted;
use Barterfy\Item\Model\Item;
use Illuminate\Contracts\Queue\ShouldQueue;

class CleanUpItemLikes implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemDeleted  $event
     * @return void
     */
    public function handle(ItemDeleted $event)
    {
        $item = Item::withoutGlobalScopes()->find($event->itemId);
        $item->itemLikes()->delete();
    }
}
