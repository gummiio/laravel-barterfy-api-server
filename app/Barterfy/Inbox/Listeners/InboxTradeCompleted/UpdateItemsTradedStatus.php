<?php

namespace Barterfy\Inbox\Listeners\InboxTradeCompleted;

use Barterfy\Inbox\Events\InboxTradeCompleted;
use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Model\Inbox;
use Illuminate\Contracts\Queue\ShouldQueue;

class UpdateItemsTradedStatus implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InboxTradeCompleted  $event
     * @return void
     */
    public function handle(InboxTradeCompleted $event)
    {
        $inbox = $event->inbox;

        $inbox->items->each(function ($item) {
            $item->traded();
        });
    }
}
