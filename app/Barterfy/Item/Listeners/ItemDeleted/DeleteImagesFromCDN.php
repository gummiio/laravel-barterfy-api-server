<?php

namespace Barterfy\Item\Listeners\ItemDeleted;

use Barterfy\Inbox\Model\Inbox;
use Barterfy\Item\Events\ItemDeleted;
use Barterfy\Item\Model\Item;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteImagesFromCDN implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemDeleted  $event
     * @return void
     */
    public function handle(ItemDeleted $event)
    {
        $item = Item::withoutGlobalScopes()->find($event->itemId);

        $item->images->each(function($image) {
            $image->deleteFromCDN();
            $image->delete();
        });
    }
}
