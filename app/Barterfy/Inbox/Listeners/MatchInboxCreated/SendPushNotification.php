<?php

namespace Barterfy\Inbox\Listeners\MatchInboxCreated;

use Barterfy\Device\Repository\DeviceRepository;
use Barterfy\Inbox\Events\MatchInboxCreated;
use Barterfy\Inbox\Repository\InboxMessageStatusRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendPushNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MatchInboxCreated  $event
     * @return void
     */
    public function handle(MatchInboxCreated $event)
    {
        $inbox = $event->inbox;

        $devices = DeviceRepository::GetInboxDevices($inbox, 'notify_on_matches');

        $devices->each(function ($device) use ($inbox) {
            app('PushNotificationHelper')
                ->to($device)
                // ->badge(InboxMessageStatusRepository::GetUserUnreadMessages($device->user)->count())
                ->with($this->prepareNotificationData($inbox))
                ->send('Congratulations! You’ve got a match!');
        });
    }

    protected function prepareNotificationData($inbox)
    {
        return [
            'type' => 'matches',
            'inbox_id' => $inbox->id
        ];
    }
}
