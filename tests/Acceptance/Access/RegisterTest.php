<?php

namespace Acceptance\Access;

use Barterfy\Access\Model\User;
use Acceptance\AcceptanceTestCase;

class RegisterTest extends AcceptanceTestCase
{
    /** @test */
    public function user_cannnot_register_without_facebook_token()
    {
        $this->post('v2/users/register', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->seeJson([
            'type' => 'ValidationFailed'
        ]);

        $this->assertResponseStatus(422);
    }

    /** @test */
    public function user_cannot_register_with_invalid_facebook_token()
    {
        $data = [
            'facebook_token' => 'abcde'
        ];

        $this->post('v2/users/register', $data, $this->headers());

        $this->seeJson(['success' => false]);

        $this->seeJson([
            'type' => 'OAuthException'
        ]);

        $this->assertResponseStatus(400);
    }

    /** @test */
    public function user_can_register_with_valid_facebook_token()
    {
        $this->prepareGraphUser();

        $data = [
            'facebook_token' => $this->graphUser['access_token']
        ];

        $this->post('v2/users/register', $data, $this->headers());

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(201);

        $this->seeInDatabase('users', ['facebook_token' => $this->graphUser['access_token']]);

        $this->removeGraphUser();
    }

    /** @test */
    public function user_wont_register_if_facebook_token_exists()
    {
        $this->prepareGraphUser();

        factory(User::class)->create([
            'facebook_token' => $this->graphUser['access_token']
        ]);

        $data = [
            'facebook_token' => $this->graphUser['access_token']
        ];

        $this->post('v2/users/register', $data, $this->headers());

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->assertCount(1, User::all());

        $this->removeGraphUser();
    }

    /** @test */
    public function user_will_be_updated_if_facebook_token_refreshed()
    {
        $this->prepareGraphUser();

        factory(User::class)->create([
            'username' => 'fb_' . $this->graphUser['id'],
            'facebook_token' => 'abcde'
        ]);

        $data = [
            'facebook_token' => $this->graphUser['access_token']
        ];

        $this->post('v2/users/register', $data, $this->headers());

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->seeInDatabase('users', ['facebook_token' => $this->graphUser['access_token']]);

        $this->notSeeInDatabase('users', ['facebook_token' => 'abcde']);

        $this->assertCount(1, User::all());

        $this->removeGraphUser();
    }

    /** @test */
    public function new_user_will_be_created_if_existing_facebook_token_user_is_deleted()
    {
        $this->prepareGraphUser();

        $user = factory(User::class)->create([
            'username' => 'fb_' . $this->graphUser['id'],
            'facebook_token' => $this->graphUser['access_token']
        ]);

        $user->delete();

        $data = [
            'facebook_token' => $this->graphUser['access_token']
        ];

        $this->post('v2/users/register', $data, $this->headers());

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(201);

        $this->seeInDatabase('users', [
            'facebook_token' => $this->graphUser['access_token'],
            'deleted_at' => null
        ]);

        $this->assertCount(
            2,
            User::withTrashed()->where('facebook_token', $this->graphUser['access_token'])->get()
        );

        $this->removeGraphUser();
    }
}
