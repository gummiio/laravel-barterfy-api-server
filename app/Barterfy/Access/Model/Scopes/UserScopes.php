<?php

namespace Barterfy\Access\Model\Scopes;

trait UserScopes
{
    public function scopeHasLocations($query)
    {
        return $query->whereNotNull('latitude')->whereNotNull('longitude');
    }
}
