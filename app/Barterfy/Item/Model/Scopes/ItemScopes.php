<?php

namespace Barterfy\Item\Model\Scopes;

trait ItemScopes
{
    public function scopeReadied($query)
    {
        return $query->where('is_readied', true);
    }

    public function scopeNotReadied($query)
    {
        return $query->where('is_readied', false);
    }

    public function scopeTraded($query)
    {
        return $query->where('is_traded', true);
    }

    public function scopeNotTraded($query)
    {
        return $query->where('is_traded', false);
    }
}
