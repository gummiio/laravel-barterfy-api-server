<?php

namespace Integration\Inbox\Events;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Events\SupportInboxCreated;
use Barterfy\Inbox\Model\Inbox;
use Integration\IntegrationTestCase;

class SupportInboxCreatedTest extends IntegrationTestCase
{
    /** @test */
    public function it_should_create_the_system_welcome_message()
    {
        $user = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create(['is_support' => true]);

        $inbox->users()->save($user);

        event(new SupportInboxCreated($inbox, $user));

        $this->seeInDatabase('inbox_messages', [
            'inbox_id' => $inbox->id,
            'type' => 'conversation'
        ]);

        $this->assertCount(1, $inbox->load('messages')->messages);
    }
}
