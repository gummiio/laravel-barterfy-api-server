<?php

namespace Barterfy\Item\Events;

use App\Events\Event;
use Barterfy\Item\Model\ItemLike;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class ItemLiked extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * ItemLike Object
     *
     * @var object
     */
    public $itemLike;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ItemLike $itemLike)
    {
        $this->itemLike = $itemLike;
    }
}
