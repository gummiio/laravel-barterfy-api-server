<?php

namespace Barterfy\Access\Listeners\UserDeleted;

use Barterfy\Access\Events\UserDeleted;
use Barterfy\Access\Model\User;
use Barterfy\Device\Events\DeviceDeleted;
use Barterfy\Item\Events\ItemDeleted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UnassociateAllRecords implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserDeleted  $event
     * @return void
     */
    public function handle(UserDeleted $event)
    {
        $user = User::withTrashed()->with('devices', 'items')->find($event->userID);

        // devices
        $user->devices->each(function($device) {
            $device->delete();

            event(new DeviceDeleted($device));
        });

        // item
        $user->items->each(function($item) {
            $item->delete();

            event(new ItemDeleted($item));
        });

        // support
        $user->inboxes()->support()->get()->each(function($inbox) {
            $inbox->delete();
        });

    }
}
