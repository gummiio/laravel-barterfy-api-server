<?php

namespace Barterfy\Inbox\Listeners\NewSupportMessageCreated;

use Barterfy\Inbox\Events\NewSupportMessageCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Config;

class NotifyOnSlack implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewSupportMessageCreated  $event
     * @return void
     */
    public function handle(NewSupportMessageCreated $event)
    {
        $message = $event->message;

        if ($message->is_system) return;

        app('SlackHelper')
            ->text('*New support message was posted.*')
            ->color('good')
            ->addField([
                "*Inbox ID*: <" . $message->inbox->inboxLink() . "|{$message->inbox_id}>",
                "*User:* _{$message->user->full_name}_",
                "*Message:* {$message->message}",
                "*At:* ".$message->created_at->toDateTimeString()
            ])
            ->sendTo(env('SLACK_ACTIVITY_CHANNEL', null), '@here');
    }
}
