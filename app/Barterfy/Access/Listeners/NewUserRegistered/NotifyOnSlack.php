<?php

namespace Barterfy\Access\Listeners\NewUserRegistered;

use Barterfy\Access\Events\NewUserRegistered;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Config;

class NotifyOnSlack implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewUserRegistered  $event
     * @return void
     */
    public function handle(NewUserRegistered $event)
    {
        if (! $event->shouldNotify) return;

        $user = $event->user;

        app('SlackHelper')
            ->text('A new user has just registered.')
            ->color('good')
            ->addField([
                "*User ID*: <" . $user->getProfileUrl() . "|{$user->id}>",
                "*Name:* {$user->full_name}",
                "*Gender:* {$user->gender}",
                "*Age:* {$user->age}",
                "*Stalk This Person:* " . sprintf('<%s|Facebook>', $user->fb_profile_link),
                "*At:* ".$user->created_at->toDateTimeString()
            ])
            ->thumbnail($user->avatar)
            ->send();
    }
}
