<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Barterfy\Inbox\Repository\InboxRepository;
use Barterfy\Item\Events\ItemCreated;
use Barterfy\Item\Events\ItemDeleted;
use Barterfy\Item\Events\ItemDisliked;
use Barterfy\Item\Events\ItemLiked;
use Barterfy\Item\Events\ItemReported;
use Barterfy\Item\Events\ItemUpdated;
use Barterfy\Item\Model\Item;
use Barterfy\Item\Repository\ItemImageRepository;
use Barterfy\Item\Repository\ItemRepository;
use Barterfy\Item\Requests\ItemCreateRequest;
use Barterfy\Item\Requests\ItemDeleteRequest;
use Barterfy\Item\Requests\ItemFetchRequest;
use Barterfy\Item\Requests\ItemListRequest;
use Barterfy\Item\Requests\ItemShowRequest;
use Barterfy\Item\Requests\ItemUpdateRequest;
use Illuminate\Http\Request;

class ItemController extends ApiController
{
    public function index(ItemListRequest $request)
    {
        $items = ItemRepository::GetItemsByUser(Auth()->user(), $request);
        $stats = InboxRepository::GetInboxStatsByUser(Auth()->user());

        return response()->apiSuccess($items->count() . ' item(s) found.', $items, 200, ['stats' => $stats]);
    }

    public function store(ItemCreateRequest $request)
    {
        $item = ItemRepository::CreateItemFromInput($request);
        $images = ItemImageRepository::CreateImagesFromInput($request);

        Auth()->user()->items()->save($item);
        $item->images()->saveMany($images);

        event(new ItemCreated($item));

        return response()->apiSuccess('Item created.', $item->fresh(), 201);
    }

    public function show(ItemShowRequest $request, Item $item)
    {
        return response()->apiSuccess('', $item);
    }

    public function update(ItemUpdateRequest $request, Item $item)
    {
        $item->fill($request->all());
        $item->save();

        event(new ItemUpdated($item));

        return response()->apiSuccess('Item updated.', $item, 200);
    }

    public function destroy(ItemDeleteRequest $request, Item $item)
    {
        $item->delete();

        event(new ItemDeleted($item));

        return response()->apiSuccess('Item deleted.', [], 200);
    }

    public function like(Item $item)
    {
        if ($item->is_yours) {
            return response()->apiError('You cannot like your own item', [], 400);
        }

        if ($item->isDecidedBy(Auth()->user())) {
            return response()->apiError('You\'ve already decided on this item', [], 400);
        }

        $itemLike = $item->likedBy(Auth()->user());

        event(new ItemLiked($itemLike));

        return response()->apiSuccess('You liked the item.', [], 201);
    }

    public function dislike(Item $item)
    {
        if ($item->is_yours) {
            return response()->apiError('You cannot like your own item', [], 400);
        }

        if ($item->isDecidedBy(Auth()->user())) {
            return response()->apiError('You\'ve already decided on this item', [], 400);
        }

        $itemLike = $item->dislikedBy(Auth()->user());

        event(new ItemDisliked($itemLike));

        return response()->apiSuccess('You disliked the item.', [], 201);
    }

    public function report(Item $item, Request $request)
    {
        if (! $item->isDecidedBy(Auth()->user())) {
            $itemLike = $item->dislikedBy(Auth()->user());
            event(new ItemDisliked($itemLike));
        }

        event(new ItemReported($item, $request->input('message')));

        return response()->apiSuccess('Item has be reported.', [], 201);
    }

    public function search(ItemFetchRequest $request)
    {
        $user = Auth()->user();

        if (! $user->longitude || ! $user->latitude) {
            return response()->apiError('Current user does not have a coordinate', [], 400);
        }

        $items = ItemRepository::SearchItemsForUser($user, $request);

        return response()->apiSuccess($items->count() . ' item(s) found.', $items);
    }

}
