<?php

namespace Barterfy\Helpers;

use Cloudinary\Uploader;
use Cloudinary;

class CloudinaryHelper
{
    static $cloud = 0;
    protected $imageSrc;
    protected $args = [];

    public function __construct($imageSrc = '', $cloud = null)
    {
        self::setCloud($cloud);
        $this->source($imageSrc);
    }

    public static function randomCloud()
    {
        return rand(1, env("CLOUDINARY_CLOUDS", 0));
    }

    public static function getCloud()
    {
        return self::$cloud;
    }

    public static function setCloud($cloud)
    {
        if (is_null($cloud)) {
            $cloud = self::randomCloud();
        }

        if (self::$cloud != $cloud) {
            Cloudinary::config([
                "cloud_name" => env("CLOUDINARY_CLOUD_NAME_{$cloud}", ''),
                "api_key"    => env("CLOUDINARY_API_KEY_{$cloud}", ''),
                "api_secret" => env("CLOUDINARY_API_SECRET_{$cloud}", ''),
            ]);

            self::$cloud = $cloud;
        }
    }

    public function source($imageSrc)
    {
        $this->imageSrc = $imageSrc;

        return $this;
    }

    public function width($width)
    {
        $this->args['width'] = $width;

        return $this;
    }

    public function height($height)
    {
        $this->args['height'] = $height;

        return $this;
    }

    public function format($format)
    {
        $this->args['format'] = $format;

        return $this;
    }

    public function quality($quality)
    {
        $this->args['quality'] = $quality;

        return $this;
    }

    public function tags($tags)
    {
        if (is_string($tags)) {
            $tags = preg_split('/\s*,\s*/', trim($tags));
        }

        $this->args['tags'] = $tags;

        return $this;
    }

    public function upload($publicID = '', $args = array())
    {
        if ($publicID) {
            $this->args['public_id'] = $publicID;
        }

        if (! file_exists($this->imageSrc)) {
            return false;
        }

        return Uploader::upload($this->imageSrc, array_merge($this->args, $args));
    }

    public static function delete($publicID, $cloud)
    {
        self::setCloud($cloud);
        return Uploader::destroy($publicID, ['invalidate' => true]);
    }

    public static function init($imageSrc = '', $cloud = null)
    {
        return new static($imageSrc, $cloud);
    }

    public static function exists($publicID)
    {
        try {
            $api = new \Cloudinary\Api();
            $api->resource($publicID);
            return true;
        } catch(\Exception $e) {
            return false;
        }
    }
}
