<?php

namespace Barterfy\Item\Model;

use Barterfy\Item\Model\Relationships\ItemLikeRelationships;
use Barterfy\Model;

class ItemLike extends Model
{
    use ItemLikeRelationships;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'item_likes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['liked'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $casts = [
        'liked' => 'boolean'
    ];
}
