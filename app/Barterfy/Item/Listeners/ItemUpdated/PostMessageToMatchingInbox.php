<?php

namespace Barterfy\Item\Listeners\ItemUpdated;

use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Repository\InboxRepository;
use Barterfy\Item\Events\ItemUpdated;
use Barterfy\Item\Model\Item;
use Illuminate\Contracts\Queue\ShouldQueue;

class PostMessageToMatchingInbox implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemUpdated  $event
     * @return void
     */
    public function handle(ItemUpdated $event)
    {
        $item = $event->item;

        $inboxes = InboxRepository::GetMatchingInboxesByItem($item);

        $inboxes->each(function ($inbox) use ($item) {
            $message = $inbox->systemMessage(
                sprintf('%s has changed the item description.', $item->user->display_name)
            );

            event(new NewMessageCreated($message));
        });
    }
}
