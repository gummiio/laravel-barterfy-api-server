<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Events\NewSupportMessageCreated;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Inbox\Repository\InboxMessageRepository;
use Barterfy\Inbox\Requests\InboxMessageCreateRequest;
use Barterfy\Inbox\Requests\InboxMessageListRequest;
use Illuminate\Http\Request;

class InboxMessageController extends ApiController
{
    public function index(InboxMessageListRequest $request, Inbox $inbox)
    {
        $messages = InboxMessageRepository::GetInboxMessages($inbox, $request);

        return response()->apiSuccess($messages->count() . ' message(s) found.', $messages);
    }

    public function store(InboxMessageCreateRequest $request, Inbox $inbox)
    {
        if (! $inbox->active) {
            return response()->apiError('Inbox is inactive', [], 400);
        }

        $message = InboxMessageRepository::CreateMessageFromInput($request);
        $message->user_id = Auth()->id();

        $inbox->messages()->save($message);

        if ($inbox->is_support) {
            event(new NewSupportMessageCreated($message));
        } else {
            event(new NewMessageCreated($message));
        }

        return response()->apiSuccess('Message Posted.', $message->fresh(), 201);
    }
}
