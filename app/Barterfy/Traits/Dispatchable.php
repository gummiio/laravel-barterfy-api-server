<?php

namespace Barterfy\Traits;

trait Dispatchable
{
    public function dispatchServiceEvents($dispatcher)
    {
        foreach ($this->serviceEvents() as $event => $listeners) {
            foreach ($listeners as $listener) {
                $dispatcher->listen($event, $listener);
            }
        }
    }

    public function serviceEvents()
    {
        return $this->serviceEvents;
    }
}
