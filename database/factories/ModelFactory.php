<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(Barterfy\Access\Model\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->username,
        'password' => bcrypt('password'),
        'email' => $faker->safeEmail,
        'api_token' => str_random(32),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
    ];
});

$factory->define(Barterfy\Access\Model\AdminUser::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->username,
        'password' => bcrypt('password'),
        'email' => $faker->safeEmail,
        'api_token' => str_random(32),
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
    ];
});

$factory->define(Barterfy\Api\Model\ApiKey::class, function (Faker\Generator $faker) {
    return [
        'key' => $faker->unique()->md5,
        'description' => $faker->sentence
    ];
});

$factory->define(Barterfy\Api\Model\ApiLog::class, function (Faker\Generator $faker) {
    return [
        'endpoint' => $faker->slug,
        'method' => 'get',
        'params' => '[]',
        'ip' => $faker->localIpv4,
        'user_agent' => $faker->userAgent,
        'respond_code' => '200',
        'respond_time' => $faker->randomFloat(6, 0, 2)
    ];
});

$factory->define(Barterfy\Device\Model\Device::class, function (Faker\Generator $faker) {
    return [
        'notification_token' => $faker->uuid,
        'device_platform' => $faker->boolean? 'ios' : 'android'
    ];
});

$factory->define(Barterfy\Item\Model\Item::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->sentence
    ];
});

$factory->define(Barterfy\Inbox\Model\Inbox::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->sentence,
        'match_result' => null
    ];
});

$factory->define(Barterfy\Inbox\Model\InboxMessage::class, function (Faker\Generator $faker) {
    return [
        'message' => $faker->paragraph
    ];
});

$factory->define(Barterfy\Item\Model\Item::class, function (Faker\Generator $faker) {
    return [
        'description' => $faker->paragraph
    ];
});

$factory->define(Barterfy\Item\Model\ItemLike::class, function (Faker\Generator $faker) {
    return [
        'liked' => $faker->boolean
    ];
});

$factory->define(Barterfy\Item\Model\ItemImage::class, function (Faker\Generator $faker) {
    return [
        'cloud_index' => 1,
        'public_id' => $faker->randomNumber,
        'resource_type' => 'image',
        'url' => $faker->imageUrl(980, 980)
    ];
});
