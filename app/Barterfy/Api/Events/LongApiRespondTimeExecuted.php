<?php

namespace Barterfy\Api\Events;

use App\Events\Event;
use Barterfy\Api\Model\ApiLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class LongApiRespondTimeExecuted extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * Api Log Object
     *
     * @var object
     */
    public $apiLog;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ApiLog $apiLog)
    {
        $this->apiLog = $apiLog;
    }
}
