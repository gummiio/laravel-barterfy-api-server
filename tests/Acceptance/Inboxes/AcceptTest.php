<?php

namespace Acceptance\Inboxes;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Inbox\Events\InboxHasBeenAccepted;
use Barterfy\Inbox\Model\Inbox;

class AcceptTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_return_resource_not_found_even_unauthenticate()
    {
        $this->post('v2/inboxes/1/accept', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate_when_item_exists()
    {
        $user = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();
        $inbox->users()->save($user);

        $this->post('v2/inboxes/1/accept', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_id_does_not_exists()
    {
        $user = factory(User::class)->create();

        $this->post('v2/inboxes/1/accept', [], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_failed_if_inbox_does_not_belongs_to_you()
    {
        $you = factory(User::class)->create();

        $other = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();
        $inbox->users()->save($other);

        $this->post('v2/inboxes/1/accept', [], $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function should_failed_if_you_already_decided_on_inbox()
    {
        $you = factory(User::class)->create();
        $other = factory(User::class)->create();

        $inbox = factory(Inbox::class)->create();
        $inbox->users()->saveMany([$you, $other]);

        $inbox->acceptedBy($you);

        $this->post('v2/inboxes/1/accept', [], $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(400);
    }

    /** @test */
    public function should_successfully_when_inbox_exists_and_own_by_you_and_has_not_yet_decided()
    {
        $you = factory(User::class)->create();

        $inbox = factory(Inbox::class)->create();
        $inbox->users()->save($you);

        $this->expectsEvents(InboxHasBeenAccepted::class);

        $this->post('v2/inboxes/1/accept', [], $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(201);

        $this->seeInDatabase('inbox_acceptances', ['user_id' => $you->id, 'inbox_id' => $inbox->id]);
    }
}
