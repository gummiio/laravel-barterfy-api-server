<?php

namespace Barterfy\Api\Exceptions;

use Barterfy\Api\Exceptions\ApiResponseException;

class ApiTokenInvalidException extends ApiResponseException
{
    public function __construct()
    {
        parent::__construct('Whoops, looks like something went wrong.', [
            'type'    => 'ApiTokenInvalid',
            'message' => 'Provided api token is invalid for the current request.'
        ], 403);
    }
}
