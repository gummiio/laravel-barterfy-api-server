<?php

use Barterfy\Access\Model\AdminUser;
use Barterfy\Access\Model\User;
use Barterfy\Api\Model\ApiKey;
use Illuminate\Database\Seeder;

class GenerateApiAccessData extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'username' => 'gummi',
            'password' => bcrypt('password'),
            'api_token' => 'asdfasdf',
        ]);

        factory(AdminUser::class)->create([
            'username' => 'admin',
            'password' => bcrypt('password'),
            'api_token' => 'zxcvzxcv'
        ]);

        factory(ApiKey::class)->create([
            'key' => 'asdf'
        ]);

        factory(ApiKey::class)->create([
            'key' => 'zxcv',
            'is_admin' => true
        ]);
    }
}
