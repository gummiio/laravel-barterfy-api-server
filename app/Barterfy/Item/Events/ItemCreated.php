<?php

namespace Barterfy\Item\Events;

use App\Events\Event;
use Barterfy\Item\Model\Item;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class ItemCreated extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * Item ID
     *
     * @var object
     */
    public $itemId;
    public $shouldNotify;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Item $item, $shouldNotify = true)
    {
        $this->itemId = $item->getKey();
        $this->shouldNotify = $shouldNotify;
    }
}
