<?php

namespace Barterfy\Device\Events;

use App\Events\Event;
use Barterfy\Device\Model\Device;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class DeviceDeleted  extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * Device Object
     *
     * @var object
     */
    public $deviceId;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Device $device)
    {
        $this->deviceId = $device->id;
    }
}
