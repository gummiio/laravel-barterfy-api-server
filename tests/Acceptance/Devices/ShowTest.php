<?php

namespace Acceptance\Devices;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Device\Model\Device;

class ShowTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_return_resource_not_found_even_unauthenticate()
    {
        $this->get('v2/devices/1', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate_when_device_exists()
    {
        $user = factory(User::class)->create();
        $user->devices()->save(
            factory(Device::class)->make()
        );

        $this->get('v2/devices/1', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_id_does_not_exists()
    {
        $user = factory(User::class)->create();

        $this->get('v2/devices/1', $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_failed_if_device_does_not_belong_to_you()
    {
        $other = factory(User::class)->create();
        $other->devices()->save(
            factory(Device::class)->make()
        );

        $you = factory(User::class)->create();

        $this->get('v2/devices/1', $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function should_success_when_device_exists_and_belong_to_you()
    {
        $you = factory(User::class)->create();
        $you->devices()->save(
            factory(Device::class)->make()
        );

        $this->get('v2/devices/1', $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function it_should_return_whats_nesssary()
    {
        $you = factory(User::class)->create();
        $you->devices()->save(
            factory(Device::class)->make()
        );

        $this->get('v2/devices/1', $this->headers($you));

        $this->seeJsonStructure([
            'data' => [
                "id", "notification_token", "device_platform",
                "notify_on_matches", "notify_on_new_message",
                "created_at"
            ]
        ]);
    }
}
