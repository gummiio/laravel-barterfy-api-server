<?php

namespace Barterfy\Api\Listeners\LongApiRespondTimeExecuted;

use Barterfy\Api\Events\LongApiRespondTimeExecuted;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Config;

class NotifyOnSlack implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  LongApiRespondTimeExecuted  $event
     * @return void
     */
    public function handle(LongApiRespondTimeExecuted $event)
    {
        $apiLog = $event->apiLog;

        app('SlackHelper')
            ->text(sprintf('*An Api request is talking for more than %d secdons.*', Config::get('api.long_request_threshold')))
            ->color('warning')
            ->addField([
                "*Log ID*: {$apiLog->id}",
                "*Request:* `{$apiLog->method}` _{$apiLog->endpoint}_",
                "*Response:* `{$apiLog->respond_code}` _{$apiLog->respond_time}s_",
                "*At:* ".$apiLog->created_at->toDateTimeString()
            ])
            ->send();
    }
}
