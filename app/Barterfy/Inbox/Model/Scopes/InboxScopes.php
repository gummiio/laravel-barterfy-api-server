<?php

namespace Barterfy\Inbox\Model\Scopes;

trait InboxScopes
{
    public function scopeSupport($query)
    {
        return $query->where('is_support', true);
    }

    public function scopeNotSupport($query)
    {
        return $query->where('is_support', false);
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function scopeInactive($query)
    {
        return $query->where('active', false);
    }

    public function scopeSuccessed($query)
    {
        return $query->where('match_result', true);
    }

    public function scopeFailed($query)
    {
        return $query->where('match_result', false);
    }

    public function scopeMatching($query)
    {
        return $query->whereNull('match_result');
    }
}
