<?php

namespace Integration\Inbox\Events;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Events\InboxHasBeenAccepted;
use Barterfy\Inbox\Model\Inbox;
use Integration\IntegrationTestCase;

class InboxHasBeenAcceptedTest extends IntegrationTestCase
{
    /** @test */
    public function it_should_create_status_message()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();

        $inbox->users()->saveMany([$user, $user2]);

        $accept = $inbox->acceptedBy($user);

        event(new InboxHasBeenAccepted($accept));

        $this->seeInDatabase('inbox_messages', [
            'inbox_id' => $inbox->id,
            'type' => 'status'
        ]);

        $this->assertCount(1, $inbox->load('messages')->messages);
    }

    /** @test */
    public function is_will_not_set_the_result_if_only_one_accepted()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();

        $inbox->users()->saveMany([$user, $user2]);
        $accept = $inbox->acceptedBy($user);

        event(new InboxHasBeenAccepted($accept));

        $this->assertNull($inbox->fresh()->match_result);
    }

    /** @test */
    public function is_will_set_the_result_to_success_if_both_accepted()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();

        $inbox->users()->saveMany([$user, $user2]);
        $inbox->acceptedBy($user);

        $accept = $inbox->acceptedBy($user2);

        event(new InboxHasBeenAccepted($accept));

        $this->assertTrue($inbox->fresh()->match_result);
    }
}
