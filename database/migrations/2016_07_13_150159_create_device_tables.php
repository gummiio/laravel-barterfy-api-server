<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeviceTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('devices', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('notification_token', 191)->nullable()->default(null);
            $table->string('device_platform', 191);

            $table->string('system_name', 191)->nullable()->default(null);
            $table->string('system_version', 191)->nullable()->default(null);

            $table->string('device_manufacturer', 191)->nullable()->default(null);
            $table->string('device_model', 191)->nullable()->default(null);
            $table->string('device_id', 191)->nullable()->default(null);
            $table->string('device_name', 191)->nullable()->default(null);
            $table->string('device_locale', 191)->nullable()->default(null);

            $table->boolean('notify_on_matches')->default(true);
            $table->boolean('notify_on_new_message')->default(true);

            $table->softDeletes();
            $table->timestamps();

            $table->unique(['notification_token', 'user_id', 'deleted_at']);

            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('devices');
    }
}
