<?php

namespace Barterfy\Item\Listeners\ItemDeleted;

use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Inbox\Repository\InboxRepository;
use Barterfy\Item\Events\ItemDeleted;
use Barterfy\Item\Model\Item;
use Illuminate\Contracts\Queue\ShouldQueue;

class CloseAllMatchingInboxes implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemDeleted  $event
     * @return void
     */
    public function handle(ItemDeleted $event)
    {
        $item = Item::withoutGlobalScopes()->find($event->itemId);

        $inboxes = InboxRepository::GetMatchingInboxesByItem($item);

        foreach ($inboxes as $inbox) {
            $inbox->fail();

            $message = $inbox->systemMessage(
                sprintf('%s has deleted the matching item.', $item->user()->withTrashed()->first()->display_name)
            );

            event(new NewMessageCreated($message));
        }
    }
}
