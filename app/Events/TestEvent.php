<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TestEvent extends Event implements ShouldBroadcast
{
    use SerializesModels;

    public $someData;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($someData)
    {
        $this->someData = $someData;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [env('REDIS_CHANNEL', '')];
    }
}
