<?php

namespace Barterfy\Inbox\Repository;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\InboxMessage;
use Barterfy\Inbox\Model\InboxMessageStatus;
use Barterfy\Repository;
use Illuminate\Http\Request;

class InboxMessageStatusRepository extends Repository
{
    public static function createUnreadStatusForMessage(InboxMessage $message)
    {
        $statuses = [];
        $inbox = $message->inbox;

        $inbox->users->each(function ($user) use (&$statuses) {
            $status = new InboxMessageStatus([
                'is_read' => false
            ]);

            $status->user_id = $user->id;

            $statuses[] = $status;
        });

        return $statuses;
    }

    public static function GetUserUnreadMessages(User $user)
    {
        return InboxMessageStatus::where('user_id', $user->id)
            ->where('is_read', false)
            ->get();
    }
}
