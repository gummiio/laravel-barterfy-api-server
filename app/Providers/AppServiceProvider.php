<?php

namespace App\Providers;

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::singularResourceParameters(false);

        Request::macro('isApi', function() {
            return Request::is('v2/*');
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('SlackHelper', function() {
            return new \Barterfy\Helpers\SlackHelper;
        });

        $this->app->bind('CloudinaryHelper', function() {
            return new \Barterfy\Helpers\CloudinaryHelper;
        });

        $this->app->singleton('FacebookHelper', function() {
            return \Barterfy\Helpers\FacebookHelper::init();
        });

        $this->app->singleton('PushNotificationHelper', function() {
            return new \Barterfy\Helpers\PushNotificationHelper;
        });
    }
}
