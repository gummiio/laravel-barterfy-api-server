<?php

namespace Barterfy\Api;

use Barterfy\Api\Model\ApiKey;

class ApiGuard
{
    /**
     * Api Token from the request header
     *
     * @var String
     */
    protected $requestToken = false;

    /**
     * Api Key Object
     *
     * @var Object
     */
    protected $apiKey = false;

    /**
     * Try to fetch the token from header
     *
     * @return Boolean
     */
    public function fetchApiToken()
    {
        return !! $this->getToken();
    }

    /**
     * Check the current api key status
     *
     * @return Boolean
     */
    public function verifiedApiToken($admin = false)
    {
        return !! $this->getApiKey($admin);
    }

    /**
     * Retreive the Api Key Object
     *
     * @return Object
     */
    public function getApiKey($admin = false)
    {
        if (false !== $this->apiKey) {
            return $this->apiKey;
        }

        return $this->apiKey = ApiKey::where('key', $this->getToken())
            ->where('is_admin', !!$admin)
            ->first();
    }

    /**
     * Get the header api key string
     *
     * @return string
     */
    public function getToken()
    {
        if (false !== $this->requestToken) {
            return $this->requestToken;
        }

        return $this->requestToken = Request()->header(Config()->get('api.header_key'));
    }
}
