<?php

namespace Barterfy\Item\Model;

use Barterfy\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemImage extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'item_images';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cloud_index', 'public_id', 'resource_type', 'url', 'path'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'item_id', 'cloud_index', 'public_id', 'path', 'deleted_at', 'updated_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * Delete the local file
     *
     */
    public function deleteLocalCopy()
    {
        return @unlink($this->path);
    }

    public function deleteFromCDN()
    {
        return App('CloudinaryHelper')::delete($this->public_id, $this->cloud_index);
    }
}
