<?php

namespace Barterfy\Access\Providers;

use Barterfy\Traits\Dispatchable;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Support\ServiceProvider;

class AccessServiceProvider extends ServiceProvider
{
    use Dispatchable;

    public $serviceEvents = [
        \Barterfy\Access\Events\NewUserRegistered::class => [
            \Barterfy\Access\Listeners\NewUserRegistered\NotifyOnSlack::class,
            \Barterfy\Access\Listeners\NewUserRegistered\CreateSupportInbox::class,
        ],
        \Barterfy\Access\Events\UserLoggedIn::class => [
            \Barterfy\Access\Listeners\UserLoggedIn\UpdateLastLoginTimestamp::class,
        ],
        \Barterfy\Access\Events\UserDeleted::class => [
            \Barterfy\Access\Listeners\UserDeleted\UnassociateAllRecords::class
        ],
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(DispatcherContract $dispatcher)
    {
        $this->dispatchServiceEvents($dispatcher);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
