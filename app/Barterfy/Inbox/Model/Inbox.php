<?php

namespace Barterfy\Inbox\Model;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\InboxAcceptance;
use Barterfy\Inbox\Model\InboxMessage;
use Barterfy\Inbox\Model\Relationships\InboxRelationships;
use Barterfy\Inbox\Model\Scopes\InboxActiveOnlyScope;
use Barterfy\Inbox\Model\Scopes\InboxExcludeSupportScope;
use Barterfy\Inbox\Model\Scopes\InboxScopes;
use Barterfy\Inbox\Model\Scopes\InboxUnmatchedOnlyScope;
use Barterfy\Inbox\Repository\InboxMessageRepository;
use Barterfy\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inbox extends Model
{
    use SoftDeletes, InboxRelationships, InboxScopes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'inboxes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'is_support'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'deleted_at', 'pivot'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $casts = [
        'is_support' => 'boolean',
        'match_result' => 'boolean',
        'active' => 'boolean',
        'total_messages' => 'integer',
        'last_message' => 'object'
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['users', 'items'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('messagesCount', function(Builder $builder) {
            $builder->withCount('messages');
        });
    }

    public function getLastMessageAttribute($value)
    {
        if (! $value) return new \stdClass;

        return json_decode($value);
    }

    /**
     * Close this inbox
     *
     */
    public function close()
    {
        $this->active = false;
        $this->save();

        return $this;
    }

    /**
     * Open this inbox
     *
     */
    public function open()
    {
        $this->active = true;
        $this->save();

        return $this;
    }

    /**
     * Set the match to success
     *
     */
    public function success()
    {
        $this->match_result = true;
        $this->active = false;
        $this->save();

        return $this;
    }

    /**
     * Set the match to fail
     *
     */
    public function fail()
    {
        $this->match_result = false;
        $this->active = false;
        $this->save();

        return $this;
    }

    public function acceptedBy(User $user)
    {
        $acceptance = new InboxAcceptance;
        $acceptance->user_id = $user->id;
        $acceptance->accepted = true;

        return $this->acceptances()->save($acceptance);
    }

    public function rejectedBy(User $user)
    {
        $acceptance = new InboxAcceptance;
        $acceptance->user_id = $user->id;
        $acceptance->accepted = false;

        return $this->acceptances()->save($acceptance);
    }

    public function regretedBy(User $user)
    {
        $acceptance = $this->acceptances()
            ->where('user_id', $user->id)
            // ->where('accepted', true)
            ->first();

        $acceptance->delete();

        return $acceptance;
    }

    public function systemMessage($message, $type = 'status')
    {
        $message = InboxMessageRepository::CreateSystemMessage($message, $type);

        $this->messages()->save($message);

        return $message;
    }

    public function getLastPage()
    {
        return floor($this->messages_count / 25) + 1;
    }

    public function inboxLink()
    {
        return url('admin/inboxes/' . $this->id . '?page=' . $this->getLastPage());
    }
}
