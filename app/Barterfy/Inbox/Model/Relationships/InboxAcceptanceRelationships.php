<?php

namespace Barterfy\Inbox\Model\Relationships;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;

trait InboxAcceptanceRelationships
{
    /**
     * An acceptance belongs to a user
     *
     * @return relationship model
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * An acceptance belongs to an inbox
     *
     * @return relationship model
     */
    public function inbox()
    {
        return $this->belongsTo(Inbox::class);
    }
}
