<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApiTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('api_keys', function (Blueprint $table) {
            $table->increments('id');
            $table->string('key', 64)->unique();
            $table->string('description', 191)->nullable()->default(null);
            $table->boolean('is_admin')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('api_logs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('api_key_id')->nullable()->default(null);
            $table->unsignedInteger('auth_user_id')->nullable()->default(null);
            $table->string('endpoint', 191);
            $table->string('method', 191);
            $table->text('params');
            $table->string('ip', 191);
            $table->string('user_agent', 191);
            $table->string('respond_code', 191);
            $table->decimal('respond_time', 16, 6);
            $table->timestamps();

            $table->foreign('api_key_id')
                ->references('id')->on('api_keys')->onDelete('cascade');

            $table->foreign('auth_user_id')
                ->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('api_logs');
        Schema::drop('api_keys');
    }
}
