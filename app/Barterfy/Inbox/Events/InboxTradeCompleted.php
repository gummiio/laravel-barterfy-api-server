<?php

namespace Barterfy\Inbox\Events;

use App\Events\Event;
use Barterfy\Inbox\Model\Inbox;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class InboxTradeCompleted extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * Inbox Object
     *
     * @var object
     */
    public $inbox;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Inbox $inbox)
    {
        $this->inbox = $inbox;
    }
}
