<?php

namespace Barterfy\Helpers;

use Maknz\Slack\Facades\Slack;

class SlackHelper
{
    /**
     * Message body
     *
     * @var string
     */
    protected $text = '';

    /**
     * Attachment color
     *
     * @var string
     */
    protected $color = 'good';

    /**
     * Attachment fields
     *
     * @var array
     */
    protected $fields = [];

    /**
     * Attachment thumbnail
     *
     * @var string
     */
    protected $thumbnail = '';

    /**
     * Attachment image
     *
     * @var string
     */
    protected $image = '';

    /**
     * Initilize a new instance
     *
     * @return object
     */
    public static function new()
    {
        return new static();
    }

    /**
     * Set the message body
     *
     * @param  string $text message body
     * @return object
     */
    public function text($text = '')
    {
        $this->text = $text
            ? $text . PHP_EOL . str_repeat('=', strlen($text))
            : '';

        return $this;
    }

    /**
     * Set the attachment Color
     *
     * @param  [type] $color hax code or string
     * @return object
     */
    public function color($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Add a field
     *
     * @param string  $content field content
     * @param string  $title   field title
     * @param boolean $short   field width
     */
    public function addField($content = '', $title = '', $short = false)
    {
        $field = [
            'title' => $title,
            'value' => is_array($content)? implode(PHP_EOL, $content) : $content,
            'short' => $short
        ];

        $this->fields[] = $field;

        return $this;
    }

    /**
     * Add a thumbnail
     *
     * @param  string $url image url
     */
    public function thumbnail($url)
    {
        $this->thumbnail = $url;

        return $this;
    }

    /**
     * Add a image
     *
     * @param  string $url image url
     */
    public function image($url)
    {
        $this->image = $url;

        return $this;
    }

    /**
     * Build the slack object
     *
     * @return object
     */
    public function build()
    {
        return Slack::attach([
            'text' => $this->text,
            'color' => $this->color,
            'fields' => $this->fields,
            'thumb_url' => $this->thumbnail,
            'image_url' => $this->image
        ]);
    }

    /**
     * Send the slack message
     *
     * @return void
     */
    public function send($text = '')
    {
        $this->build()->send($text);
    }

    /**
     * Send the spesific slack channel
     */
    public function sendTo($channel, $text = '')
    {
        $this->build()->to($channel)->send($text);
    }

}
