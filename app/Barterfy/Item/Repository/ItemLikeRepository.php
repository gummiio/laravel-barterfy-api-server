<?php

namespace Barterfy\Item\Repository;

use Barterfy\Repository;

class ItemLikeRepository extends Repository
{
    public static function UserItemsLikedBy($user, $owner, $excepts = [])
    {
        $itemLike = $user->itemLikes()
            ->with('item')
            ->leftJoin('items', 'items.id', '=', 'item_likes.item_id')
            ->where('items.is_traded', false)
            ->whereNull('items.deleted_at')
            ->where('items.user_id', $owner->id)
            ->where('item_likes.liked', true)
            ->inRandomOrder()
            ->first();

        return $itemLike? $itemLike->item : false;
    }
}
