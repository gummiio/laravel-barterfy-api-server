<?php


Route::get('/', ['middleware' => 'auth:app', function () {
    return Response::apiSuccess("Yeeep app", [], 200);
}]);

Route::get('/long', ['middleware' => 'auth:app', function () {
    sleep(6);
    return Response::apiSuccess("yerpp", [], 200);
}]);

/*
|--------------------------------------------------------------------------
| API Routes, No Auth Required
|--------------------------------------------------------------------------
*/
Route::post('users/register', 'AccessController@register');
Route::post('users/login', 'AccessController@login');
Route::post('devices/send-test', 'DeviceController@send_test');

Route::get('status', function() {
    return response()->apiSuccess('Api Running');
});

/*
|--------------------------------------------------------------------------
| API Routes, Auth Required
|--------------------------------------------------------------------------
*/

Route::group(['middleware' => 'auth:app'], function() {

    /*
     |----------------------------------------------------------------
     |  Users
     |----------------------------------------------------------------
     */
    Route::get('users/me', 'UserController@me');
    Route::get('users/stats', 'UserController@stats');
    Route::put('users/me', 'UserController@update');

    /*
     |----------------------------------------------------------------
     |  Devices
     |----------------------------------------------------------------
     */
    Route::resource('devices', 'DeviceController', [
        'only' => ['index', 'store', 'show', 'update', 'destroy']
    ]);

    /*
     |----------------------------------------------------------------
     |  Items
     |----------------------------------------------------------------
     */
    Route::get('items/search', 'ItemController@search');
    Route::post('items/{items}/like', 'ItemController@like');
    Route::post('items/{items}/dislike', 'ItemController@dislike');
    Route::post('items/{items}/report', 'ItemController@report');

    Route::resource('items', 'ItemController', [
        'only' => ['index', 'store', 'show', 'update', 'destroy']
    ]);

    /*
     |----------------------------------------------------------------
     |  Inboxes
     |----------------------------------------------------------------
     */
    Route::get('inboxes/support', 'InboxController@support');
    Route::post('inboxes/{inboxes}/accept', 'InboxController@accept');
    Route::post('inboxes/{inboxes}/reject', 'InboxController@reject');
    Route::post('inboxes/{inboxes}/regret', 'InboxController@regret');

    Route::resource('inboxes', 'InboxController', [
        'only' => ['index', 'show']
    ]);

    /*
     |----------------------------------------------------------------
     |  Inbox Items
     |----------------------------------------------------------------
     */
    // Route::resource('inboxes.items', 'InboxItemController', [
    //     'only' => ['index']
    // ]);

    /*
     |----------------------------------------------------------------
     |  Inbox Messages
     |----------------------------------------------------------------
     */
    Route::resource('inboxes.messages', 'InboxMessageController', [
        'only' => ['index', 'store']
    ]);

    /*
     |----------------------------------------------------------------
     |  Utility
     |----------------------------------------------------------------
     */
    Route::get('api_status', function() {
        return response()->apiSuccess('Api Running');
    });
});
