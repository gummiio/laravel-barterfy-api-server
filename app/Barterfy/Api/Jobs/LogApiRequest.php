<?php

namespace Barterfy\Api\Jobs;

use App\Jobs\Job;
use Barterfy\Api\Events\LongApiRespondTimeExecuted;
use Barterfy\Api\Model\ApiLog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Config;

class LogApiRequest extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $logData;
    protected $time;

    /**
     * Create a new Log instance.
     *
     * @return void
     */
    public function __construct($logData, $time)
    {
        $this->logData = $logData;
        $this->time = $time;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $apiLog = new ApiLog($this->logData);
        $apiLog->timestamps = false;
        $apiLog->created_at = $apiLog->updated_at = $this->time;

        $apiLog->save();

        if ($apiLog->respond_time > Config::get('api.long_request_threshold')) {
            event(new LongApiRespondTimeExecuted($apiLog));
        }
    }
}
