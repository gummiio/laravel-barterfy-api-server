<?php

namespace Barterfy\Item\Events;

use App\Events\Event;
use Barterfy\Item\Model\Item;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class ItemReported extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * Item Object
     *
     * @var object
     */
    public $item;
    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Item $item, $message = '')
    {
        $this->item = $item;
        $this->message = $message;
    }
}
