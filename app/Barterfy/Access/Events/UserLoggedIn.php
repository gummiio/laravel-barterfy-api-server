<?php

namespace Barterfy\Access\Events;

use App\Events\Event;
use Barterfy\Access\Model\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class UserLoggedIn extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * User Object
     *
     * @var object
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}
