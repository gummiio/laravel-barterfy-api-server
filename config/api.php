<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Header Key
    |--------------------------------------------------------------------------
    |
    | Number of second for the server to decide if the current request is
    | consider too long, and trigger the LongApiRespondTimeExecuted event.
    |
    */
    'header_key' => 'BARTERFY-API',

    /*
    |--------------------------------------------------------------------------
    | Long Request Threshold
    |--------------------------------------------------------------------------
    |
    | Number of second for the server to decide if the current request is
    | consider too long, and trigger the LongApiRespondTimeExecuted event.
    |
    */
    'long_request_threshold' => 5
);
