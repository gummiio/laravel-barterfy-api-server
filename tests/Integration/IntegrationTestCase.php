<?php

namespace Integration;

use Barterfy\Access\Model\User;
use Barterfy\Api\Model\ApiKey;
use Barterfy\Helpers\PushNotificationHelper;
use Barterfy\Helpers\SlackHelper;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;

class IntegrationTestCase extends \TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    protected $mocked = false;

    public function setUp()
    {
        parent::setUp();

        $this->mockSlackSending();
        $this->mockNotificationSending();
    }

    public function tearDown()
    {
        $this->beforeApplicationDestroyed(function () {
            \DB::disconnect();
        });

        if ($this->mocked) {
            Mockery::close();
        }

        parent::tearDown();
    }

    public function mockSlackSending()
    {
        $slackHelper = Mockery::mock(SlackHelper::class)->shouldDeferMissing();
        $slackHelper->shouldReceive('send')->andReturn(true);
        $slackHelper->shouldReceive('sendTo')->andReturn(true);

        $this->app->instance('SlackHelper', $slackHelper);
        $this->mocked = true;
    }

    public function mockNotificationSending()
    {
        $pushNotificationHelper = Mockery::mock(PushNotificationHelper::class)->shouldDeferMissing();
        $pushNotificationHelper->shouldReceive('send')->andReturn(true);

        $this->app->instance('PushNotificationHelper', $pushNotificationHelper);
        $this->mocked = true;
    }
}
