<?php

namespace Barterfy\Inbox\Model\Relationships;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\InboxAcceptance;
use Barterfy\Inbox\Model\InboxMessage;
use Barterfy\Item\Model\Item;

trait InboxRelationships
{
    /**
     * A inbox can have many messages
     *
     * @return relationship object
     */
    public function messages()
    {
        return $this->hasMany(InboxMessage::class);
    }

    /**
     * A inbox can have many users
     *
     * @return relationship object
     */
    public function users()
    {
        return $this->belongsToMany(User::class, 'inbox_users');
    }

    /**
     * A inbox can have many acceptances
     *
     * @return relationship object
     */
    public function acceptances()
    {
        return $this->hasMany(InboxAcceptance::class);
    }

    /**
     * A inbox can have many items
     *
     * @return relationship object
     */
    public function items()
    {
        return $this->belongsToMany(Item::class, 'inbox_items')->withTrashed();
    }
}
