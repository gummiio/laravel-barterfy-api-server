<?php

namespace Barterfy\Access\Events;

use App\Events\Event;
use Barterfy\Access\Model\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class UserDeleted extends Event implements ShouldQueue
{

    /**
     * User ID
     *
     * @var object
     */
    public $userID;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->userID = $user->getKey();
    }
}
