// Load basics
var express     = require('express');
var app         = express();
var server      = require('http').createServer(app);
var io          = require('socket.io')(server);
var socketioJwt = require('socketio-jwt');
var mysql       = require('mysql');

var Redis       = require('ioredis');
var redis       = new Redis();

require('dotenv').config({path : '../.env'});

var mysqlConfig = {
    host     : process.env.DB_HOST,
    user     : process.env.DB_USERNAME,
    password : process.env.DB_PASSWORD,
    database : process.env.DB_DATABASE
};
// ==========================================================================

// Start Node server at port 80
server.listen(process.env.SOCKET_PORT, function(){
    console.log('Listening on Port ' + process.env.SOCKET_PORT);
});

redis.subscribe(process.env.REDIS_CHANNEL, function(err, count) {
    console.log('Subscribes to Redis: ' + process.env.REDIS_CHANNEL);
});

// Accept connection and authorize token
io.on('connection', socketioJwt.authorize({
    secret: process.env.JWT_SECRET,
    timeout: 15000
}));

// When authenticated, send back name
io.on('authenticated', function (socket) {
    socket.emit('user_' + socket.decoded_token.id + ':AuthSuccess', socket.decoded_token);

    setUserAsOnline(socket.decoded_token.id);

    socket.on('disconnect', function(e) {
        setUserAsOffline(socket.decoded_token.id);
    });

    redis.on('message', function(channel, message) {
        message = JSON.parse(message);
        var event = message.event;
        var data = message.data;
        // console.log('Redis received: ' + event + ' == ' + data);

        switch (event) {
            case 'Barterfy\\Inbox\\Events\\MatchInboxCreated' :
                data.sendTo.forEach(function(value) {
                    // console.log('user_' + value + ':MatchInboxCreated');
                    var inbox = data.inbox;

                    inbox.items.forEach(function(item, index) {
                        inbox.items[index].is_yours = item.user.id == value;
                        inbox.items[index].user.is_you = item.user.id == value;
                    });

                    inbox.users.forEach(function(user, index) {
                        inbox.users[index].is_you = user.id == value;
                    });

                    socket.emit('user_' + value + ':MatchInboxCreated', inbox);
                });
                break;

            case 'Barterfy\\Inbox\\Events\\NewMessageCreated' :
                data.sendTo.forEach(function(value) {
                    // console.log('user_' + value + ':NewMessageCreated');
                    var message = data.message;

                    // fix is_you value
                    if (message.user) {
                        message.user.is_you = message.user.id == value;
                    }

                    socket.emit('user_' + value + ':NewMessageCreated', message);
                });
                break;

            case 'Barterfy\\Inbox\\Events\\NewSupportMessageCreated' :
                data.sendTo.forEach(function(value) {
                    // console.log('user_' + value + ':NewSupportMessageCreated');
                    socket.emit('user_' + value + ':NewSupportMessageCreated', data.message);
                });
                break;

            case 'Barterfy\\Item\\Events\\ItemIsReadied' :
                data.sendTo.forEach(function(value) {
                    // console.log('user_' + value + ':ItemIsReadied');
                    socket.emit('user_' + value + ':ItemIsReadied', data.item);
                });
                break;
        }
    });
});

function setUserAsOnline(userId) {
    var connection  = mysql.createConnection(mysqlConfig);
    connection.query('UPDATE users SET online=1 WHERE id=' + userId);
    connection.end();
}

function setUserAsOffline(userId) {
    var connection  = mysql.createConnection(mysqlConfig);
    connection.query('UPDATE users SET online=0 WHERE id=' + userId);
    connection.end();
}
