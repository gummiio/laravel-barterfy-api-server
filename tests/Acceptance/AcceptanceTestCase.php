<?php

namespace Acceptance;

use Barterfy\Access\Model\User;
use Barterfy\Api\Model\ApiKey;
use Barterfy\Helpers\PushNotificationHelper;
use Barterfy\Helpers\SlackHelper;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery;

class AcceptanceTestCase extends \TestCase
{
    use DatabaseMigrations, DatabaseTransactions;

    protected $apiKey;

    protected $graphUser;

    protected $testUser;

    protected $mocked = false;

    public function setUp()
    {
        parent::setUp();

        $this->prepareApiKey();
        $this->mockSlackSending();
        $this->mockNotificationSending();
    }

    public function tearDown()
    {
        $this->beforeApplicationDestroyed(function () {
            \DB::disconnect();
        });

        if ($this->mocked) {
            Mockery::close();
        }

        parent::tearDown();
    }

    public function headers($user = null)
    {
        $headers = [
            'HTTP_' . config('api.header_key') => $this->appKey
        ];

        if ($user && $user instanceof User) {
            $headers['HTTP_Authorization'] = 'Basic ' . $user->auth_token;
        }

        return $headers;
    }

    public function prepareApiKey()
    {
        $this->appKey = factory(ApiKey::class)->create()->key;
    }

    public function prepareGraphUser()
    {
        $this->graphUser = App('FacebookHelper')->createTestUser();
    }

    public function removeGraphUser()
    {
        App('FacebookHelper')->deleteTestUser($this->graphUser['id']);
    }

    public function mockSlackSending()
    {
        $slackHelper = Mockery::mock(SlackHelper::class)->shouldDeferMissing();
        $slackHelper->shouldReceive('send')->andReturn(true);
        $slackHelper->shouldReceive('sendTo')->andReturn(true);

        $this->app->instance('SlackHelper', $slackHelper);
        $this->mocked = true;
    }

    public function mockNotificationSending()
    {
        $pushNotificationHelper = Mockery::mock(PushNotificationHelper::class)->shouldDeferMissing();
        $pushNotificationHelper->shouldReceive('send')->andReturn(true);

        $this->app->instance('PushNotificationHelper', $pushNotificationHelper);
        $this->mocked = true;
    }

    public function jsonData()
    {
        return json_decode($this->response->getContent())->data;
    }
}
