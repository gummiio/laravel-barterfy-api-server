<?php

namespace Acceptance\Users;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;

class UpdateTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->put('v2/users/me', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_fail_if_missing_location()
    {
        $user = factory(User::class)->create();

        $this->put('v2/users/me', [], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(422);
    }

    /** @test */
    public function it_should_fail_if_invalid_location()
    {
        $user = factory(User::class)->create();

        $this->put('v2/users/me', [
            'longitude' => 'yoyo',
            'latitude' => 'wuuut?'
        ], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(422);
    }

    /** @test */
    public function it_should_update_location()
    {
        $user = factory(User::class)->create();

        $this->put('v2/users/me', [
            'longitude' => '123',
            'latitude' => '4567'
        ], $this->headers($user));

        $this->seeJson(['success' => true]);
        $this->seeJson(['longitude' => 123]);
        $this->seeJson(['latitude' => 4567]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function it_can_accept_and_update_city_and_country_as_well()
    {
        $user = factory(User::class)->create();

        $this->put('v2/users/me', [
            'longitude' => '123',
            'latitude' => '4567',
            'city' => 'abcd',
            'country' => 'asdfsadf',
        ], $this->headers($user));

        $this->seeJson(['success' => true]);
        $this->seeJson(['longitude' => 123]);
        $this->seeJson(['latitude' => 4567]);
        $this->seeJson(['city' => 'abcd']);
        $this->seeJson(['country' => 'asdfsadf']);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function it_should_not_update_anything_else()
    {
        $user = factory(User::class)->create();

        $this->put('v2/users/me', [
            'longitude' => '123',
            'latitude' => '4567',
            'username' => 'yep',
            'avatar' => 'asdfsadf',
        ], $this->headers($user));

        $freshUser = $user->fresh();

        $this->seeJson(['success' => true]);
        $this->seeJson(['longitude' => 123]);
        $this->seeJson(['latitude' => 4567]);

        $this->assertResponseStatus(200);

        $this->assertEquals($user->username, $freshUser->username);
        $this->assertEquals($user->avatar, $freshUser->avatar);
    }
}
