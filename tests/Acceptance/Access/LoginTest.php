<?php

namespace Acceptance\Access;

use Acceptance\AcceptanceTestCase;

class LoginTest extends AcceptanceTestCase
{
    /** @test */
    public function user_cannnot_login_without_facebook_token()
    {
        $this->post('v2/users/login', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->seeJson([
            'type' => 'ValidationFailed'
        ]);

        $this->assertResponseStatus(422);
    }

    /** @test */
    public function user_cannot_login_with_invalid_facebook_token()
    {
        $data = [
            'facebook_token' => 'abcde'
        ];

        $this->post('v2/users/login', $data, $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }
}
