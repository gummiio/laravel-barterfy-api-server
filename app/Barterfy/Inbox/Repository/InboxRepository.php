<?php

namespace Barterfy\Inbox\Repository;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Inbox\Model\Scopes\InboxExcludeSupportScope;
use Barterfy\Item\Model\Item;
use Barterfy\Repository;
use Carbon\Carbon;
use Illuminate\Http\Request;

class InboxRepository extends Repository
{
    public static function GetInboxStatsByUser(User $user)
    {
        $inboxes = $user->inboxes()->notSupport()->get();

        return [
            'matches' => $inboxes->count(),
            'matching' => $inboxes->whereStrict('match_result', null)->count(),
            'successed' => $inboxes->whereStrict('match_result', true)->count(),
            'failed' => $inboxes->whereStrict('match_result', false)->count()
        ];
    }

    public static function UsersHaveActiveInbox($users)
    {
        $users = collect($users);

        return false !== $users[0]->inboxes()->with('users')->get()
            ->search(function ($inbox) use ($users) {
                return $inbox->users->sortBy('id')->pluck('id') == $users->sortBy('id')->pluck('id');
            });
    }

    public static function GetInboxesByUser(User $user, Request $request)
    {
        $query = $user->inboxes()
            ->orderBy('updated_at', 'desc')
            ->limit($request->input('limit', 10));

        if ($request->has('before')) {
            $before = Carbon::createFromFormat('Y-m-d H:i:s', $request->before);
            $query->where('updated_at', '<' , $before);
        }

        if (! $request->input('with_support', false)) {
            $query->notSupport();
        }

        switch ($request->input('match_result', 'matching')) {
            case 'successed':
                $query->successed();
                break;
            case 'failed':
                $query->failed();
                break;
            case 'matching':
                $query->matching();
                break;
        }

        return $query->get();
    }

    public static function CreateSupportInboxForUser(User $user)
    {
        $inbox = new Inbox([
            'name' => 'Support Inbox: ' . $user->full_name
        ]);

        $inbox->is_support = true;

        return $user->inboxes()->save($inbox);
    }

    public static function GetUserSupportInbox(User $user)
    {
        return $user->inboxes()
            ->support()
            ->first();
    }

    public static function UserHasMadeDecision(User $user, Inbox $inbox)
    {
        return $inbox->acceptances()->where('user_id', $user->id)->first();
    }

    public static function GetMatchingInboxesByItem(Item $item)
    {
        return Inbox::select('inboxes.*')
            ->join('inbox_items', 'inbox_items.inbox_id', '=', 'inboxes.id')
            ->where('inbox_items.item_id', $item->id)
            ->matching()
            ->get();
    }
}
