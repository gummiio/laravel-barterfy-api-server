<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Barterfy\Access\Requests\UserUpdateRequest;
use Barterfy\Inbox\Model\Scopes\InboxActiveOnlyScope;
use Barterfy\Inbox\Model\Scopes\InboxUnmatchedOnlyScope;
use Barterfy\Inbox\Repository\InboxRepository;
use Barterfy\Item\Repository\ItemRepository;
use Illuminate\Http\Request;

class UserController extends ApiController
{
    public function me()
    {
        return response()->apiSuccess('', Auth()->user());
    }

    public function stats()
    {
        $stats = InboxRepository::GetInboxStatsByUser(Auth()->user());

        return response()->apiSuccess('', $stats);
    }

    public function update(UserUpdateRequest $request)
    {
        Auth()->user()->update($request->only([
            'country', 'city', 'longitude', 'latitude'
        ]));

        return response()->apiSuccess('User updated.', Auth()->user());
    }
}
