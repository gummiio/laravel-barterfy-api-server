<?php

namespace Barterfy\Item\Listeners\ItemIsReadied;

use Barterfy\Item\Events\ItemIsReadied;
use Illuminate\Contracts\Queue\ShouldQueue;

class NotifyOnSlack implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ItemIsReadied  $event
     * @return void
     */
    public function handle(ItemIsReadied $event)
    {
        if (! $event->shouldNotify) return;

        $item = $event->item;
        $user = $item->user;

        app('SlackHelper')
            ->text('*A new item has been posted.*')
            ->color('good')
            ->addField([
                "*Item ID:* <" . $item->getItemUrl() . "|{$item->id}>",
                "*User:* {$user->full_name}",
                "*Description:* {$item->description}",
                "*At:* " . $item->created_at->toDateTimeString()
            ])
            ->thumbnail($item->images->first()->url)
            ->send();
    }
}
