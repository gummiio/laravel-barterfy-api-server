<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInboxTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inboxes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 191);
            $table->boolean('is_support')->default(false);
            $table->boolean('match_result')->nullable()->default(null);
            $table->boolean('active')->default(true);
            $table->integer('total_messages')->default(0);
            $table->text('last_message')->nullable()->default(null);

            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('inbox_users', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inbox_id');
            $table->unsignedInteger('user_id');

            $table->foreign('inbox_id')
                ->references('id')->on('inboxes')->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('inbox_items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inbox_id');
            $table->unsignedInteger('item_id');

            $table->foreign('inbox_id')
                ->references('id')->on('inboxes')->onDelete('cascade');

            $table->foreign('item_id')
                ->references('id')->on('items')->onDelete('cascade');
        });

        Schema::create('inbox_acceptances', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inbox_id');
            $table->unsignedInteger('user_id');
            $table->boolean('accepted');

            $table->timestamps();

            $table->foreign('inbox_id')
                ->references('id')->on('inboxes')->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('inbox_messages', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inbox_id');
            $table->unsignedInteger('user_id')->nullable()->default(null);
            $table->string('type', 191)->default('conversation');
            $table->longText('message');

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('inbox_id')
                ->references('id')->on('inboxes')->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('inbox_message_statuses', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('inbox_message_id');
            $table->unsignedInteger('user_id');
            $table->boolean('is_read')->default(false);

            $table->timestamps();

            $table->foreign('inbox_message_id')
                ->references('id')->on('inbox_messages')->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('inbox_message_statuses');
        Schema::drop('inbox_messages');
        Schema::drop('inbox_acceptances');
        Schema::drop('inbox_items');
        Schema::drop('inbox_users');
        Schema::drop('inboxes');
    }
}
