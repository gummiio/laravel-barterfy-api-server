<?php

namespace Barterfy\Inbox\Listeners\NewMessageCreated;

use Barterfy\Inbox\Events\NewMessageCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UpdateInboxProps implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewMessageCreated  $event
     * @return void
     */
    public function handle(NewMessageCreated $event)
    {
        $message = $event->message;
        $inbox = $message->inbox;

        $message = $message->fresh()->load('user');

        $inbox->total_messages = $inbox->total_messages + 1;
        $inbox->last_message = $message->toArray();
        $inbox->save();
    }
}
