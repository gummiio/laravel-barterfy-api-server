<?php

namespace Acceptance\InboxMessages;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Events\NewSupportMessageCreated;
use Barterfy\Inbox\Model\Inbox;

class StoreTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_return_resource_not_found_even_unauthenticate()
    {
        $this->post('v2/inboxes/1/messages', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate_when_inbox_exists()
    {
        $user = factory(User::class)->create();
        $user->inboxes()->save(
            factory(Inbox::class)->make()
        );

        $this->post('v2/inboxes/1/messages', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_id_does_not_exists()
    {
        $user = factory(User::class)->create();

        $this->post('v2/inboxes/1/messages', [], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_failed_if_inbox_does_not_belong_to_you()
    {
        $other = factory(User::class)->create();
        $other->inboxes()->save(
            factory(Inbox::class)->make()
        );

        $you = factory(User::class)->create();

        $this->post('v2/inboxes/1/messages', [], $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function it_should_fail_if_no_message_is_posted()
    {
        $user = factory(User::class)->create();
        $user->inboxes()->save(
            factory(Inbox::class)->make()
        );

        $this->post('v2/inboxes/1/messages', [], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(422);
    }

    /** @test */
    public function it_should_fail_if_inbox_is_inactive()
    {
        $user = factory(User::class)->create();
        $user->inboxes()->save(
            factory(Inbox::class)->make(['active' => false])
        );

        $this->post('v2/inboxes/1/messages', [
            'message' => 'asdf'
        ], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(400);
    }

    /** @test */
    public function it_should_success_and_call_NewMessageCreated_event_for_normal_message()
    {
        $user = factory(User::class)->create();
        $user->inboxes()->save(
            factory(Inbox::class)->make()
        );

        $this->expectsEvents(NewMessageCreated::class);
        $this->doesntExpectEvents(NewSupportMessageCreated::class);

        $this->post('v2/inboxes/1/messages', [
            'message' => 'asdf'
        ], $this->headers($user));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(201);

        $this->seeJson(['message' => 'asdf']);
    }

    /** @test */
    public function it_should_success_and_call_NewSupportMessageCreated_event_for_support_message()
    {
        $user = factory(User::class)->create();
        $user->inboxes()->save(
            factory(Inbox::class)->make(['is_support' => true])
        );

        $this->doesntExpectEvents(NewMessageCreated::class);
        $this->expectsEvents(NewSupportMessageCreated::class);

        $this->post('v2/inboxes/1/messages', [
            'message' => 'asdf'
        ], $this->headers($user));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(201);

        $this->seeJson(['message' => 'asdf']);
    }

}
