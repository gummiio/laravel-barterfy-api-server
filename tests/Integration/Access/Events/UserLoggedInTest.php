<?php

namespace Integration\Access\Events;

use Barterfy\Access\Events\UserLoggedIn;
use Barterfy\Access\Model\User;
use Integration\IntegrationTestCase;

class UserLoggedInTest extends IntegrationTestCase
{
    /** @test */
    public function last_login_will_update_when_login()
    {
        $user = factory(User::class)->create();

        $last_login = $user->last_login;

        event(new UserLoggedIn($user));

        $this->assertNotEquals($last_login, $user->fresh()->last_login);
    }

    /** @test */
    public function update_at_should_not_change_if_login_time_updated()
    {
        $user = factory(User::class)->create();

        $updated_at = $user->updated_at;

        event(new UserLoggedIn($user));

        $this->assertEquals($updated_at, $user->fresh()->updated_at);
    }
}
