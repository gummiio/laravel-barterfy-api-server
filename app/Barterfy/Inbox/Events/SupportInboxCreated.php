<?php

namespace Barterfy\Inbox\Events;

use App\Events\Event;
use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class SupportInboxCreated extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * Inbox Object
     *
     * @var object
     */

    public $inbox;
    /**
     * User Object
     *
     * @var object
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Inbox $inbox, User $user)
    {
        $this->inbox = $inbox;
        $this->user = $user;
    }
}
