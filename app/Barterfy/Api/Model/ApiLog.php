<?php

namespace Barterfy\Api\Model;

use Barterfy\Access\Model\AdminUser;
use Barterfy\Access\Model\User;
use Barterfy\Api\Model\Relationships\ApiLogRelationships;
use Barterfy\Model;

class ApiLog extends Model
{
    use ApiLogRelationships;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'api_logs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'api_key_id', 'api_token', 'auth_user_id',
        'endpoint', 'method', 'params', 'ip', 'user_agent',
        'respond_code', 'respond_time'
    ];

    /**
     * Get auth user
     *
     * @return Object
     */
    public function getAuthUser()
    {
        if ( ! $this->apiKey) return null;

        return $this->apiKey->is_admin?
            $this->belongsTo(AdminUser::class)->withTrashed() :
            $this->belongsTo(User::class)->withTrashed();
    }
}
