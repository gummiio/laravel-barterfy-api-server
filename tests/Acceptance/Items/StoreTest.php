<?php

namespace Acceptance\Items;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Item\Events\ItemCreated;
use Barterfy\Item\Model\Item;
use Barterfy\Item\Model\Scopes\ItemIsReadiedScope;
use Mockery;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class StoreTest extends AcceptanceTestCase
{
    public function tearDown()
    {
        $this->itemsCleanUp();

        parent::tearDown();
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->post('v2/items/', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_no_data_is_posted()
    {
        $user = factory(User::class)->create();

        $this->post('v2/items/', [], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(422);
    }

    /** @test */
    public function is_shoud_failed_if_required_data_is_invalid()
    {
        $user = factory(User::class)->create();

        $this->post('v2/items/', [
            'description' => 'test item',
            'images' => 'asdf'
        ], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(422);
    }

    /** @test */
    public function new_item_and_images_will_be_unreadied_after_upload()
    {
        $user = factory(User::class)->create();

        $this->expectsEvents(ItemCreated::class);

        $this->call(
            'POST', 'v2/items/', ['description' => 'test item'], [],
            ['images' => $this->prepareImages()],
            [
                'HTTP_BARTERFY_API' => $this->appKey,
                'HTTP_AUTHORIZATION' => 'Basic ' . $user->auth_token
            ]
        );

        $this->seeJson(['success' => true]);
        $this->seeJson(['is_readied' => false]);

        $this->assertResponseStatus(201);

        $item = Item::find($this->jsonData()->id);

        $this->assertGreaterThan(0, $item->images()->count());
    }

    // =====================================================================
    public function prepareImages()
    {
        $image = new UploadedFile(
            storage_path('tests/test-image-1.png'),
            'test-image-1.png',
            'image/png',
            446, null, TRUE
        );

        $image2 = new UploadedFile(
            storage_path('tests/test-image-2.png'),
            'test-image-2.png',
            'image/png',
            446, null, TRUE
        );

        return [$image, $image2];
    }

    public function itemsCleanUp($fromCloud = false)
    {
        Item::withoutGlobalScopes()->with('images')->get()->each(function ($item) {
            $item->images->each(function ($image) {
                if ($image->public_id) {
                    $image->deleteFromCDN();
                } else {
                    $image->deleteLocalCopy();
                }
            });
        });
    }
}
