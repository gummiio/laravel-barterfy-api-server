<?php

namespace Integration\Item\Events;

use Barterfy\Access\Model\User;
use Barterfy\Item\Events\ItemCreated;
use Barterfy\Item\Events\ItemIsReadied;
use Barterfy\Item\Model\Item;
use Barterfy\Item\Model\ItemImage;
use Integration\IntegrationTestCase;

class ItemCreatedTest extends IntegrationTestCase
{
    public function tearDown()
    {
        $this->itemsCleanUp();

        parent::tearDown();
    }

    /** @test */
    public function new_item_images_will_be_processed_to_cdn()
    {
        $user = factory(User::class)->create();
        $item = $user->items()->save(
            factory(Item::class)->make([
                'is_traded' => false,
                'is_readied' => false
            ])
        );

        $tempImage = storage_path('temp/test-upload.png');
        copy(storage_path('tests/test-image-1.png'), $tempImage);

        $item->images()->save(
            new ItemImage([
                'path' => $tempImage,
                'cloud_index' => 1
            ])
        );

        event(new ItemCreated($item));

        $item->fresh()->images->each(function ($image) {
            $this->assertNotNull($image->public_id);
            $this->assertNotNull($image->url);
            $this->assertTrue(App('CloudinaryHelper')::exists($image->public_id));
        });
    }

    /** @test */
    public function new_item_will_be_ready_after_image_is_processed()
    {
        $user = factory(User::class)->create();
        $item = $user->items()->save(
            factory(Item::class)->make([
                'is_traded' => false,
                'is_readied' => false
            ])
        );

        $tempImage = storage_path('temp/test-upload.png');
        copy(storage_path('tests/test-image-1.png'), $tempImage);

        $item->images()->save(
            new ItemImage([
                'path' => $tempImage,
                'cloud_index' => 1
            ])
        );

        event(new ItemCreated($item));

        $item = $item->fresh();

        $this->assertTrue($item->is_readied);
    }

    // =====================================================================
    public function itemsCleanUp($fromCloud = false)
    {
        Item::withoutGlobalScopes()->with('images')->get()->each(function ($item) {
            $item->images->each(function ($image) {
                if ($image->public_id) {
                    $image->deleteFromCDN();
                } else {
                    $image->deleteLocalCopy();
                }
            });
        });
    }
}
