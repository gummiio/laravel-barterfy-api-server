<?php

namespace Barterfy\Api\Exceptions;

use Barterfy\Api\Exceptions\ApiResponseException;

class ApiForbiddenException extends ApiResponseException
{
    public function __construct()
    {
        parent::__construct('Forbidden.', [
            'type'    => 'NotOwnByYou',
            'message' => 'Unable to process the action because the resource doesn\'t belong to you.'
        ], 403);
    }
}
