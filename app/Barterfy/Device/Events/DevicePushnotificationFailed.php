<?php

namespace Barterfy\Device\Events;

use App\Events\Event;
use Barterfy\Device\Model\Device;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class DevicePushNotificationFailed extends Event implements ShouldQueue
{
    use SerializesModels;

    /**
     * Device Object
     *
     * @var object
     */
    public $device;

    public $error;

    public $message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Device $device, $error, $message)
    {
        $this->device = $device;
        $this->error = $error;
        $this->message = $message;
    }
}
