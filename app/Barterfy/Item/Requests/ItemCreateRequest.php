<?php

namespace Barterfy\Item\Requests;

use Barterfy\Api\Requests\ApiRequest;

class ItemCreateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'description' => 'required',
            'images' => 'required|array',
            //'images.*' => 'image'
        ];
    }
}
