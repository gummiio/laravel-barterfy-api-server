<?php

namespace Barterfy\Device\Model\Relationships;

use Barterfy\Access\Model\User;

trait DeviceRelationships
{
    /**
     * A device belongs to a user
     *
     * @return relation object
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
