<?php

namespace Acceptance\Inboxes;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Inbox\Model\Inbox;

class SupportTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_fail_if_unable_to_authenticate()
    {
        $this->get('v2/inboxes/support', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_return_your_support_inbox()
    {
        $user = factory(User::class)->create();
        $user->inboxes()->saveMany(
            factory(Inbox::class, 5)->make(['match_result' => null])
        );
        $support = $user->inboxes()->save(
            factory(Inbox::class)->make(['is_support' => true, 'match_result' => null])
        );

        factory(User::class)->create()->inboxes()->save(
            factory(Inbox::class)->make(['is_support' => true, 'match_result' => null])
        );

        $this->get('v2/inboxes/support', $this->headers($user));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);

        $this->seeJson(['is_support' => true]);
    }

}
