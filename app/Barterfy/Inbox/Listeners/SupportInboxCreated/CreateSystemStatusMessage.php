<?php

namespace Barterfy\Inbox\Listeners\SupportInboxCreated;

use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Events\NewSupportMessageCreated;
use Barterfy\Inbox\Events\SupportInboxCreated;
use Illuminate\Contracts\Queue\ShouldQueue;

class CreateSystemStatusMessage implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  SupportInboxCreated  $event
     * @return void
     */
    public function handle(SupportInboxCreated $event)
    {
        $inbox = $event->inbox;

        $message = $inbox->systemMessage(
            'Hi! Welcome to Barterfy. Swipe Right to Swap!' . PHP_EOL .
            'This is your support inbox. If you have any questions, you can just message me here.'
        , 'conversation', false);

        event(new NewSupportMessageCreated($message, false));
    }
}
