<?php

namespace Barterfy\Item\Providers;

use Barterfy\Traits\Dispatchable;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Support\ServiceProvider;

class ItemServiceProvider extends ServiceProvider
{
    use Dispatchable;

    public $serviceEvents = [
        \Barterfy\Item\Events\ItemCreated::class => [
            \Barterfy\Item\Listeners\ItemCreated\ProcessItemImagesToCDN::class,
        ],
        \Barterfy\Item\Events\ItemIsReadied::class => [
            \Barterfy\Item\Listeners\ItemIsReadied\NotifyOnSlack::class,
        ],
        \Barterfy\Item\Events\ItemUpdated::class => [
            \Barterfy\Item\Listeners\ItemUpdated\PostMessageToMatchingInbox::class,
        ],
        \Barterfy\Item\Events\ItemDeleted::class => [
            \Barterfy\Item\Listeners\ItemDeleted\CloseAllMatchingInboxes::class,
            \Barterfy\Item\Listeners\ItemDeleted\CleanUpItemLikes::class,
            \Barterfy\Item\Listeners\ItemDeleted\DeleteImagesFromCDN::class,
        ],
        \Barterfy\Item\Events\ItemLiked::class => [
            \Barterfy\Item\Listeners\ItemLiked\CheckIfMatchOccurred::class,
        ],
        \Barterfy\Item\Events\ItemDisliked::class => [
            //
        ],
        \Barterfy\Item\Events\ItemReported::class => [
            \Barterfy\Item\Listeners\ItemReported\NotifyOnSlack::class,
        ],
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(DispatcherContract $dispatcher)
    {
        $this->dispatchServiceEvents($dispatcher);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
