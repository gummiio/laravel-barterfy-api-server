<?php

namespace Barterfy\Access\Exceptions;

use Barterfy\Api\Exceptions\ApiResponseException;

class FacebookOAuthException extends ApiResponseException
{
    public function __construct($e)
    {
        parent::__construct('Unable to connect to Facebook.', [
            'type'    => is_callable($e, 'getErrorType')? $e->getErrorType() : 'OAuthException',
            'message' => $e->getMessage()
        ], $e->getHttpStatusCode());
    }
}
