<?php

namespace Barterfy\Access\Requests;

use Barterfy\Api\Requests\ApiRequest;

class UserUpdateRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'longitude' => 'required|numeric',
            'latitude' => 'required|numeric',
            'country' => 'string',
            'city' => 'string'
        ];
    }
}
