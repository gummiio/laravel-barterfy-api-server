<?php

namespace Barterfy\Api\Exceptions;

use Barterfy\Api\Exceptions\ApiResponseException;

class ApiValidationException extends ApiResponseException
{
    public function __construct($validator)
    {
        parent::__construct('Invalid data.', [
            'type'    => 'ValidationFailed',
            'message' => 'Unable to process the post data.'
        ], 422);

        $this->addExtra('validation', $validator->errors());
    }
}
