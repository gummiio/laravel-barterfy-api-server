<?php

namespace Barterfy\Inbox\Listeners\InboxHasBeenAccepted;

use Barterfy\Inbox\Events\InboxHasBeenAccepted;
use Barterfy\Inbox\Events\InboxTradeCompleted;
use Barterfy\Inbox\Events\NewMessageCreated;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckIfAllAccepted implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  InboxHasBeenAccepted  $event
     * @return void
     */
    public function handle(InboxHasBeenAccepted $event)
    {
        $acceptance = $event->acceptance;
        $inbox = $acceptance->inbox()->with('acceptances', 'users')->first();

        if ($inbox->users->count() == $inbox->acceptances->whereStrict('accepted', true)->count()) {
            $inbox->success();
            event(new InboxTradeCompleted($inbox));
        }
    }
}
