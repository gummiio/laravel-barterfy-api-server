<?php

namespace Barterfy\Api\Model\Relationships;

use Barterfy\Api\Model\ApiLog;

trait ApiKeyRelationships
{
    /**
     * A Key have multiple api logs
     *
     * @return relation object
     */
    public function apiLogs()
    {
        return $this->hasMany(ApiLog::class);
    }
}
