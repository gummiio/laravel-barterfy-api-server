<?php

namespace Barterfy\Inbox\Events;

use App\Events\Event;
use Barterfy\Inbox\Model\InboxMessage;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class NewSupportMessageCreated extends Event implements ShouldQueue, ShouldBroadcast
{
    use SerializesModels;

    /**
     * InboxMessage Object
     *
     * @var object
     */
    public $message;

    public $shouldNotify;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(InboxMessage $message, $shouldNotify = true)
    {
        $this->message = $message;
        $this->shouldNotify = $shouldNotify;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return $this->shouldNotify? [env('REDIS_CHANNEL', '')] : [];
    }

    public function broadcastWith()
    {
        $this->message->addHidden('inbox');

        $sendTo = $this->message->is_system?
            $this->message->inbox->users->pluck('id')->all() :
            [];

        return [
            'sendTo' => $sendTo,
            'message' => $this->message->toArray()
        ];
    }
}
