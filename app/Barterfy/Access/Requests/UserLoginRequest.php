<?php

namespace Barterfy\Access\Requests;

use Barterfy\Api\Requests\ApiRequest;

class UserLoginRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'facebook_token' => 'required'
        ];
    }
}
