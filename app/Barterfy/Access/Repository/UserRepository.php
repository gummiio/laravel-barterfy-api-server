<?php

namespace Barterfy\Access\Repository;

use Barterfy\Access\Exceptions\FacebookOAuthException;
use Barterfy\Access\Model\User;
use Barterfy\Helpers\FacebookHelper;
use Barterfy\Repository;

class UserRepository extends Repository
{
    public static function GetUserByFacebookToken($facebook_token)
    {
        return User::where('facebook_token', $facebook_token)->first();
    }

    public static function GetGraphUserFromFacebookToken($facebook_token)
    {
        $fb = app('FacebookHelper')->login($facebook_token);

        try {
            return $fb->get('/me?fields=id,name,email,first_name,last_name,middle_name,gender,age_range,birthday')->getGraphUser();
        } catch(\Exception $e) {
            throw new FacebookOAuthException($e);
        }
    }

    public static function GetUserFromGraphUser($graphUser)
    {
        return User::where('username', 'fb_' . $graphUser->getId())->first();
    }

    public static function CreateUserFromGraphUser($graphUser, $facebook_token)
    {
        $user = new User([
            'email' => $graphUser->getEmail(),
            'first_name' => $graphUser->getFirstName(),
            'middle_name' => $graphUser->getMiddleName(),
            'last_name' => $graphUser->getLastName(),
            'gender' => $graphUser->getGender(),
            'age' => $graphUser->getField('age_range')->asJson(),
        ]);

        $user->username = 'fb_' . $graphUser->getId();
        $user->password = str_random(60);
        $user->api_token = md5($user->password . $user->username);
        $user->facebook_token = $facebook_token;
        $user->avatar = 'https://graph.facebook.com/'.$graphUser->getId().'/picture';

        $user->save();

        return $user;
    }

}
