<?php

namespace Barterfy\Item\Model;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Repository\InboxRepository;
use Barterfy\Item\Model\ItemLike;
use Barterfy\Item\Model\Relationships\ItemRelationships;
use Barterfy\Item\Model\Scopes\ItemLikeDislikeCounts;
use Barterfy\Item\Model\Scopes\ItemScopes;
use Barterfy\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Item extends Model
{
    use SoftDeletes, ItemRelationships, ItemScopes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['description', 'is_traded', 'is_readied'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'user_id', 'deleted_at', 'itemLikes', 'pivot',
        'item_likes_like_count', 'item_likes_dislike_count'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = ['images', 'user'];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'is_traded' => 'boolean',
        'is_readied' => 'boolean',
        'is_yours' => 'boolean',
        'dislikes' => 'integer',
        'likes' => 'integer',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['is_yours', 'dislikes', 'likes'];

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new ItemLikeDislikeCounts);
    }

    /**
     * Get is_yours attribute
     *
     * @return boolean
     */
    public function getisYoursAttribute()
    {
        return $this->user_id == Auth()->id();
    }

    public function getLikesAttribute()
    {
        return $this->item_likes_like_count;
    }

    public function getDislikesAttribute()
    {
        return $this->item_likes_dislike_count;
    }

    public function getIsMatchingAttribute()
    {
        return !! InboxRepository::GetMatchingInboxesByItem($this);
    }

    public function readied()
    {
        $this->is_readied = true;
        $this->save();

        return $this;
    }

    public function traded()
    {
        $this->is_traded = true;
        $this->save();

        return $this;
    }

    public function likedBy(User $user)
    {
        $like = new ItemLike;
        $like->liked = true;
        $like->user_id = $user->id;

        return $this->itemLikes()->save($like);
    }

    public function dislikedBy(User $user)
    {
        $like = new ItemLike;
        $like->liked = false;
        $like->user_id = $user->id;

        return $this->itemLikes()->save($like);
    }

    public function isDecidedBy(User $user)
    {
        return !! $this->itemLikes->where('user_id', $user->id)->count();
    }

    public function getItemUrl()
    {
        return url('admin/items/' . $this->id);
    }
}
