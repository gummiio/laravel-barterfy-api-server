<?php

namespace Integration\Item\Events;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Model\Inbox;
use Barterfy\Item\Events\ItemUpdated;
use Barterfy\Item\Model\Item;
use Integration\IntegrationTestCase;

class ItemUpdatedTest extends IntegrationTestCase
{
    /** @test */
    public function it_will_notify_all_matching_inbox_users()
    {
        $user = factory(User::class)->create();
        $item = $user->items()->save(
            factory(Item::class)->make()
        );

        $other = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create(['match_result' => null]);
        $inbox->users()->saveMany([$user, $other]);
        $inbox->items()->save($item);
        $messages = $inbox->messages;

        event(new ItemUpdated($item));

        $this->assertCount($messages->count() + 1, $inbox->fresh()->messages);
    }
}
