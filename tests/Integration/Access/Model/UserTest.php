<?php

namespace Integration\Access\Model;

use Barterfy\Access\Model\User;
use Integration\IntegrationTestCase;

class UserTest extends IntegrationTestCase
{
    /** @test */
    public function it_return_if_user_is_the_current_auth_user()
    {
        $you = factory(User::class)->create();
        $other = factory(User::class)->create();

        $this->actingAs($you);

        $this->assertTrue($you->is_you);
        $this->assertFalse($other->is_you);
    }
}
