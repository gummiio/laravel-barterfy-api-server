<?php

namespace Acceptance\Items;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Item\Events\ItemLiked;
use Barterfy\Item\Events\ItemUpdated;
use Barterfy\Item\Model\Item;

class LikeTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_return_resource_not_found_even_unauthenticate()
    {
        $this->post('v2/items/1/like', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate_when_item_exists()
    {
        $user = factory(User::class)->create();
        $user->items()->save(
            factory(Item::class)->make()
        );

        $this->post('v2/items/1/like', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_id_does_not_exists()
    {
        $user = factory(User::class)->create();

        $this->post('v2/items/1/like', [], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_failed_if_item_belongs_to_you()
    {
        $you = factory(User::class)->create();
        $you->items()->save(
            factory(Item::class)->make()
        );

        $this->post('v2/items/1/like', [], $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(400);
    }

    /** @test */
    public function should_failed_if_you_already_decided_on_item()
    {
        $you = factory(User::class)->create();
        $item = $you->items()->save(
            factory(Item::class)->make()
        );

        $item->likedBy($you);

        $this->post('v2/items/1/like', [], $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(400);
    }

    /** @test */
    public function should_successfully_when_item_exists_and_not_own_by_you_and_has_not_yet_decided()
    {
        factory(User::class)->create()->items()->save(
            factory(Item::class)->make([
                'description' => 'abcde'
            ])
        );

        $you = factory(User::class)->create();

        $this->expectsEvents(ItemLiked::class);

        $this->post('v2/items/1/like', [], $this->headers($you));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(201);
    }
}
