<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for adminitration pages.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/logs', ['middleware' => 'auth:admin', function () {
    return Randomlaunch\Api\Model\ApiLog::orderBy('created_at', 'desc')->take(10)->get();
}]);

/*
|--------------------------------------------------------------------------
| Admin Routes, No Auth Required
|--------------------------------------------------------------------------
*/
Route::get('login', 'AccessController@showLogin');
Route::post('login', 'AccessController@login');

/*
|--------------------------------------------------------------------------
| API Routes, Auth Required
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => 'auth:admin'], function() {
    // Route::post('/request_app_token', 'AppTokenController@new');
    Route::post('logout', 'AccessController@logout');

    Route::get('/', function() {
        return redirect('admin/dashboard');
    });

    Route::get('dashboard', 'ControllController@dashboard');

    Route::get('users/map', 'UserController@map');
    Route::delete('users/reset-status', 'UserController@resetStatus');
    Route::resource('users', 'UserController', [
        'except' => ['store']
    ]);

    Route::get('items/wall', 'ItemController@wall');
    Route::resource('items', 'ItemController');

    Route::get('inboxes/support', 'InboxController@support');
    Route::post('inboxes/{inboxes}/accept', 'InboxController@accept');
    Route::post('inboxes/{inboxes}/reject', 'InboxController@reject');
    Route::post('inboxes/{inboxes}/regret', 'InboxController@regret');
    Route::resource('inboxes', 'InboxController', [
        'only' => ['index', 'show']
    ]);

    Route::resource('inboxes.messages', 'InboxMessageController', [
        'only' => ['store']
    ]);

    Route::resource('api-logs', 'ApiController', [
        'only' => ['index', 'show', 'destroy']
    ]);

    Route::get('simulations/like-items', 'SimulationController@likeItems');
    Route::post('simulations/like-items/{items}', 'SimulationController@likeItemByID');

});
