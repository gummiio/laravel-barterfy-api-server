<?php

namespace Integration\Inbox\Events;

use Barterfy\Access\Model\User;
use Barterfy\Inbox\Events\InboxHasBeenRejected;
use Barterfy\Inbox\Model\Inbox;
use Integration\IntegrationTestCase;

class InboxHasBeenRejectedTest extends IntegrationTestCase
{
    /** @test */
    public function it_should_create_status_message()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();

        $inbox->users()->saveMany([$user, $user2]);

        $accept = $inbox->rejectedBy($user);

        event(new InboxHasBeenRejected($accept));

        $this->seeInDatabase('inbox_messages', [
            'inbox_id' => $inbox->id,
            'type' => 'status'
        ]);

        $this->assertCount(1, $inbox->load('messages')->messages);
    }

    /** @test */
    public function it_should_terminate_the_inbox_right_away()
    {
        $user = factory(User::class)->create();
        $user2 = factory(User::class)->create();
        $inbox = factory(Inbox::class)->create();

        $inbox->users()->saveMany([$user, $user2]);

        $accept = $inbox->rejectedBy($user);

        event(new InboxHasBeenRejected($accept));

        $this->assertFalse($inbox->fresh()->match_result);
    }
}
