<?php

namespace Barterfy\Inbox\Listeners\NewMessageCreated;

use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Repository\InboxMessageStatusRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class CreateUnreadStatus implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewMessageCreated  $event
     * @return void
     */
    public function handle(NewMessageCreated $event)
    {
        $message = $event->message;

        $statuses = InboxMessageStatusRepository::createUnreadStatusForMessage($message);

        $message->statuses()->saveMany($statuses);
    }
}
