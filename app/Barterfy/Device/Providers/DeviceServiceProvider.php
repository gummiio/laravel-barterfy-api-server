<?php

namespace Barterfy\Device\Providers;

use Barterfy\Traits\Dispatchable;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Support\ServiceProvider;

class DeviceServiceProvider extends ServiceProvider
{
    use Dispatchable;

    public $serviceEvents = [
        \Barterfy\Device\Events\DeviceCreated::class => [
            //
        ],
        \Barterfy\Device\Events\DeviceUpdated::class => [
            //
        ],
        \Barterfy\Device\Events\DeviceDeleted::class => [
            //
        ],
        \Barterfy\Device\Events\DevicePushNotificationFailed::class => [
            \Barterfy\Device\Listeners\DevicePushNotificationFailed\NotifyOnSlack::class
        ],
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(DispatcherContract $dispatcher)
    {
        $this->dispatchServiceEvents($dispatcher);
    }

    /**
     * Register ApiGuard singleton
     */
    public function register()
    {
        //
    }
}
