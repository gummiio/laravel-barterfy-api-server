<?php

namespace Barterfy\Api\Exceptions;

use Barterfy\Api\Exceptions\handleApiExceptions;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ApiExceptionHandlers
{
    public static function handleApiExceptions(\Exception $e)
    {
        if (! Request::isApi()) {
            return false;
        }

        if ($e instanceof AuthenticationException) {
            return Response()->apiError('Authentication Failed. Login Required.', [], 401);
        }

        if ($e instanceof MethodNotAllowedHttpException) {
            return response()->apiError('Method not allowed.', [
                'type'    => 'MethodNotAllowed',
                'message' => 'HTTP method are not allowed.'
            ], 405);
        }

        if ($e instanceof ModelNotFoundException) {
            return response()->apiError('Resource not found.', [
                'type'    => 'ResourceNotFound',
                'message' => 'Unable to find the resource you are looking for.'
            ], 404);
        }

        if ($e instanceof NotFoundHttpException) {
            return response()->apiError('Url not found.', [
                'type'    => 'UrlNotFound',
                'message' => 'Unable to find the endpoint you are looking for.'
            ], 404);
        }

        if ($e instanceof ApiResponseException) {
            return response()->apiError($e->getMessage(), $e->getErrorObject(), $e->getCode(), $e->getExtras());
        }

        return false;
    }
}
