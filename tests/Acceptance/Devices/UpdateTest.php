<?php

namespace Acceptance\Devices;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;
use Barterfy\Device\Model\Device;

class UpdateTest extends AcceptanceTestCase
{
    /** @test */
    public function it_should_return_resource_not_found_even_unauthenticate()
    {
        $this->put('v2/devices/1', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function it_should_fail_if_unable_to_authenticate_when_device_exists()
    {
        $user = factory(User::class)->create();
        $user->devices()->save(
            factory(Device::class)->make()
        );

        $this->put('v2/devices/1', [], $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function it_should_failed_if_id_does_not_exists()
    {
        $user = factory(User::class)->create();

        $this->put('v2/devices/1', [], $this->headers($user));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_failed_if_device_does_not_belong_to_you()
    {
        $other = factory(User::class)->create();
        $other->devices()->save(
            factory(Device::class)->make()
        );

        $you = factory(User::class)->create();

        $this->put('v2/devices/1', [], $this->headers($you));

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function should_successfully_update_when_device_exists_and_belong_to_you()
    {
        $you = factory(User::class)->create();
        $you->devices()->save(
            factory(Device::class)->make([
                'notify_on_matches' => false,
                'notify_on_new_message' => false
            ])
        );

        $this->put('v2/devices/1', [
            'notify_on_matches' => true,
            'notify_on_new_message' => false
        ], $this->headers($you));

        $this->seeJson(['success' => true]);
        $this->seeJson(['notify_on_matches' => true]);
        $this->seeJson(['notify_on_new_message' => false]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function only_SwitchableSettings_fields_are_updateable()
    {
        $you = factory(User::class)->create();
        $you->devices()->save(
            factory(Device::class)->make([
                'notify_on_matches' => false,
                'notify_on_new_message' => false,
                'device_name' => 'my ipod',
                'device_platform' => 'ios'
            ])
        );

        $this->put('v2/devices/1', [
            'device_name' => 'my blackberry',
            'device_platform' => 'windows'
        ], $this->headers($you));

        $this->seeJson(['success' => true]);
        $this->seeInDatabase('devices', ['id' => 1, 'device_name' => 'my ipod']);
        $this->seeInDatabase('devices', ['id' => 1, 'device_platform' => 'ios']);

        $this->assertResponseStatus(200);
    }
}
