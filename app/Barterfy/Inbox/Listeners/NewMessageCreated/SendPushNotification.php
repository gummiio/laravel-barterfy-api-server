<?php

namespace Barterfy\Inbox\Listeners\NewMessageCreated;

use Barterfy\Device\Repository\DeviceRepository;
use Barterfy\Inbox\Events\NewMessageCreated;
use Barterfy\Inbox\Repository\InboxMessageStatusRepository;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendPushNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewMessageCreated  $event
     * @return void
     */
    public function handle(NewMessageCreated $event)
    {
        if (! $event->shouldNotify) return;

        $message = $event->message;
        $inbox = $message->inbox;

        $devices = DeviceRepository::GetInboxDevices($inbox, 'notify_on_new_message', $message->user);

        $devices->each(function ($device) use ($message) {
            app('PushNotificationHelper')
                ->to($device)
                // ->badge(InboxMessageStatusRepository::GetUserUnreadMessages($device->user)->count())
                ->with($this->prepareNotificationData($message))
                ->send(sprintf('%s%s%s',
                    $message->user? $message->user->display_name : '',
                    $message->user? ': ' : '',
                    str_limit($message->message, 50)
                ));
        });
    }

    protected function prepareNotificationData($message)
    {
        return [
            'type' => 'messages',
            'inbox_id' => $message->inbox_id,
            'message_id' => $message->id,
        ];
    }
}
