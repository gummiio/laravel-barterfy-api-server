<?php

namespace Barterfy\Inbox\Listeners\NewMessageCreated;

use Barterfy\Inbox\Events\NewMessageCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Config;

class LogOnSlack implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewMessageCreated  $event
     * @return void
     */
    public function handle(NewMessageCreated $event)
    {
        $message = $event->message;

        app('SlackHelper')
            ->text('*New message was posted.*')
            ->color('good')
            ->addField([
                "*Inbox ID*: <" . $message->inbox->inboxLink() . "|{$message->inbox_id}>",
                "*User:* " . ($message->is_system? "_System ({$message->type})_" : "_{$message->user->full_name}_"),
                "*Message:* {$message->message}",
                "*At:* ".$message->created_at->toDateTimeString()
            ])
            ->sendTo(env('SLACK_LOG_CHANNEL', null));
    }
}
