<?php

namespace Acceptance\Api;

use Acceptance\AcceptanceTestCase;
use Barterfy\Access\Model\User;

class ApiTest extends AcceptanceTestCase
{
    /** @test */
    public function it_failed_if_no_app_key_is_provided()
    {
        $this->get('v2/api_status');

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(403);
    }

    /** @test */
    public function it_failed_if_not_authenticated()
    {
        $this->get('v2/api_status', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function should_failed_if_auth_key_invalid()
    {
        $this->get('v2/api_status', [
            'HTTP_BARTERFY_API' => $this->appKey,
            'HTTP_AUTHORIZATION' => 'Basic i-am-just-some-random-string'
        ]);

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(401);
    }

    /** @test */
    public function should_success_if_auth_key_correct()
    {
        $user = factory(User::class)->create();

        $this->get('v2/api_status', $this->headers($user));

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function can_authenticated_with_api_token_as_url_string()
    {
        $user = factory(User::class)->create([
            'api_token' => 'abcde'
        ]);

        $this->get('v2/api_status?api_token=abcde', $this->headers());

        $this->seeJson(['success' => true]);

        $this->assertResponseStatus(200);
    }

    /** @test */
    public function should_return_404_if_no_endpoint()
    {
        $this->get('v2/asdfasdf', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_return_404_if_model_not_found()
    {
        $this->get('v2/items/1', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(404);
    }

    /** @test */
    public function should_return_405_if_invalid_method()
    {
        $this->patch('v2/api_status', $this->headers());

        $this->seeJson(['success' => false]);

        $this->assertResponseStatus(405);
    }
}
