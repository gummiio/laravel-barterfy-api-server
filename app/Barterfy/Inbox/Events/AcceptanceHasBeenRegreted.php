<?php

namespace Barterfy\Inbox\Events;

use App\Events\Event;
use Barterfy\Inbox\Model\InboxAcceptance;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;

class AcceptanceHasBeenRegreted extends Event implements ShouldQueue
{
    /**
     * InboxAcceptance Object
     *
     * @var object
     */
    public $acceptanceJSON;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(InboxAcceptance $acceptance)
    {
        $this->acceptanceJSON = $acceptance->toJson();
    }
}
