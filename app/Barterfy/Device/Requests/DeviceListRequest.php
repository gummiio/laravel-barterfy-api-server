<?php

namespace Barterfy\Device\Requests;

use Barterfy\Api\Requests\ApiRequest;

class DeviceListRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'deleted' => 'boolean',
            'platform' => 'in:ios,android,windows'
        ];
    }
}
