<?php

namespace Barterfy\Api\Model\Relationships;

use Barterfy\Access\Model\User;
use Barterfy\Api\Model\ApiKey;

trait ApiLogRelationships
{
    /**
     * A log belongs to a api key
     *
     * @return relation object
     */
    public function apiKey()
    {
        return $this->belongsTo(ApiKey::class);
    }

    /**
     * A log can belongs to a user
     *
     * @return relation object
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'auth_user_id')->withTrashed();
    }
}
