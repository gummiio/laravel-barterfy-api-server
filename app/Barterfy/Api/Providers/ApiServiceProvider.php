<?php

namespace Barterfy\Api\Providers;

use Barterfy\Api\ApiGuard;
use Barterfy\Traits\Dispatchable;
use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\ServiceProvider;
use Response;

class ApiServiceProvider extends ServiceProvider
{
    use Dispatchable;

    public $serviceEvents = [
        \Barterfy\Api\Events\LongApiRespondTimeExecuted::class => [
            \Barterfy\Api\Listeners\LongApiRespondTimeExecuted\NotifyOnSlack::class,
        ],
    ];

    /**
     * Perform post-registration booting of services.
     *
     * @return void
     */
    public function boot(DispatcherContract $dispatcher)
    {
        $this->dispatchServiceEvents($dispatcher);

        Response::macro('api', function($message = '', $data = [], $status = 200, $success = true, $extra = []) {
            $respond = array_merge([
                'success'   => $success,
                'message'   => $message
            ], $extra);

            $respond[$success? 'data' : 'error'] = $data;

            return Response::json($respond, $status);
        });

        Response::macro('apiSuccess', function($message = '', $data = [], $status = 200, $extra = []) {
            return Response::api($message, $data, $status, true, $extra);
        });

        Response::macro('apiError', function($message = '', $error = [], $status = 404, $extra = []) {
            return Response::api($message, $error, $status, false, $extra);
        });
    }

    /**
     * Register ApiGuard singleton
     */
    public function register()
    {
        $this->app->singleton('ApiGuard', function() {
            return new ApiGuard;
        });
    }
}
