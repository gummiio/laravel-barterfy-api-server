<?php

namespace Barterfy\Helpers;

use Facebook\Facebook;

class FacebookHelper
{
    protected $fb;
    protected $facebook_token;

    public static function init()
    {
        $fb = new static();

        $fb->fb = new Facebook([
            'app_id' => env('FACEBOOK_APP_ID', ''),
            'app_secret' => env('FACEBOOK_APP_SECRET', ''),
            'default_graph_version' => 'v2.5'
        ]);

        return $fb;
    }

    public function login($facebook_token)
    {
        $this->facebook_token = $facebook_token;

        return $this;
    }

    public function get($endpoint)
    {
        return $this->fb->get($endpoint, $this->facebook_token);
    }

    public function createTestUser($name = '')
    {
        $response = $this->fb->post(env('FACEBOOK_APP_ID', '').'/accounts/test-users', [
            'installed' => true,
            'name' => $name,
            'permissions' => 'user_birthday,user_location,user_friends,email,public_profile,basic_info',
            'access_token' => env('FACEBOOK_APP_ID', '') .'|'. env('FACEBOOK_APP_SECRET', '')
        ]);

        return $response->getGraphUser()->asArray();
    }

    public function deleteTestUser($testUserId)
    {
        $this->fb->delete($testUserId, [
            'access_token' => env('FACEBOOK_APP_ID', '') .'|'. env('FACEBOOK_APP_SECRET', '')
        ]);
    }
}
