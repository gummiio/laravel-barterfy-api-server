<?php

namespace Barterfy\Inbox\Events;

use App\Events\Event;
use Barterfy\Inbox\Model\Inbox;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\SerializesModels;

class MatchInboxCreated extends Event implements ShouldQueue, ShouldBroadcast
{
    use SerializesModels;

    /**
     * Inbox Object
     *
     * @var object
     */
    public $inbox;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Inbox $inbox)
    {
        $this->inbox = $inbox;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [env('REDIS_CHANNEL', '')];
    }

    public function broadcastWith()
    {
        // $this->inbox->addHidden('users');

        return [
            'sendTo' => $this->inbox->users->pluck('id')->all(),
            'inbox' => $this->inbox->toArray()
        ];
    }
}
