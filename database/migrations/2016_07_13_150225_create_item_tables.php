<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('description', 191)->nullable();
            $table->boolean('is_traded')->default(false);
            $table->boolean('is_readied')->default(false);

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');
        });

        Schema::create('item_likes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_id');
            $table->unsignedInteger('user_id');

            $table->boolean('liked');

            $table->foreign('item_id')
                ->references('id')->on('items')->onDelete('cascade');

            $table->foreign('user_id')
                ->references('id')->on('users')->onDelete('cascade');

            $table->unique(['item_id', 'user_id']);
        });

        Schema::create('item_images', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('item_id');
            $table->integer('cloud_index');
            $table->string('public_id', 191)->nullable()->default(null);
            $table->string('resource_type', 191)->nullable()->default(null);
            $table->string('url', 191)->nullable()->default(null);
            $table->string('path', 191)->nullable()->default(null);

            $table->softDeletes();
            $table->timestamps();

            $table->foreign('item_id')
                ->references('id')->on('items')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('item_images');
        Schema::drop('item_likes');
        Schema::drop('items');
    }
}
